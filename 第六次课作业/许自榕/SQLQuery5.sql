create database  Student2

on

(

	name='Student2',

	filename='D:\Student2.mdf',

	size=10,

	maxsize=100,

	filegrowth=10

)

log on

(

	name='Student2_log',

	filename='D:\Student2_log.ldf',

	size=10,

	maxsize=100,

	filegrowth=10

)
go

use  Student2

go
create table tblUser 
(
userId int identity(1,1) primary key not null,
userName varchar(20),
userTel varchar(20) not null,
userTeldsa int check(len(userTeldsa)=11)
)