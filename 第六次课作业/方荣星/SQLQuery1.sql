create database student 
on
(
name='Student',
	filename='D:\Student.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(name='Student_log',
	filename='D:\Student_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
use student
go
create table class
(
classid int primary key identity(1,1),
classname char not null  unique ,
classtime text not null ,
classdescribe text not null

)
create table classmessage
(studentnumber int primary key identity(1,1),
studentname nvarchar(10)not null check(studentname>2),
studentsex nvarchar(10)  check(studentsex='��' or studentsex='Ů') default('��'),
studentage int not null check(studentage>15 and studentage<40),
studentfamily char(10) not null default('�����人'),
classid int)

create table course
(
courseid int primary key identity(1,1),
coursename nvarchar(10) not null unique ,
coursedescribe nvarchar(10) not null)

create table grade(
gradeid int primary key identity(1,1) not null ,
geade01 int references  classmessage(studentnumber) not null,
geade02 int references  classmessage(studentnumber)not null,
geade int check(geade>0 and geade<100)
)
