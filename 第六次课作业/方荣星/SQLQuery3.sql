create database BBS
on
(name='BBS',
	filename='D:\BSS.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(name='BBS_log',
	filename='D:\BSS_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
go
use BBS
create table bbsUsers
(
uIDD int primary key identity(1,1),
uName varchar(10) not null unique ,
 uSex  varchar(2)  not null check(uSex='男' or uSex='女'),
 uAge  int  not null  check(uAge>15 and uAge<60),
  uPoint  int not null  check(uPoint>=0)
)

create table bbsTopic(
 tID  int  primary key identity(1,1),
 tUID  int references bbsUsers(uIDD),
 tSID  int references bbsSection(sIDD),
 tTitle  varchar(100) not null,
 tMsg  text  not null,
 tTime datetime ,
 tCount  int
)
create table bbsReply(
rID  int primary key identity(1,1),
 rUID  int references bbsUsers(uIDD ),
 rTID  int references bbsTopic(tID),
 rMsg  text  not null,
 rTime  datetime 

)
create table bbsSection(
sIDD  int primary key identity(1,1),
sName  varchar(10) not null ,
 sUid   int references bbsUsers(uIDD)
)
--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2
insert into bbsUsers(uName,uSex,uAge,uPoint)
select'小雨点','女','20','0'union
select'逍遥','男','18','4'union
select'七年级生','男','19','2'
go	

	--2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
	select uName,uPoint into bbsPoint from bbsUsers
	go
	--3.给论坛开设4个板块
	--  名称        版主名
	--  技术交流    小雨点
	--  读书世界    七年级生
	--  生活百科     小雨点
	--  八卦区       七年级生
	insert into bbsSection(sName,sUid)
	select'技术交流',3 union
	select'读书世界',1 union
	select'生活百科',3 union
	select'八卦区',1
	go
	--4.向主贴和回帖表中添加几条记录
	insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
select '2','1','范跑跑','谁是范跑跑','2008-7-8','1'union
select '1','3','.NET','与Java的区别是什么呀？','2008-9-1','2'union
select '3','4','今年夏天最流行什么呀？','有谁知道今年夏天最流行什么呀？','2008-9-10','0'


--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

insert into bbsReply(rTID,rMsg,rTime,rUId)
select'1','不知道','2008-9-5','2'union
select'2','不认识','2008-9-10','1'union
select'3','不知道','2008-10-1','3'

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）



alter table bbsReply drop constraint FK_bbsReply_rUID
alter table bbsTopic drop constraint FK_bbsTopic_tUID
alter table bbsSection drop constraint FK_bbsSection_sUid
alter table bbsUsers drop constraint PK_bbsUsers_UID

delete from bbsUsers where UIDD=2 

--	6.因为小雨点发帖较多，将其积分增加10分


update bbsPoint set uPoint=(uPoint + 10) where uName='小雨点'


--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）

delete from bbsReply where rTID=3
delete from bbsTopic where tSID=4
delete from bbsSection where sIDD=4

--	8.因回帖积累太多，现需要将所有的回帖删除

delete from bbsReply 
truncate table bbsReply
drop table bbsReply