use master
go
create database bbs
on
(
 name='bbs',
 filename='D:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='bbs_log',
 filename='D:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use bbs
go
create table bbsUsers
(
 bbUID int identity,
 uName varchar(10) not null,
 uSex varchar(2) not null,
 uAge int not null,
 uPoint int not null
 )
go
use bbs
go
create table bbsSection
(
 bbsID int identity,
 sName varchar(10) not null,
 sUid int references bbsUsers(bbUID)
 )
go
use bbs
go
create table bbsTopic
(
 tID int primary key identity,
 tUID int references bbsUsers(bbUID),
 tSID int references bbsSection(bbsID),
 tTitle varchar(100) not null,
 tMsg text not null,
 tTime datetime,
 tCount int  
 )
go
use bbs
go
create table bbsReply
(
 rID int primary key identity,
 rUID int references bbsUsers(bbUID),
 rTID int references bbsTopic(tID),
 rMsg text not null,
 rTime datetime 
 )
 --添加约束
alter table bbsUsers add constraint PK_bbsUsers_bbUID primary key(bbUID)

alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName) 

alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男'or uSex='女')

alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60) 

alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_bbsID primary key(bbsID)

alter table bbsSection add constraint FK_bbsUsers_bbUID foreign key(bbUID) references bbsUsers(bbUID)

insert into bbsUsers(uName,uSex,uAge,uPoint) 
values('小雨点','女',20,0),
      ('逍遥','男',18,4),
      ('七年级生','男',19,2)
--二2.
select uName,uPoint into bbsPoint from bbsUsers

--二3.
insert into bbsSection(sName,sUid) 
values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

--二4.
--主贴表（bbsTopic）
--主贴编号  tID  int 主键primary key  标识列，identity
--发帖人编号  tUID  int 外键 renferences 表（列） 引用用户信息表的用户编号
--版块编号    tSID  int 外键 primary key 引用版块表的版块编号    （标明该贴子属于哪个版块）
--贴子的标题  tTitle  varchar(100) 不能为空
--帖子的内容  tMsg  text  不能为空
--发帖时间    tTime  datetime  
--回复数量    tCount  int
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) 
values(2,4,'范跑跑','谁是范跑跑',2008-7-8,1),
      (3,1,'.NET','与JAVA的区别是什么呀？',2008-9-1,2),
      (1,3,'今年夏天最流行什么',' 有谁知道今年夏天最流行什么呀？',2008-9-10,0)

--回帖：分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

--回帖表（bbsReply）
--回贴编号  rID  int 主键primary key   标识列，identity
--回帖人编号  rUID  int 外键 renferences 引用用户信息表的用户编号
--对应主贴编号    rTID  int 外键 renferences 引用主贴表的主贴编号（标明该贴子属于哪个主贴）
--回帖的内容  rMsg  text  不能为空
--回帖时间    rTime  datetime 

insert into bbsReply(rUID,rTID,rMsg,rTime) 
values(1,3,'地震跑路人',2020-1-1),
(1,2,'区别很大',2020-2-1),
(2,2,'流行敲代码',2020-3-1)
--二.5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

alter table dbo.bbsTopic drop constraint FK__bbsTopic__tUID__1A14E395
alter table dbo.bbsReply drop constraint FK__bbsReply__rUID__1DE57479

delete from bbsUsers where uName='逍遥'

--二.6.因为小雨点发帖较多，将其积分增加10分

update bbsUsers set uPoint=10 where uName='小雨点'

--二.7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）

alter table dbo.bbsTopic drop constraint FK__bbsTopic__tSID__1B0907CE

delete from bbsSection where sName='生活百科'

--8.因回帖积累太多，现需要将所有的回帖删除

drop table bbsReply

