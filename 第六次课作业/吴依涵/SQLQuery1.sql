use master-------------------------------------------------第一个数据库
go
create database TestDB
on
(
 name='TestDB',
 filename='D:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='TestDB_log',
 filename='D:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use TestDB
go
create table typeInfo
(
 typeId int primary key identity,
 typeName varchar(10) not null,
 )
go
use TestDB
go
create table loginInfo
(
 LoginId int primary key identity,
 LoginName nvarchar(10) unique not null,
 LoginPwd varchar(20) default(123456) not null,
 StuSex char(2) default('男') check(StuSex='男' or StuSex='女'),  
 Birthday datetime,
 MembershipType text
 )



use master----------------------------------------------------第二个数据库
go
create database company
on
(
 name='company',
 filename='E:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='company_log',
 filename='E:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use company
go
create table sectionInfo
(
 sectionID int primary key identity,
 sectionName varchar(10) not null,
 )
go
use company
go
create table userInfo
(
 userNo int primary key identity not null,
 userName varchar(10) unique check(userName>4) not null,
 userSex varchar(2) check(userSex='男' or userSex='女') not null,  
 userAge int check(userAge>=1 and userAge<=100) not null,  
 userAddress varchar(50) default('湖北'),
 userSection int references sectionInfo (sectionID )
 )
go
use company
go
create table workInfo
(
 workId int primary key identity not null,
 userId int references userInfo(userNo) not null,
 workTime datetime not null,
 workDescription varchar(40) check(workDescription='迟到'or workDescription='早退'or workDescription='旷工'or workDescription='病假'or workDescription='事假')
 )


use master------------------------------------------------第三个数据库
go
create database Students
on
(
 name='Students',
 filename='C:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='C:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Students
go
create table Classlnfo
(
 classid int primary key identity,
 className varchar(10) unique not null,
 openTime datetime not null,
 ClassDescription text
 )
go
use Students
go
create table Stulnfo
(
stuNumber int primary key identity,
stuName nvarchar(10) check(stuname>2) unique,
stuSex varchar(2) check(stuSex='男' or stuSex='女') not null,
stuAge int check(stuAge>=15 and stuAge<=40) not null,  
stuAddress varchar(50) default('湖北武汉'),
classNumber varchar(10) 
)
go
use Students
go
create table CourseInfo
(
CourseNummber int primary key identity,
CourseName nvarchar(10) unique not null,
CourseDescription text
) 
go
use Students
go
create table ScoreInformation
(
ScoreNummber int primary key identity,
StuNummber nvarchar(10) not null,
CourseNummber nvarchar(10) not null,
Course int check(Course>=0 and Course<=100)
) 



use master------------------------------------------------第四个数据库
go
create database HousInformation 
(
 name='HousInformation',
 filename='C:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='C:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use HousInformation 
go
create table tblUser
(
 userId varchar(10),
 userName varchar(10),
 userTel varchar(11)
 )
go
use HousInformation 
go
create table tblHouseType 
(
 typeId varchar(10),
 typName varchar(10),
 )
go
use HousInformation 
go
create table tblQx 
(
 qxId varchar(10),
 qxName varchar(10)
 )
go
use HousInformation 
go
create table tblHouseInfo 
(
 Houseid varchar(10),
 Housedesc text,
 HouseuserId varchar(10) references tblUser(userId),
 Housezj int,
 Houseshi text,
 Houseting text,
 HousetypeId varchar(10) references tblHouseType(typeId),
 HouseqxId varchar(10) references tblQx(qxId) 
 )