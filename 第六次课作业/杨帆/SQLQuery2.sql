create database Commpany
on
(
	name='Commpany',
	filename='D:\test\Commpany.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='Commpany_log',
	filename='D:\test\Commpany_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
use Commpany
go

create table SectionInfo
(
	SectionId int primary key identity(1,1),
	SectionName varchar(10) not null 
)

create table UserInfo
(
	UserNo int primary key identity(1,1),
	UserName varchar(10) unique not null  check(len(UserName)>=4),
	UserSex varchar(2) not null check(UserSex in('��', 'Ů')),
	UserAge int not null check(UserAge>0 and UserAge<=100) ,
	UserAddress varchar(50) default '����',
	SectionId int references SectionInfo(SectionId)
)

create table WorkInfo
(
	WorkId int primary key not null identity(1,1),
	UserNo int references UserInfo(UserNo) not null ,
	WorkTime datetime not null ,
	WorkDescription varchar(40) not null check(WorkDescription in ('�ٵ�','����','����','����','�¼�'))
)