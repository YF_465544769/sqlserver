create database  bbs
on
(
	name='bbs',
	filename='D:\test\bbs.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\test\bbs_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)

use bbs
go

create table bbsUsers
(
	UuId int identity(1,1),
	UName varchar(10) not null ,
	USex varchar(2) ,
	UAge int ,
	UPoint int not null
)

alter table bbsUsers add constraint PK_UuId primary key (UuId)
alter table bbsUsers add constraint UQ_UName unique (UName)
alter table bbsUsers add constraint CK_USex check(USex='男'or USex='女')
alter table bbsUsers add constraint CK_UAge check(UAge>=15 and UAge<=60)
alter table bbsUsers add constraint CK_UPoint check(Upoint>=0)

create table bbsTopic
(
	TtId int primary key identity(1,1),
	TuId int references bbsUsers(Uuid),
	TsId int references bbsSection(SsId),
	TTitlr varchar(100) not null,
	TMsg text,
	TTime datetime,
	TCount int 
)

create table bbsReply
(
	RrId int primary key identity(1,1),
	RuId int references bbsUsers(UuId),
	RtId int references bbsTopic(TtId),
	RMsg text not null,
	RTime datetime,
)

create table bbsSection
(
	Ssid int identity(1,1) not null,
	SName varchar(10) not null,
	SuId int,
	Name varchar(10)
)

alter table bbsSection add constraint PK_Ssid primary key (Ssid)
alter table bbsSection add constraint FK_SuId foreign key(SuId) references bbsUsers(UuId)

insert bbsUsers (UName,USex,UAge,UPoint)
select '小雨点','女',20,0 union
select '逍遥','男',18,4 union
select '七年级生','男',19,2 

select UName ,UPoint into bbsPoint from bbsUsers

insert into bbsSection(SName,SuId)values('技术交流',2),('读书世界',3),('生活百科',2),('八卦区',3)

insert into bbsTopic(TuId,TsId,TTitlr,TMsg,TTime,TCount)values (3,4,'范跑跑','谁是范跑跑','2008-7-8','1'),
(2,1,'.NET ','与JAVA的区别是什么呀？','2008-9-1','2'),
(4,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10','0')

insert into bbsReply(RtID,RMsg,RTime,RUId)
select'1','范跑跑','2008-7-8','3'union
select'2','范跑跑','2008-9-1','1'union
select'3','范跑跑','2008-9-10','2'

alter table bbsSection drop constraint FK_SuId
 
delete from bbsUsers where UuId='2'

update bbsUsers set UPoint=14 where UuId = 1 

alter table bbsSection drop constraint 

delete from bbsSection where SsId=3

truncate table bbsReply




