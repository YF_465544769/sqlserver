use master
go

create database bbs
on
(
	name=bbs,
	filename='D:\text\bbs.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=bbs_log,
	filename='D:\text\bbs.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)

use bbs
go

create table bbsUsers
(
	uID1 int primary key identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge  varchar(2) not null,
	uPoint int not null
)
alter table bbsUsers
	add constraint uq_uName unique (uName)
alter table bbsUsers
	add constraint ck_uSex check(uSex='男' or uSex='女')
alter table bbsUsers
	add constraint ck_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers
	add constraint ck_uPoint check(uPoint>=0)
go


use bbs
go

create table bbsSection
(
	sID1 int not null,
	sName varchar(10) not null,
	sUid int,
)
select * from bbsSection
alter table bbsSection
	add constraint pk_sID primary key (sID1)
alter table bbsSection
	add constraint fk_sUid
	foreign key(sUid) references bbsUsers(uid1)
	alter table bbsSection add mingchen nvarchar(10)
	insert into bbsSection values(1,'小雨点',null,'技术交流' ),(2,'七年级',null,'读书世界'),(3,'小雨点',null,'生活百科' ),(4,'七年级',null,'八卦区')
	alter table bbsSection add mingchen nvarchar(5) 



use bbs
go

create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int references bbsUsers(uID1),
	tSID int references bbsSection(sID1),
	tTitle  varchar(100) not null,
	tMsg text not null,
	tTimes datetime,
	tCount int
)

use bbs
go

create table bbsReply
(
	rID int primary key identity(1,1),	
	rUID int references bbsUsers(uID1),
	tTID int references bbsTopic(tID),
	rMsg text not null,
	tTimes datetime
)



	select * from bbsSection
go

use bbs
go
create table bbsPoint
(
	uID1 int primary key identity(1,1),
	uName varchar(10) not null,
	uPoint int not null
)
select * from bbsPoint
insert into bbsUsers(uName,uSex,uAge,uPoint)values('小雨点','女','20','0'),('逍遥','男','18','4'),('七年级生','男','19','2')

insert into bbsPoint select uName,uPoint from bbsUsers
