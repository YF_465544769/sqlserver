 use master
	 go

	 create database Bbs
	 on
	 (
	 name = 'Bbs',
	 filename = 'D:\test\Bbs.mdf',
	 size = 5MB,
	 maxsize = 50MB ,
	 filegrowth = 10%
	 )
	 log on
	 (
	 name = 'Bbs_log',
	 filename = 'D:\test\Bbs_log.mdf',
	 size = 5MB,
	 maxsize = 50MB ,
	 filegrowth = 10%
	 )

	 use Bbs
	 go

	  if exists (select * from sys.all_objects where name = 'BbsUSersInfo')
	 drop database BbsUSersInfo
	 create table BbsUSersInfo
	 (
	  UId int identity(1,1) , 
	  UName varchar(10) not null,
	  USex  varchar(2) not null,
	  UAge  int not null,
	  UPoint  int  not null
	 )
	
	 alter table BbsUSersInfo add constraint PK_UId Primary Key (UId)
	 alter table BbsUSersInfo add constraint UQ_UName unique (UName)
	 alter table BbsUSersInfo add constraint CK_USex check( USex = '男' or  USex = '女')
	 alter table BbsUSersInfo add constraint CK_UAge check  ( UAge >=15 and  UAge <= 60)
	 alter table BbsUSersInfo add constraint CK_UPoint check( UPoint>=0)

	 select * from BbsUSersInfo
	 insert into BbsUSersInfo values ( '小雨点', '女',20 ,0 ),(  '逍遥','男',18 ,4 ),( '七年级生' ,'男',19,2 )
	  create table BbsPointInfo
	  (
	  UId int identity(1,1) , 
	  UName varchar(10) not null,
	  UPoint  int  not null
	  )
	 insert into BbsPointInfo select  UName,UPoint from BbsUSersInfo


	 create table BbsTopicInfo
	 (
	 TId  int primary key identity(1,1),
	 TUID int references BbsUSersInfo( UId) ,
	 TSId  int references BbsSectionInfo( SId ),
	 TTitle  varchar(100) not null,
	 TMsg  text not null,
	 TTime  datetime,
	 TCount  int
	 )

	 select * from  BbsTopicInfo 
	 insert into BbsTopicInfo ( TUID ,TSId , TTitle,TMsg,TTime, TCount ) values (2,1,'范跑跑' ,' 谁是范跑跑 ', 2008-7-8 , 1),
									( 3,2,'.NET ' ,'与JAVA的区别是什么呀？',  2008-9-1  , 2),
									(1,3, '今年夏天最流行什么什么呀？',   '有谁知道今年夏天最流行',  2008-9-10  ,0)
	  create table BbsReplyInfo
	  (
	  RID  int primary key identity(1,1),
	  RUID  int references BbsUSersInfo(UId) ,
	  RTID  int references BbsTopicInfo( TId ),
	  RMsg  text not null,
	  RTime  datetime 
	  )

	   select * from BbsReplyInfo
	   insert into BbsReplyInfo ( RMsg,RTime,RUID ) values ('范跑跑 ', 2008-7-8 ,3),
														   ('区别',2008-9-1  ,1  ),
														   ('穿搭',2008-9-10 ,2 )


	  if exists (select * from sys.all_objects where name = ' BbsSectionInfo')
	 drop database  BbsSectionInfo
	  create table BbsSectionInfo
	  (
	  SId  int  identity(1,1),
	  SName  varchar(10) not null ,
	  SUid   int
	  )
	
	 alter table BbsSectionInfo add constraint PK_SId   Primary Key ( SId )
	 alter table BbsSectionInfo add constraint UQ_SName unique (SName)
	 alter table BbsSectionInfo add constraint FK_SUid foreign key (SUid) references  BbsUSersInfo( UId)

	  select * from  BbsSectionInfo 
	  insert into BbsSectionInfo (SName,SUid) 
	   select '技术交流',1 union
	   select ' 读书世界',3 union
	   select '  生活百科',1 union
	   select ' 八卦区 ',3


		--5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
		alter table BbsTopicInfo drop constraint FK__BbsTopicIn__TUID__31EC6D26
		delete from BbsUSersInfo where UId = 2

		--6.因为小雨点发帖较多，将其积分增加10分
		update BbsUSersInfo set UPoint = 10 where UId = 1

		--7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
		alter table BbsTopicInfo  drop constraint FK__BbsTopicIn__TSId__32E0915F
		delete from BbsSectionInfo where SId = 1

		--8.因回帖积累太多，现需要将所有的回帖删除
		delete from BbsReplyInfo