create database  company

on

(

	name='company',

	filename='D:\company.mdf',

	size=10,

	maxsize=100,

	filegrowth=10

)

log on

(

	name='company_log',

	filename='D:\company_log.ldf',

	size=10,

	maxsize=100,

	filegrowth=10

)
go

use   company

go

create table sectionInfo
(
   sectionID  int identity(1,1) primary key,
   sectionName varchar(10) not null

)
create table userInfo
(
 userNo int identity(1,1) primary key not null,
 userName varchar(10) not null unique check(len( userName)>4),
 userSex varchar(2)   check(usersex='��'or usersex='Ů' ) not null,
 userAge int not null  check(userAge>0 and userAge<=100),
 userAddress  varchar(50) default('����'),
 userSection  int references sectionInfo(sectionID)
)
create table workInfo
( workId int identity(1,1) primary key not null,
  userId  int references userInfo(userNo) null,
  workTime datetime null,
  workDescription  varchar(40) null check(workDescription='�ٵ�'or workDescription='����'or workDescription='����'or workDescription='����'or workDescription='�¼�')
)
