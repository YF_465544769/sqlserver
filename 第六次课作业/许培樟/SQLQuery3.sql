create database Student
on
(
 name='Student',
 filename='D:\Student.mdf',
 size=10,
 maxsize=100,
 filegrowth=10%
)
log on
(
 name='Student_log',
 filename='D:\Student_log.ldf',
 size=10,
 maxsize=100,
 filegrowth=10%
)

use Student
go

create table class
(
 classid int primary key identity(1,1),
 classname varchar(10) not null unique,
 classday date,
 classtext text
)
create table Studentinformation
(
 studentid int primary key identity(1,1),
 studentname varchar check(len(studentname)>2) not null,
 studentsex varchar check(studentsex in('��','Ů')) default('��') not null,
 studentage int check(studentage>=15 and studentage<=40 ) not null,
 Homeaddress  varchar default('�����人'),
 classid int references class(classid)
)
create table courseinformation
(
 courseid int primary key identity(1,1),
 coursename varchar not null unique,
 Coursedescription text
)
create table Performanceinformation
(
 resultsid int primary key identity(1,1),
 studentid int references Studentinformation(studentid) not null,
 courseid int references courseinformation(courseid) not null,
 resultscj int check(resultscj>0 and resultscj<=100)
)