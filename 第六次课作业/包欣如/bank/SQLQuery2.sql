use master
go
create database  ATM
on
(
name='ATM',
filename='D:\bank\ATM.mdf',
size=5mb,
maxsize=100mb,
filegrowth=15mb
)
log on 
(
name='ATM_log',
filename='D:\bank\ATM_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=15mb
)
go
use ATM
go
create table userInfo
(
 customerID int identity primary key,
 customerName varchar not null,
 PID int not null check(pid=15 or pid=18),
 telephone int check(telephone=13),
 address text 
 )
 create table cardInfo
 (
 cardID int primary key not null,
 curType money default('RMB') not null,
 savingType varchar check(savingtype in('活期','定活两便','定期')),
 openDate datetime not null default(getdate()),
 balance int not null check(balance<=1),
 pass int not null default(888888) check(pass=6),
 IsReportLoss bit default('否'),
 customerID int not null foreign key references userInfo(customerID)
 )
 go
 create table transInfo
 (
 transId int identity primary key,
 transDate datetime not null default(getdate()),
 cardID int not null foreign key references cardInfo(cardID),
 transType nvarchar check(transType in('存入' , '支取')),
 transMoney money not null check(transMoney>0),
 remark text
 )




