use master 
go
if exists(select*from sys.databases ATMxt)
drop database ATMxt
create database ATMxt
on
(
name='ATMxt',
filename='D:\bank\ATMxt.dmf',
size=5mb,
filegrowth=15%,
maxsize=100mb
)
log on
(
name='ATMxt_log',
filename='D:\bank\ATMxt_log.lmf',
size=5mb,
filegrowth=15%,
maxsize=100mb
)
go
use ATMxt
go
create table userInfo
(
customerID int primary key identity(1,1),
customerName nvarchar(20) not null,
PID int unique check(len(PID)=18 or len(PID)=15),
telephone varchar(20) check(len(telephone)=13 or telephone like ('____-________')) not null ,
address nvarchar(20)
)
create table cardInfo
(
cardID varchar(20) not null primary key check(len(cardID)=13 or cardID like('1010 3576 ____ ___')),
curType nvarchar default('RMB'),
savingType nvarchar(4) check(savingType in ('活期','定活两便','定期')),
openDate datetime not null default(getdate()),
balance money not null check(balance>=1),
pass char(6) not null default(888888),
IsReportLoss char(2) check(IsReportLoss in ('是','否')) default('否'),
customerID int references userInfo(customerID) not null
)
create table transInfo 
(
transId int primary key identity(1,1),
transDate datetime default(getdate()) not null,
cardID varchar(20) not null references cardInfo(cardID) ,
transType nvarchar(2) check(transType in ('存入','支取')) not null,
transMoney money not null check(transMoney >0),
remark ntext 
)
insert into userInfo(customerID,customerName,PID,telephone,address)values
(1,'孙悟空',123456789012345,0716-78989783,'北京海淀')
insert into cardInfo(cardID,savingType,balance,customerID)values
(1,'活期','1010 3576 1234 567',1000,1)
insert into userInfo(customerName,PID,telephone)values
(2,'沙和尚',421345678912345678,0478-44223333)
insert into cardInfo(cardID,savingType,balance,customerID)values
(2,1,'定期','1010 3576 1212 117',1,2)
insert into userInfo(customerName,PID,telephone)values
(3,'唐僧',321245678912345678,0478-44443333)
insert into cardInfo(cardID,savingType,balance,customerID)values
(3,'定期','1010 3576 1212 113',1,3)
update cardInfo set pass=611234 where cardID=1
select customerID from userInfo where customerID=1
select cardID from transInfo where cardID=1
insert into transInfo(transId,cardID,transType,transMoney)
values(1,1,'支取',200)
update cardInfo set balance=(balance-200) where cardID=1
select customerID from userInfo where customerID=2
select cardID from transInfo
insert into transInfo(transId,cardID,transType,transMoney)
values(2,2,'存入',300)
update cardInfo set balance=(balance+300) where customerID=2
select IsReportLoss from cardInfo where customerID=3
update cardInfo set IsReportLoss='是' where customerID=3
select*from cardInfo where openDate='(2021-03-09)-(2021-03-19)'



