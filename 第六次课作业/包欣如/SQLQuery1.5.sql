use master
go
create database house
on
(
name='house',
filename='D:\house.mdf',
size=5mb ,
maxsize=100mb ,
filegrowth=10%
)
log on
(
name='house_log',
filename='D:\house_log.ldf',
size=5mb ,
maxsize=100mb ,
filegrowth=10%
)
go
use house
go
create table tblUser--发布人信息表
(
userId int primary key identity(1,1),
userName nvarchar(10) not null,
userTel varchar(11) not null
)
create table tblHouseType--房屋的类型
(
typeId int primary key identity(1,1),
typName nvarchar check(typName='别墅'or typName='普通住宅'or typName='平房'or typName='地下室') not null,
)
create table tblQx --区县
(
qxId int primary key identity(1,1),
qxName nvarchar check(qxName='武昌'or qxName='汉阳'or qxName='汉口')
)
create table tblHouseInfo--房屋信息表
(
id int primary key identity(1,1),
hdesc  ntext,--房屋信息描述
userId int references tblUser(userId),
zj money not null,--租金
shi char(4) not null,--几室
ting char(2) not null, --几厅
typeId int references tblHouseType(typeId) not null,
qxId int references tblQx(qxId) not null
)