use bbs
go
create table bbsUsers   --用户信息表
(
UID int identity (1,1),   
uName varchar(10) not null ,  
uSex  varchar(2) not null,  
uAge  int not null,  
uPoint  int not null,  
)
go
create table bbsSection		--板块表
(
 sID  int identity, 
 sName  varchar(10) not null,
 sUid   int,  
)
use bbs
go
alter table bbsUsers add constraint PK_UID primary key(UID);
alter table bbsUsers add constraint UQ_uName unique(uName);
alter table bbsUsers add constraint CK_uSex check(uSex = '男' or uSex = '女');
alter table bbsUsers add constraint CK_uAge check(uAge>=15 and uAge<=60);
alter table bbsUsers add constraint CK_uPoint check(uPoint>=0);
alter table bbsSection add constraint CK_sID primary key (sID);
alter table bbsSection add constraint FK_sUid foreign key(sUid) references bbsUsers(UID)
use bbs
go
create table bbsTopic           --主贴表
(
tID  int primary key identity,
tUID  int references bbsUsers(UID),
tSID  int references bbsSection(sID),
tTitle  varchar(100) not null,
tMsg  text not null,
tTime  datetime  ,
tCount  int
)
go
create table bbsReply		--回贴表
(
rID  int primary key identity,
 rUID  int references bbsUsers(UID),
 rTID  int references bbsTopic(tID),
 rMsg  text not null,
  rTime  datetime 
)
