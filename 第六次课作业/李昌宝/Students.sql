create database Students
on primary
(
name = 'Students_DB',
filename = 'E:\sql sever.mdf',
size = 5,
maxsize = 500,
filegrowth = 10%
)
log on
(
name = 'Students_Log',
filename = 'E:\sql sever.ldf',
size = 5,
maxsize = 500,
filegrowth = 10%

)
use Students
go
create table Class
(
classid int primary key identity,
classname varchar(10) not null unique ,
classtime datetimeoffset not null,
classaa text
)
go
create table Student 
(
studentname char(2) unique,
studentsex  varchar(2) check(studentsex = '��' or studentsex = 'Ů') default('��') not null,
studentage char(5) check(studentage>15 and studentage<40),
studentadd varchar(10) default('�����人'),
studentid text
)
go
create table coure
(
coureid int primary key identity,
courename varchar(10) not null unique,
courems text
)
go
create table score
(
scoreid int primary key identity,
scorecj varchar(5) check(scorecj >1 and scorecj<100 )  ,
scorecjbh varchar(10) not null,
scorekcbj int references  coure(coureid)
)