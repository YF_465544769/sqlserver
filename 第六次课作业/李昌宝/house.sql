use house
go
create table tblUser    --个人信息表
(
useld varchar(50) not null unique,
userName  varchar(10) not null,
userTel	varchar(30) not null
)
go
create table tblHouseType  --房屋类型表
(
typeId int unique,
typName text
)
go
create table tblQx 
(
qxId int,
qxName  varchar(2) check(qxName='武昌' or qxName='汉阳' or qxName  ='汉口')
)
create table tblHouseInfo
(
id int ,
fwms text,
zj int,
shi int ,
ting int,
typeId varchar(4) check(typeId='别墅' or typeId='普通住宅' or typeId='平方' or typeId='地下室'),
qxid int
)