use master 
go
create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
use bbs
  create table bbsUsers
 (
	UIDD  int identity(1,1) not null ,
	uName   nvarchar(10) not null,
	uSex   varchar(2) not null ,
	uAge  int not null,
	uPoint int not null 
 )
 alter table bbsUsers add constraint PK_bbsUsers_UIDD primary key(UIDD )
alter table  bbsUsers add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('女','男'))
alter table bbsUsers add constraint CK_bbsUsers_uAge  check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint  check(uPoint>=0)
  create table bbsTopic
 (
	tID   int primary key identity(1,1),
	tUID   int references  bbsUsers(UIDD),
	tSID  int references  bbsSection(sIDD),
	tTitle    nvarchar(100) not null,
	tMsg    text not null,
	tTime   datetime,  
	tCount  int 
 )
 create table bbsReply
 (
	rID     int primary key identity(1,1),
	rUID     int references bbsUsers(UIDD) ,
	rTID     int references  bbsTopic(tID),
	rMsg      text not null,
	rTime     datetime,   
 )
  create table bbsSection
 (
	sIDD    int identity(1,1),
	sName  nvarchar(10),
	sUid  int  , 
 )
 alter table bbsSection add constraint PK_bbsSection_sIDD primary key(sIDD)
 alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(UIDD)
 
 insert into bbsUsers(uName,uSex,uAge,uPoint) values ('小雨点','女','20','0'),('逍遥 ','男','18','4'),('七年级生','男','19','2')

 create table bbsPoint
 (
	ID nvarchar(10) ,
	point int 
 )
 
 --
 insert into bbsPoint(ID,point) select uName,uPoint from bbsUsers


 --
 select uName,uPoint into bbsUsers2 from bbsUsers 

 select * from bbsUsers

 insert into bbsSection(sName,sUid) values ('技术交流  ','1'),('  读书世界 ','3'),('生活百科','1'),('八卦区 ','3')
 insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values ('2','4','范跑跑  ','谁是范跑跑','2008-7-8 ','1'),
 ('3','1','.NET','与JAVA的区别是什么呀？','2008-9-1','2'),('1','3','今年夏天最流行什么','有谁知道今年夏天最流行什么呀？',' 2008-9-10','0')
 select * from bbsSection
 insert into bbsReply(rUID,rMsg, rTime) values ('1','你是','2020.1.2'),('3','爱狗','2020.2.15'),('2','还是能','2020.5.25')
delete bbsTopic where tUID =2
delete bbsReply where rUID =2
delete bbsSection where  sUid =2
delete bbsUsers where   UIDD =2
update bbsUsers set uPoint =(uPoint +10) where uName='小雨点'
delete bbsSection where sName ='生活百科'
delete bbsTopic where tSID  =3
 select * from bbsTopic
 select * from bbsUsers
 select * from bbsReply

 delete bbsReply where rID =1 or rID =2