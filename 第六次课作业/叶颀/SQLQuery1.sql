create database TestDB
on
(
	name='TestDB',
	filename='d:\temp\TestDB.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='TestDB_log',
	filename='d:\temp\TestDB_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go 
create table typeinfo
(
	typeld int primary key identity(1,1),
	typeName varchar(10) not null,

)
create table logininfo
(
	Loginld int primary key identity(1,1),
	LoginName varchar(10) not null unique,
	LoginPwd varchar(20) not null default(123456),
	SexSut bit not null default(1),
	LogBirthday date,
	Loghuiy varchar(30),
)
create database company
on
(
	name='TestDB1',
	filename='d:\temp\TestDB1.mdf',
	size=5,
	maxsize=50,
	filegtowth=10%
),
(
	name='TestDB2',
	filename='d:\temp\TestDB2.ndf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on 
(
	name='testDB_log ',
	filename='d:\temp\TestDB1_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go
create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null,
)
create table userinfo
(
	userNo int primary key identity(1,1) not null,
	userName varchar(10) unique not null check(len(userName)>4),
	userSex varchar(2) not null check(userSex='' or userSex='Ů'),
	userAge int check (userAge>=1 and userAge<=100)default(18)not null,
	userAddress varchar(50) default(''),
	userSection int references sectionInfo(sectionID),
)
CREATE table workinfo 
(
	workld int primary key identity(1,1),
	userld int references userinfo(userNo) not null,
	workTime datetime not null,
	workDesscription varchar(40) not null check(workDesscription='' or workDesscription=''  or workDesscription=''or  workDesscription='' or workDesscription='' ),

)
create database Student
use Student
go
create table classBan
(
	classid int primary key identity(1,1),
	className varchar(20) unique not null,
	classdate date,
	classMiao varchar(20)
)
create table classXue
(
		classID int primary key identity(1,1),
		className char(10) unique check(len(className)>2),
		classSex char(2) check(classSex='' or classSex='')default('')not null,
		classAge int check(classage>=15 and classAge<=40)not null,
		classAdd varchar(50) default('��'),
		classIDD int ,

)
create table classke 
(
	classIDa int primary key identity(1,1),
	classMeng varchar(20) not null unique,
	classkemang varchar(20)
)
create table classxin
(
	classIDaa int primary key identity(1,1),
	classcheng int references classXue(classID),
	classchengs int references classKe(classIDa),
	classcjb varchar(20) check(classcjb>=0 and classcjb<=100)
	 
)
create database xiaozhu
go 
use xiaozhu
create table tbiuser
(
	userid int primary key identity(1,1),
	userName varchar(20),
	userTel int default (123456),
)
create table tbiHouseType
(
	typeid int primary  key identity(1,1),
	typName char(10) check(typName='' ot typName='' or typName='' or typName=''),

)
create table tbiQx
(
	qxid int primary key identity (1,1),
	quName varchar(10) check(quName='' or quName='' or quName=''),


)
create table tbiHousetinfo
(
	id int primary key identity(1,1),

)
create table descID
(
	useID int primary key identity(1,1),
	zj money,
	shi varchar(10),
	ting varchar(10),
	typeid int references tbiHouseType(typeid),
	qxid int references tbiQx(qxid)
)
