create database bbs9
on
(
	name='bbs',
	filename='D:\E\bbs.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\E\bbs.ldf',
	size=5,
	maxsize=15,
	filegrowth=10%
)

go
use bbs9
create table bbsUsers
(
	bbbsUID int	identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null ,
	uAge  int not null ,
	uPoint  int not null 
)
go
alter table bbsUsers add constraint PK_bbbsUID primary key (bbbsUID)
alter table bbsUsers add constraint UK_uName unique(uName)
alter table bbsUsers add constraint CK_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_uAge check(uAge>14 and uAge<61 )
alter table bbsUsers add constraint CK_uPoint check(len(uPoint)>=0)
go
create table bbsSection
(
	bbssID  int identity(1,1),
	sName  varchar(10) not null,
	sUid   int
)

go
alter table bbsSection add constraint PK_bbssID primary key (bbssID)
alter table bbsSection add constraint FK_sUid foreign key (sUid) references bbsUsers(bbbsUID)
go
create table bbsTopic
(
	tID  int primary key identity(1,1),
	tUID  int foreign key references bbsUsers(bbbsUID),
	tSID  int foreign key references bbsSection(bbssID),
	 tTitle  varchar(100) not null,
	 tMsg  text,
	 tTime  datetime default(getdate()),
	 tCount  int
)

create table bbsReply
(
	rID  int primary key identity(1,1),
	rUID  int foreign key references bbsUsers(bbbsUID),
	rTID  int	foreign key references bbsTopic(tID),
	rMsg  text NOT NULL,
	rTime  datetime default(getdate())
)

go
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection (sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2 ,4,'范跑跑','谁是范跑跑 ',2008-7-8,1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(3 ,1,'.NET','与JAVA的区别是什么呀？ ',2008-9-1,2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(1 ,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀 ',2008-9-10,0)

insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,1,'不认识',2008-7-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,2,'没有区别',2008-9-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(2 ,2,'请百度',2008-9-12)

delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsReply where rUID=2
delete from bbsUsers where uName='逍遥'

update bbsUsers set uPoint=uPoint+10 where bbbsUID=1

select * from bbsUsers
delete bbsTopic where tID=3
delete bbsSection where bbssID=3

delete from bbsReply

--1、删除字段：删除主贴表的“回帖数量”字段	
alter table bbsTopic drop column tCount
--2、新增字段：用户表新增“手机号码”字段，并为该字段添加唯一约束，且长度为11，必填
alter table bbsUsers add telephone varchar(20) unique check(len(telephone)=11) not null
--3、修改字段：修改回帖表的“回帖消息”字段，数据类型改成varchar(200)
alter table bbsReply alter column rMsg varchar(200)
--4、删除约束：删除用户表的“积分”字段的检查约束
alter table bbsUsers drop CK_uPoint
--5、修改数据：修改用户“小雨点”的名字改成“小雪”，将所有用户的积分都增加100
update bbsUsers set uName='小雪' where bbbsUID=1
update bbsUsers set uPoint=100
--6、复制用户表的数据到新表bbsUser2
select * into bbsUser2 from bbsUsers
--7、删除bbsUser2的所有数据，写出两种方式的语句
truncate table bbsUser2
delete from bbsUser2