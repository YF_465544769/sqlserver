create database TestDB

go
use TestDB
go

create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)

create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName nvarchar(10) not null unique,
	LoginPwd varchar(20) not null default(123456),
	LoninSex char(2),
	LoninTime smalldatetime,
	LoninVIP nvarchar(10) 
)