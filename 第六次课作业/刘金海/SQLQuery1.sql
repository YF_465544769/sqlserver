use master 
go
create database TestDB
on
(
	name='TestDB',
	filename='D:\TestDB.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
)
log on
(
	name='TestDB_log',
	filename='D:\TestDB_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
use TestDB
 create table typeInfo
 (
	typeId int primary key identity(1,1),
	typeName  varchar(10) not null
 )
  create table loginInfo
 (
	LoginId int primary key identity(1,1),
	LoginName  nvarchar(10) not null unique ,
	LoginPwd varchar(20) not null default(123456),
	LoginSex  nvarchar(1) check(LoginSex in('Ů','��')) default('��'),
	LoginBir  date  
 )

 use master 
 go 
 create database company
 on(
 
	name='company',
	filename='D:\company.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
 )
 log on
(
	name='company_log',
	filename='D:\company_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
use company
go
create table  sectionInfo
(
	sectionID  int primary key identity(1,1),
	sectionName  varchar(10) not null
)
create table  userInfo
(
	userNo    int primary key identity(1,1) not null,
	userName    varchar(10) not null unique check(len(userName)>4),
	userSex    varchar(2)  not null check(userSex in('Ů','��')),
	userAge   int  not null check( userAge>=1 and userAge<=100),
	userAddress    varchar(50) default('����'),
	userSection int references sectionInfo(sectionID)
)
create table workInfo
(
	workId  int primary key identity(1,1) not null,
	userId  int foreign key references userInfo(userNo),
	workTime datetime not null,
	workDescription  varchar(40) check(workDescription in('�ٵ�','����','����','����','�¼�'))
)
use master 
go
create database Studen
 on(
 
	name='Studen',
	filename='D:\Studen.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
 )
 log on
(
	name='Studen_log',
	filename='D:\Studen_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
use Studen
go 
create table ClassInfo
(
	classid int identity(1,1) primary key,
	className nvarchar(10) not null unique,
	classtime datetime not null  ,
	classdes text
)
create table StudenInfo
(
	studennum int  primary key identity(1,1),
	studensex nvarchar(1) check(studensex in('Ů','��')) not null default('��'),
	studenage int not null check(studenage<=40 and studenage>=15),
	studenadd nvarchar(50) default('�����人'),
	classnumber int 
)
create table courseInfo
(
	courseId int identity(1,1) primary key,
	coruseName nvarchar(20) unique,
	corusedes text
)

create table performance
(
	perId int identity(1,1) primary key,
	perName int  not null ,
	corusedes int ,
	performanceadd int  check(performanceadd>=0 and performanceadd<=100)
)
use master 
go
create database house
 on(
 
	name='house',
	filename='D:\house.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
 )
 log on
(
	name='house_log',
	filename='D:\house_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
use house
go 
create table tblUser 
(
	userId int identity(1,1) primary key ,
	userName nvarchar(10) not null,
	userTel int not null check(len(userTel)=11)

)
create table tblHouseType  
(
	typeId int identity(1,1) primary key ,
	typName nvarchar(10) not null check(typName in ('����','��ͨסլ','ƽ��','������')),
)
create table tblQx   
(
	qxId int identity(1,1) primary key ,
	qxName nvarchar(10) not null check(qxName in ('���','����','����')),
)
create table tblHouseInfo--   
(
	id int identity(1,1) primary key ,
	desca  nvarchar(100) ,
	userId  int not null,
	zj  money not null,
	shi nvarchar(20),
	ting  nvarchar(10),
	typeId int ,
	qxId int 
)
