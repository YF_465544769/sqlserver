use master
go
create database company
on
(
name='company',
filename='D:\SQLcunchu\company.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='company_log',
filename='D:\SQLcunchu\company_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go 
use company
go
create table sectionInfo
(
 sectionID int primary key identity(1,1),
 sectionName varchar(10) not null
)
create table userInfo
(
userNo int primary key not null identity(1,1),
userName varchar(10) unique not null check(userName>4),
userSex varchar(2) not null check(userSex in ('��','Ů')),
userAge int not null check(userAge<=100 AND userAge>=1),
userAddress varchar(50) DEFAULT('����'),
userSection int references sectionInfo( sectionID)
)
create table workInfo
(
 workId int primary key not null identity(1,1),
 userId int references userInfo(userNo) not null,
 workTime datetime not null,
 workDescription varchar(40) not null check( workDescription='�ٵ�'or workDescription='����'or workDescription='����'or workDescription='����'or  workDescription='�¼�')
)
