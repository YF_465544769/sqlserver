create database Company
on primary
(
name = 'Company_DB',
filename = 'D:\SQL  server.mdf',
size =5,
maxsize = 500,
filegrowth = 5
)
log on
(
name = 'Company_log',
filename = 'D:\SQL  server.ldf',
size =5,
maxsize = 500,
filegrowth = 5
)
use ksk
go
create table sectionInfo
(
sectionID  int identity primary key,
sectionName  varchar(10) not null
)
go
create table userInfo
(
userNo  int primary key identity not null,
userName  varchar(10) unique not null check(userName>4),
userSex   varchar(2) not null check(userSex = '��' or userSex = 'Ů'),
 userAge   int not null check(userAge>1 and userAge<100),
 userAddress  varchar(50) default('����'),
  userSection  int references sectionInfo( sectionID)
)

go
create table workInfo
(
 workId int identity primary key not null,
 userId  int references userInfo(userNo) not null,
 workTime datetime not null,
 workDescription  varchar(40) check(workDescription = '�ٵ�' or workDescription = '����' or workDescription = '����' or workDescription = '����'or workDescription = '�¼�')
)