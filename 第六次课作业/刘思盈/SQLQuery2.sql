use master
go
create database bbs
on
(
name='bbs',
filename='D:\test\bbs.mdf'
)
log on
(
name='bbs_log',
filename='D:\test\bbs_log.ldf'
)
go
use bbs
go
create table bbsUsers
(
UID int  identity(1,1),
uName varchar(10) not null,
uSex  varchar(2) not null,
uAge  int not null,
 uPoint  int not null
)
alter table bbsUsers add constraint PK_UID PRIMARY KEY(UID)
alter table bbsUsers add constraint uName unique(uName)
alter table bbsUsers add constraint uSex check(uSex in('男','女'))
alter table bbsUsers add constraint uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint uPoint check(uPoint>=0)

create table bbsTopic
(
tID  int primary key identity(1,1),
tUID  int foreign key references bbsUsers(UID) ,
 tSID  int foreign key references bbsSection(sID) ,
 tTitle  varchar(100) not null,
 tMsg  text not null,
 tTime  datetime ,
 tCount  int
)
create table bbsReply
(
rID  int primary key identity(1,1),
rUID  int foreign key references bbsUsers(UID) ,
 rTID  int foreign key references bbsTopic(tID),
 rMsg  text not null,
 rTime  datetime 


)
create table bbsSection
(
sID  int identity(1,1),
sName  varchar(10) not null,
 sUid   int
)
create table bbsPoint
(
uName varchar(10) not null,
uPoint  int not null
)

alter table bbsSection add constraint PK_sID PRIMARY KEY(sID)
alter table bbsSection add constraint FK_sUid foreign key(sUid) references bbsUsers(UID)
 insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点',  '女'  ,20  ,0),('逍遥',    '男' , 18,  4),('七年级生',  '男', 19 , 2)
 insert into bbsPoint(uName,uPoint) select uName,uPoint from bbsUsers
 insert into bbsSection(sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区 ',3)
 insert into bbsTopic(tUID,tSID ,tTitle ,tMsg,tTime,tCount) values(2,4,'范跑跑' , '谁是范跑跑', 2008-7-8,1),(3,1,'.NET', ' 与JAVA的区别是什么呀？', 2008-9-1,2),(1,3,'今年夏天最流行什么 ',   ' 有谁知道今年夏天最流行什么呀？',2008-9-10  ,0)
 insert into bbsReply(rMsg,rTime,rUID) values('范跑跑是。。。',2008-8-8,3),('区别是。。。',2008-8-22,1),('流行。。。',2008-8-9,2)
 alter table bbsReply drop constraint FK__bbsReply__rUID__20C1E124
 alter table bbsTopic drop constraint FK__bbsTopic__tUID__1CF15040
 delete from bbsUsers where UID=2
 update  bbsUsers set uPoint=10 where UID=1 
 alter table bbsTopic drop constraint FK__bbsTopic__tSID__1DE57479
 delete from bbsSection where sName='生活百科'
 truncate table bbsReply