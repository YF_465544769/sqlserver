use master
go
create database TestDB
on
(
name='TestDB',
filename='D:\test\TestDB.mdf'
)
log on
(
name='TestDB_log',
filename='D:\test\TestDB_log.ldf'
)
go
use TestDB
go
create table typeInfo
(
typeId int primary key identity(1,1),
typeName varchar(10) not null 
)
create table loginInfo
(
LoginId int primary key identity(1,1),
LoginName char(10) not null unique,
LoginPwd char(20) not null default(123456),
logSex char(2) check(logSex in('��','Ů')),
LogBrithday date,
LogType nvarchar(10)
)
go
use master 
go
create database company
on
(
name='company',
filename='D:\test\company.mdf'
) 
log on 
(
name='company_log',
filename='D:\test\company_log.ldf'

)
go
use company
go
create table sectionInfo
(
sectionID  int primary key ,
sectionName  varchar(10) not null,

)
create table userInfo
(
userNo  int primary key identity(1,1) not null,
userName  varchar(10) unique not null check(userName>4 or userName<=10),
userSex   varchar(2) not null check(userSex='��'or userSex='Ů'),
userAge   int not null check(userAge>=1 and userAge<=100),
userAddress  varchar(50) default('����'),
userSection  int foreign key references sectionInfo(sectionID)

)
create table workInfo
(
workId int primary key identity(1,1) not null,
userId  int foreign key references sectionInfo(sectionID) not null,
workTime datetime not null,
workDescription  varchar(40) not null check(workDescription in('�ٵ�','����','����','����','�¼�'))
)
go
use master
go
create database Students
on
(
name='Students',
filename='D:\test\Students.mdf'
)
log on
(
name='Students_log',
filename='D:\test\Students_log.ldf'
)
go 
use Students
go
create table classinfo
(
 classid int primary key identity(1,1),
 classname nvarchar(10) not null unique,
 opentime date not null ,
 classdescribe nvarchar(50)
)
create table studentinfo
(
studentid int primary key identity(1,1),
studentname nvarchar(10) unique check(studentname>2 and studentname<=10),
studentsex nchar(2) default('��') check(studentsex in('��','Ů')) not null,
studentage int check(studentage>=15 and studentage<=40) not null,
studentaddress nvarchar(10) default('�����人'),
classid int 
)
create table courseinfo
(
courseid int primary key identity(1,1),
coursename nvarchar(10) not null unique,
coursedescribe nvarchar(10)
)
create table soreinfo
(
soreid int primary key identity(1,1),
studentid int not null,
courseid int not null,
sore int check(sore>=0 and sore<=100)
)
go
use master
go
create database house
on
(
name='house',
filename='D:\test\house.mdf'
)
log on
(
name='house_log',
filename='D:\test\house_log.ldf'
)
go
use house
go
create table tblHouseInfo
(
tblHouseid int ,
tblHousedescribe nvarchar(50),
tblHousedejz int,
tblHousedeshi int ,
tblHousedeting int ,
tblHousedetypeld nvarchar(10),
tblHousedeqxId nvarchar(10),

)
create table tblUser
(
userId int,
userTel int,
userName nvarchar(10),
tblHouseType nvarchar(10),
typeId int,
typName nvarchar(10) ,
tblQx nvarchar(5) ,
qxId int ,
qxName nvarchar(10),
)