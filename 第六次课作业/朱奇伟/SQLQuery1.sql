use master
create database TestDB
on
(
	name='TestDB',
	filename='D:\Demo\TestDB.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='TestDB_log',
	filename='D:\Demo\TestDB_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go


use TestDB
go
create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)

create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName  varchar(10) not null unique,
	LoginPwd varchar(20) not null default('123456'),
	LoginSex nchar(1),
	LoginBirthday date,
	LoginType text
)


create database company
on
(
	name='company',
	filename='D:\Demo\company.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
),
(
	name='company2',
	filename='D:\Demo\company2.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='company_log',
	filename='D:\Demo\company_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use company
create table sectionInfo
(
	sectionID int identity(1,1) primary key,
	sectionName varchar(10) not null
)
create table userInfo
(	
	userNo int identity primary key not null,
	userName varchar(10) unique not null check(len(userName)>4),
	userSex varchar(2) not null check(userSex='��' or userSex='Ů'),
	userAge int not null check(userAge>1 and userAge<100),
	userAddress varchar(50) default('����'),
	userSection int references sectionInfo(sectionID)
)
create table workInfo
(
	workId int identity(1,1) primary key,
	userId int references sectionInfo(sectionID) not null,
	workTime datetime not null,
	workDescription varchar(40) check(workDescription='�ٵ�' or workDescription='����' or workDescription='����') not null
)

create database Student
on
(
	name='Student',
	filename='D:\Demo\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\Demo\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student
create table classInfo
(
	classId int primary key identity(1,1),
	className varchar(10) unique not null,
	classTime date not null,
	classRepresent text 
)

create table studentInfo
(
	stuId int primary key identity(1,1),
	stuName varchar(10) unique check(Len(stuName)>2) not null,
	stuAge int check(stuAge>15 and stuAge<40) not null,
	stuAddress text default('�����人'),
	classId int references classInfo(classId)
)
create table Course
(
	courseId int primary key identity(1,1),
	courseName varchar(10) not null,
	courseRepresent text
)
create table result
(
	resultId int primary key identity(1,1),
	stuId int references studentInfo(stuId),
	courseId int references Course(courseId),
	result int check(result>0 and result<100)
)


create database House
on
(
	name='House',
	filename='D:\Demo\House.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(	name='House_log',
	filename='D:\Demo\House_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use House

create table tblUser
(
	userId int identity(1,1) primary key,
	userName varchar(10) not null,
	userTel varchar(11) not null
)
create table tblHouseType
(
	typeId int identity(1,1) primary key, 
 	typName varchar(10) not null
)
create table tblQx
(
	qxId int identity(1,1) primary key,
	qxName varchar(10) not null
)
create table tblHouseInfo
(
	id int identity(1,1),
	houseDesc text ,
	userId  int references tblUser(userId),
	zj  int ,
	shi varchar(10),
	ting varchar(10),
	typeId int references tblHouseType(typeId),
	qxId int references tblQx(qxId)
)