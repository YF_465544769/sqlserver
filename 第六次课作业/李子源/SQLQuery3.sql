create database 房屋出租系统
on
(
	name='房屋出租系统',
	filename='D:\房屋出租系统.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(
	name='房屋出租系统_log',
	filename='D:\房屋出租系统_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
use   房屋出租系统
go
create table tblUser(
	userId int primary key identity(1,1),
	userName varchar(10) check(userName>2) not null,
	userTel int not null
)
create table tblHouseType (
	typeId int primary key identity(1,1),
	typName text not null,
)
create table tblQx (
	qxId int primary key identity(1,1),
	qxName text not null,
)
create table tblHouseInfo(
	id int primary key identity(1,1),
)