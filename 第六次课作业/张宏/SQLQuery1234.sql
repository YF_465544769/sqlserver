use master
go

create database company
go

use company
go
create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null
)
go
use company
go
create table userInfo
(
	userNo int primary key identity(1,1) not null,
	userName varchar(10) unique not null check(userName>4),
	userSex varchar(2) check(userSex=' ' or userSex='1') default(' ') not null,
	userAge int check(userAge>0 or userAge<100) not null,
	userAddress varchar(50) default('����') not null,
	userSection int foreign key references sectionInfo(sectionID)
)
go
use company
go
create table workInfo
(
	workId int primary key identity(1,1) not null,
	userId int foreign key references userInfo( userNo),
	workTime datetime not null,
	workDescription varchar(40) check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�') not null
)

use master
go

create database TestDB
go

use TestDB
go
create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)
go
use TextDB
go
create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName varchar(10) not null unique,
	LoginPwd varchar(20) not null  default(123456),
	LoginSex char(2) check(LoginSex=' ' or LoginSex='0') default(' ') not null,
	Loginbrithday varchar(2),  
	Category nvarchar(10)

)

create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
	)

log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
	)
go
use Student
go

create table ClassInfo
(
	ClassId int primary key identity(1,1),
	ClassName int unique not null,
	OpenTime int not null,
	ClassDescribe text
)

go
use Student 
go

create table StudentInfo
(
	StudentId int primary key identity(1,1),
	StudentName varchar(10) check(StudentName>2) unique,
	StudentSex varchar(2) check(StudentSex=' ' or StudentSex='1') default(' ') not null,
	StudentAge int check(StudentAge>15 or StudentAge<40) not null,
	StudentAddress  text default('�����人'),
	StudentClassId int references ClassInfo(ClassId),
)

go
use Student 
go

create table CourseInfo
(
	CourseId int primary key identity(1,1),
	CourseName varchar(20) unique not null,
	CoueseDescribe text

)


go
use Student 
go

create table PerformanceInfo
(
	 PerformanceId int primary key identity(1,1),
	PerformanceStudentNumber int references StudentInfo(StudentId) not null,
	PerformanceClassNumber int references CourseInfo(CourseId) not null,
	performance int check(performance>0 or performance<100)
)


create database Renting
on

(
	name='Renting',
	filename='D:\Renting.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
	)

log on
(
	name='Renting_log',
	filename='D:\Renting_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
	)

go
use Renting
go

create table tblUser 
(
	userId int primary key identity(1,1),
	userName varchar(10) not null,
	userTel text
)

go
use Renting
go

create table TblHouseType
(
	typeId int primary key identity(1,1),
	typName varchar(10) not null
)



go
use Renting
go

create table tblQx
(
	qxId int primary key identity(1,1),
	qxName varchar(10) not null
)

go
use Renting
go

create table tblHouseInfo
(
	tblHouseId int primary key identity(1,1),
	Resc text,
	tblHouseuserId int references tblUser(userId),
	ZJ int not null,
	Howroom int not null,
	Howhall int not null,
	DesctypeId  int  references TblHouseType(typeId),
	DescqxId int  references tblQx(qxId) 
	)
