create database TestDB
on
(
	NAME='TestDB_data',
	FILENAME='D:\temp\TestDB.mdf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%
)
log on
(
	NAME='TestDB_log',
	FILENAME='D:\temp\TestDB_log.ldf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%

)
go

create table typeinfo
(
	typeld int primary key identity(1,1),
	typeName varchar(10) not null,

)

create table logininfo
(
	Loginld int primary key identity(1,1),
	LoginName varchar(10) not null unique,
	LoginPwd varchar(20) not null default(123456),
	SexSut bit not null default(1),
	LogBirthday date,
	Loghuiy varchar(30),
)

create database company
on
(
	NAME='TestDB1_data',
	FILENAME='D:\temp\TestDB1.mdf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%
),
(
	NAME='TestDB2_data',
	FILENAME='D:\temp\TestDB2.ndf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%
)
log on
(
	NAME='TestDB_log',
	FILENAME='D:\temp\TestDB1_log.ldf',
	SIZE=5,
	MAXSIZE=50,
	FILEGROWTH=10%

)
go

create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null,

)

create table userinfo
(
	userNo int primary key identity(1,1) not null,
	userName varchar(10) unique not null check(len(userName)>4),
	userSex varchar(2) not null check(userSex='��' or userSex='Ů'),
	userAge int check(userAge >=1 and userAge <= 100) default(18) not null,
	userAddress varchar(50) default('����'),
	userSection int references sectionInfo(sectionID),

)

create table workinfo
(
	workld int primary key identity(1,1),
	userld int references userinfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) not null check(workDescription='�ٵ�'or workDescription='����' or workDescription='��' or workDescription='����' or workDescription='�¼�'),

)

create database Student
use Student
go
create table classBan
(
	classid int primary key identity(1,1),
	className varchar(20) unique not null,
	classdate date,
	classMiao varchar(20)
)
create table classXue
(
	classID int primary key identity(1,1),
	className char(10) unique check(len(className)>2),
	classSex char(2) check(classSex='��'or classSex='Ů') default('��') not null,
	classAge int check(classAge>=15 and classAge<=40) not null,
	classAdd varchar(50) default('�����人'),
	classIDD int,

)
create table classKe
(
	classIDa int primary key identity(1,1),
	classMeng varchar(20) not null unique,
	classkemang varchar(20)
)
create table classxin
(
	classIDaa int primary key identity(1,1),
	classcheng int references classXue(classID),
	classchengs int references classKe(classIDa),
	classcjb varchar(20) check(classcjb>=0 and classcjb<=100)
)
create database xiaozhu
go
use xiaozhu
create table tbiUser
(
	userid int primary key identity(1,1),
	userName varchar(20),
	userTel int default(123456),
)
create table tbiHouseType
(
	typeid int primary key identity(1,1),
	typName char(10) check(typName='����'or typName='��ͨסլ' or typName='ƽ��' or typName='������'),
)
create table tblQx
(
	qxid int primary key identity(1,1),
	qxName varchar(10)  check(qxName='���'or qxName='����' or qxName='����'),
)
create table tbiHouseinfo
(
	id int primary key identity(1,1),
)
create table descID
(
	userID int primary key identity(1,1),
	zj money,
	shi varchar(10),
	ting varchar(10),
	typeid int references tbiHouseType(typeid),
	qxid int references tblQx(qxid)
)
