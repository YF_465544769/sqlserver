use master
go

create database TestDB
on
(
	name=TestDB,
	filename='D:\TestDB.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=TestDB_log,
	filename='D:\TestDB_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
use TestDB
go
create table typeInfo(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)

create table loginInfo(
	Loginld int primary key identity(1,1),
	LoginName nvarchar(10) unique not null,
	LoginPwd varchar(20) default(123456) not null ,
	Sex nvarchar(1) check(Sex='��'or Sex='Ů'),
	Brithday date,
	MemType nvarchar(10)
)
select * from loginInfo
select * from typeInfo




-----------------------------------------------------------------------------------------------------------
use master
go
create database company
on(
	name=company,
	filename='E:\company.mdf',
	size=10MB,
	filegrowth=10%
)
log on(
	name=company,
	filename='E:\company.ldf',
	size=10MB,
	filegrowth=10%
)

use company
go
create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null
)

create table userInfo
(
	userNo int primary key identity(1,1) not null,
	userName varchar(10) check(userName>4)not null ,
	userSex varchar(2) check(userSex='��' or userSex='Ů') not null,
	userAge int check(userAge>0 and userAge<100) not null,
	userAddress varchar(50) default('����'),
	userSection int references sectionInfo(sectionID)
)
create table workInfo
(
	workId int primary key identity(1,1) not null,
	userId  int references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription  varchar(40) check(workDescription in('�ٵ�','����','����','����','�¼�'))
)

select * from sectionInfo
select * from userInfo
select * from workInfo




--------------------------------------------------------------------------------------------------------------------
use master 
go

create database Stumin
on(
	name=Stumin,
	filename='E:\Stumin.mdf',
	size=10MB,
	filegrowth=10%
)
log on(
	name=Stumin,
	filename='E:\Stumin.ldf',
	size=10MB,
	filegrowth=10%
)
use Stumin
go

create table classInfo
(	
	ClassID int primary key identity(1,1), 
	ClassName nvarchar(10) unique not null,
	StratTime date not null,
	ClassDes text
)

create table StuMes
(
	StuNo int primary key identity(1,1),
	StuName nvarchar(10) check(StuName>2) not null,
	StuSex nvarchar(2) check(StuSex in('��','Ů')) default('��'),
	StuAge int check(StuAge>=15 and StuAge<=40) not null,
	StuAdd nvarchar(20) default('�����人'),
	ClassNo int
)
create table Course
(
	CourseNo int primary key identity(1,1),
	CourseName nvarchar(10) unique not null,
	CourseDes text
)
create table ScoreInfo
(
	ScoreNo int primary key identity(1,1),
	StuNo int not null,
	CourseNo int not null,
	Score int check(Score>=0 and Score<=100)
)




---------------------------------------------------------------------------------------------------------------------------
create database HouseRent
on(
	name=HouseRent,
	filename='E:\HouseRent.mdf',
	size=10MB,
	filegrowth=10%
)
log on(
	name=HouseRent,
	filename='E:\HouseRent.ldf',
	size=10MB,
	filegrowth=10%
)

use HouseRent
go

create table tblUser
(
	userId int primary key identity,
	userName nvarchar(10),
	userTel varchar(50)
)
create table tblHouseType
(
	typeId int primary key identity,
	typName nvarchar(10)
)
create table tblQx
(
	qxId int primary key identity,
	qxName nvarchar(10)
)
create table tblHouseInfo
(
	id int primary key identity,
	desc01 text,
	userId int,
	zj money,
	shi nvarchar(10),
	ting nvarchar(10),
	typeId int references tblHouseType(typeId),
	qxId int references tblQx(qxId)
)


--------------------------------------------------------------------------------------------------------------------------