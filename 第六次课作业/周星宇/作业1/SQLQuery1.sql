use master
go

create database TestDB
on
(
   name='TestDB' ,
   filename='E:\新建文件夹\作业\SQL\TestDB.mdf',
   size=5MB,
   maxsize=50MB,
   filegrowth =10MB
)
log on
(
   name='TestDB_log' ,
   filename='E:\新建文件夹\作业\SQL\TestDB_log.ldf',
   size=5MB,
   maxsize=50MB,
   filegrowth =10MB
)
go

use master 
go

create table typeInf
(
    typeId int primary key identity(1,1) not null,
	typeName varchar(10) not null
)


use master 
go

create table loginInfo
(
    LoginId int primary key identity(1,1) not null,
	LoginName nvarchar(10) not null unique,
	LoginPwd nvarchar(20) not null default('123456'),
	sex char(2) check(sex='男' or sex='女') not null,
	birthday date,
	member int,
)