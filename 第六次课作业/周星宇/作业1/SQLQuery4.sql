use master
go

create database Houserental 
on
(
     name='Houserental',
	 filename='E:\新建文件夹\作业\SQL\Houserental.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='Houserental_log',
	 filename='E:\新建文件夹\作业\SQL\Houserental_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go

use master 
go

create table tblUser 
(
     userId int primary key identity(1,1) not null,
	 userName nvarchar(5) not null,
	 userTel nvarchar(11) unique  check(len(userTel)=11) not null,
)


use master 
go

create table tblHouseType 
(
     typeId int primary key identity(1,1) not null,
	 typName nvarchar(10) not null
)


use master 
go

create table tblQx 
(
      qxId int primary key identity(1,1) not null references tblHouseType(typeId),
	  qxName varchar(50) unique not null              
)


use master 
go

create table tblHouseInfo
(
      id int primary key identity(1,1) not null references tblUser(userId),
	  Housedesc nvarchar(50),
	  userId  int references tblUser(userId) unique not null,
	  zj nvarchar(10) not null,
	  shi nvarchar(10) not null,
	  ting nvarchar(10) not null,
	  typeId int references tblHouseType(typeId),
	  qxId int references tblQx(qxId)
)