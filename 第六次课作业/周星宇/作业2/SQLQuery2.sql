use master
go

create database bbs
on
(
     name='bbs',
	 filename='E:\新建文件夹\作业\SQL\bbs.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='Houserental_log',
	 filename='E:\新建文件夹\作业\SQL\bbs_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go

use master 
go

create table bbsUsers
(
     UID int primary key identity(1,1) not null,
	 uName varchar(10) unique not null,
	 uSex varchar(2) check(uSex='男' or uSex='女') not null,
	 uAge  int not null  check(uAge>=15 and uAge<=60), 
	 uPoint  int not null check(uPoint>=0)
)


use master 
go

create table bbsTopic
(
     tID int primary key identity(1,1) not null,
	 tUID int references bbsUsers(UID),
	 tSID int references bbsSection(sID),
	 tTitle varchar(100) not null,
	 tMsg text not null,
	 tTime  datetime,
	 tCount  int
)


use master 
go

create table bbsReply
(
     rID int primary key identity(1,1) not null,
	 rUID int references bbsUsers(UID),
	 rTID int references bbsTopic(tID),
	 rMsg text not null,
	 rTime  datetime
)


use master 
go

create table bbsSection
(
     sID int primary key identity(1,1) not null,
	 sName varchar(10) not null,
	 sUid int references bbsUsers(UID)
)




insert into bbsUsers(uName,uSex,uAge,uPoint)
select'小雨点','女','20','0' union
select'逍遥','男','18',' 4' union
select'七年级生','男','19','2'


select uName uPoint into  bbsPoint from bbsUsers
go


insert into bbsSection(sName,sUid)
select'技术交流','3'union
select'读书世界','1'union
select'生活百科','3'union
select'八卦区','1'


insert into bbsTopic(tID,tUID,tSID,tTitle,tMsg,tTime,tCount)
select '2','1','范跑跑','谁是范跑跑','2008-7-8','1'union
select '1','3','.NET','与JAVA的区别是什么呀？','2008-9-1','2'union
select '3','4','今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10','0'


insert into bbsReply(rID,rUID,rTID,rMsg,rTime)
select '1','不知道','2008-9-5','2'union
select '2','不认识','2008-9-10','1'union
select '3','不知道','2008-10-1','3'


alter table bbsReply drop constraint FK_bbsReply_rUID
alter table bbsTopic drop constraint FK_bbsTopic_tUID
alter table bbsSection drop constraint FK_bbsSection_sUid
alter table bbsUsers drop constraint PK_bbsUsers_UID

delete from bbsUsers where UID='2'


update bbsPoint set uPoint=(uPoint + 10) where uName='小雨点'


delete from bbsReply where rTID=3
delete from bbsTopic where tSID=4
delete from bbsSection where sIDD=4


delete from bbsReply where rTID=1 or rTID=2