use master
	go

	create database bbs
	on
	(
		name='bbs',
		filename='D:\DATA\bbs.mdf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	log on
	(
		name='bbs_log',
		filename='D:\DATA\bbs_log.ldf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	go

	use bbs
	go
	create table bbsUser
	(
		UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)

	alter table bbsUser add constraint PK_bbsUser_UID primary key(UID)
	alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex in('男','女'))
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge between 15 and 60)
	alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)
	create table bbsTopic
	(
		tID  int primary key identity,
		tUID  int references bbsUser(UID),
		tSID  int references bbsSection(sID),
		tTitle  varchar(100) not null,
		tMsg  text not null,
		tTime  datetime,
		tCount  int
	)

	create table bbsReply
	(
		rID  int primary key identity,
		rUID  int references bbsUser(UID),
		rTID  int references bbsTopic(tID),
		rMsg  text not null,
		rTime  datetime 
	)
	create table bbsSection
	(
		sID  int  identity,
		sName  varchar(10) not null,
		sUid   int 
	)
	create table bbsUser2
	(
	UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)
	alter table bbsSection add constraint PK_bbsSection_sID primary key(sID) 
	alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)
	insert into bbsUser values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
	select * from bbsUser
	select uName,uPoint into bbsPoint from bbsUser 
	insert into bbsPoint select uName,uPoint from bbsUser 
	insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

	select * from bbsSection
	insert into bbsTopic values (2,5,'范跑跑','谁是范跑跑','2008-7-8',1)
	insert into bbsTopic values (3,2,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)

	select * from bbsTopic
	insert into bbsReply values (1,2,'范跑跑是谁我也不知道',GETDATE()),(1,2,'JAVA是什么？？？',GETDATE()),
	(1,2,'范跑跑是谁我也不知道22222',GETDATE())
	delete from bbsReply where rTID=1	
	delete from bbsReply where rUID=2	
	delete from bbsTopic where tUID=2
	delete from bbsUser where UID=2
	
	select * from bbsUser
	select * from bbsReply where rTID=1
	select * from bbsTopic where tUID=2
	select * from bbsUser where UID=2
	update bbsUser set uPoint= uPoint+10 where UID=1
	delete from bbsReply 
	truncate table bbsReply 
	drop table bbsReply 
	alter table bbsTopic drop column  tCount
	alter table bbsUsers add telephone int unique check( len(telephone)=13) not null
	alter table  bbsReply alter column rMsg varchar(200) not null
	update bbsUsers  set  uName='小雪' where uName='小雨点'
	update bbsUsers  set   uPoint= uPoint+100 where uName='小雪'
	insert into bbsUser2(UID, uName,uSex,uAge, uPoint) select UID, uName,uSex,uAge, uPoint  from bbsUsers
	truncate table bbsUser2
	drop table bbsUser2