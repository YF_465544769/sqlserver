create database orders
go
use orders
go
create table orders			--订单表
(
	orderId int primary key,  --订单编号
	orderDate smalldatetime		--订购日期
)
go
create table orderItem		--订购项目表
(
	itemName nvarchar(10) not null,		--产品名称
	theNumber  int not null,		--订购数量
	theMoney money not null,		--订购单价
	itemType nvarchar(4) check(itemType in('文具','体育用品','日常用品')) not null,--产品类别
	orderId	int references	orders(orderId)	not null,		--订单编号
	ItemiD int identity(1,1) primary key,		--项目编号
)
insert into orders(orderId,orderDate) values(1,'2008-01-12'),(2,'2008-02-10'),(3,'2008-02-15'),(4,'2008-03-10')
insert into orderItem(orderId,itemType,itemName,theNumber,theMoney)
select 1,'文具','笔',72,2 union
select 1,'文具','尺',10,1 union
select 1, '体育用品','篮球',1,56 union
select 2,'文具','笔',36,2 union
select 2,'文具','固体胶',20,3 union
select 2,'日常用品','透明胶',2,1 union
select 2,'体育用品','羽毛球',20,3 union
select 3,'文具','订书机',20,3 union
select 3,'文具','订书机',10,3 union
select 3,'文具','裁缝刀',5,5 union
select 4,'文具','笔',20,2 union
select 4,'文具','信纸',50,1 union
select 4,'日常用品','毛巾',4,5 union
select 4,'日常用品','透明胶',30,1 union
select 4,'体育用品','羽毛球',20,3 
select sum(theNumber) as 所有物品数量 from orderItem 

select orderId,AVG(theMoney) from orderItem where orderId<3 group by orderId having AVG(theMoney)<10

select orderId,itemName,theNumber,AVG(theMoney) from orderItem where orderId<3 group by orderId,itemName,theNumber having AVG(theMoney)<10 and sum(theNumber)>50

select itemType,count(itemType)订购次数 from orderItem group by itemType

select itemName,avg(theMoney)平均价格,sum(theNumber)订购总数 from orderItem group by itemName having sum(theNumber)>100

select itemName as 文具类型,count(itemType)订购次数,sum(theNumber)订购总数,avg(theMoney)平均价格 from orderItem group by itemName

select * from orderItem


