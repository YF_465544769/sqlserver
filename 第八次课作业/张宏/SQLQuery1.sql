use master
go

create database DEMO
on
(
	name='DEMO'
	filename='D:\bank\Demo3.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb,
)

log on
(
	name='DEMO_log'
	filename='D:\bank\Demo3_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb,
)
go

use DEMO
go

create table orders
(
	orderId int primary key identity(1,1),
	orderDate datetime default(getdate()),
)

create table orderItem
(
	ItemiD int primary key ,
	orderId int references orders(orderId),
	itemType nvarchar(10) not null,
	itemName nvarchar(20) not null,
	theNumber varchar(20) not null,
	theMoney money not null,
)

insert into orders(orderId,orderDate) values(1,2008-01-12),(2,2008-02-10),(3,2008-02-15),(4,2008-03-10)
insert into orders values(1,1,'文具','笔','72',2)
insert into orders values(2,1,'文具','尺','10',1)
insert into orders values(3,1,'体育用品','篮球','1',56)
insert into orders values(4,2,'文具','笔','36',2)
insert into orders values(5,2,'文具','固体胶','20',3)
insert into orders values(6,2,'日常用品','透明胶','2',1)
insert into orders values(7,2,'体育用品','羽毛球','20',3)
insert into orders values(8,3,'文具','订书机','20',3)
insert into orders values(9,3,'文具','订书机','10',3)
insert into orders values(10,3 ,'文具','裁纸刀','5',5 )
insert into orders values(11 ,4 ,'文具','笔','20',2 )
insert into orders values(12, 4,'文具','信纸','50', 1)
insert into orders values(13 ,4 ,'日常用品','毛巾','4', 5)
insert into orders values(14 , 4,'日常用品','透明胶','30',1 )
insert into orders values( 15,4 ,'体育用品','羽毛球','20',3 )


select * from orders
select * from orderItem


--1.查询所有订单订购的所有物品数量总和
select sum(orderId) from orders

--2.查询 每个订单订购的所有物品的 数量和 以及 平均单价       订单编号小于3的，平均单价小于10
select orderId,avg(theMoney)平均单价,sum(theNumber) from orderItem where orderId<3 group by orderId having avg(theMoney)<10

--3.查询 每个订单订购的所有物品 数量和 以及 平均单价 平均单价小于10并且总数量大于 50 
select theMoney,avg(theMoney)平均单价 from orderItem where avg(theMoney)<10 group by theMoney having avg(theNumber)>50

--4.查询每种类别的产品分别订购了几次，
--不会

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType,avg(theMoney)平均单价 from orderItem where sum(theNumber)>100 

--6.查询每种产品的订购次数，订购总数量和订购的平均单价
--不会