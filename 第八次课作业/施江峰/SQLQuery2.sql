create  database ordersInfo
on
(
	name='ordersInfo',
	filename='D:\ordersInfo.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
log on
(
	name='ordersInfo_log',
	filename='D:\ordersInfo_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
use ordersInfo
go

create table orders
(
  orderId int primary key  identity(1,1)  not null , 
  orderDate datetime 
)

create table orderDate
(
	ItemiD int ,
	orderId int   foreign key references orders(orderId),
    itemType  nvarchar(20) not null,
	itemName   nvarchar(20) not null ,
	theNumber  int   not null,
	theMoney   money not null,
)
go 
insert orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
insert  orderDate(ItemiD,orderId,itemType,itemName,theNumber,theMoney) values
(1,1,'文具','笔',72,2),(2,1,'文具','尺子',10,1),
(3,1,'体育用品','篮球',1,36),(4,2,'文具','笔',36,2),
(5,2,'文具','固体胶',20,3),(6,2,'日常用品','透明胶',2,1),
(7,2,'体育用品','羽毛球',20,3),(8,3,'文具','订书机',20,3),
(9,3,'文具','订书钉',20,3),(10,3,'文具','裁纸刀',5,5),
(11,4,'文具','笔',20,2),(12,4,'文具','信纸',50,1),
(13,4,'日常用品','毛巾',4,5),(14,4,'日常用品','透明胶',30,1),
(15,4,'体育用品','羽毛球',20,3)

--查询所有订单订购的所有物品数量总和
select SUM(theNumber)物品总和 from dbo.orderDate
--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价

select orderId,sum(theNumber)数量和,avg(theMoney)平均单价 from orderDate where 
orderId<3 group by orderId having avg(theMoney)<10
--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId,sum(theNumber)数量和,AVG(theMoney)平均单价 from orderDate 
group by orderId having AVG(theMoney)<10 and sum(theNumber)>50

--4.查询每种类别的产品分别订购了几次，例如：
--					文具      9
--                    体育用品  3
--                    日常用品  3
select itemType 类别,COUNT(orderId)次数 from orderDate group by itemType

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType 类别,sum(theNumber)数量和,AVG(theMoney)平均单价
 from orderDate group by itemType having sum(theNumber)>100


--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：
select itemName 产品名称,SUM(theNumber)数量和,AVG(theMoney)平均单价
from orderDate group by itemName
--  产品名称   订购次数  总数量   平均单价 
--    --笔           3       120       2