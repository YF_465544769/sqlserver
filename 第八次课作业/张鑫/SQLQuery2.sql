use master
go

create database Student
on
(
	name='Stuinfo',
	filename='D:\Stuinfo.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='Stuinfo_log',
	filename='D:\Stuinfo_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
go
use Student

create table Stuinfo
(	
	stuNO varchar(5) unique not null,
	stuName nvarchar(20),
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='男' or stuSex='女')
)
go

create table Scoreinfo
(
	examNO int primary key identity(1,1),
	stuNO varchar(5) references Stuinfo(stuNO),
	writtenExan int,
	labExam int,
)

insert into Stuinfo values ('s2501','张秋利','20','美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')

insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)
select stuAddress 地区,AVG(stuage)平均年龄 from Stuinfo group by stuAddress
select stuSex,sum(stuage) 总和 from Stuinfo group by stuSex
select stuAddress,stuSex 地区,avg(stuage)平均年龄,sum(stuage)总和 from Stuinfo group by stuAddress
select stuAddress,stuSex,sum(stuage)平均年龄,avg(stuage)总和 from Stuinfo group by stuAddress,stuSex 


