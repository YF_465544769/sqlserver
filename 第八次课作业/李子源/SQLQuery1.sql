create  database  bbs
on(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)

log on
(
   name='bbs_log',
	filename='D:\bbs_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
go 
use bbs
go
create table orders
(
	orderId int primary key not null,
	orderDate datetime 
)
create table orderItem
(
	ItemID int primary key identity not null,
	orderId int not null,
	itemType nvarchar not null,
	itemName nvarchar not null,
	theNumber int not null,
	theMoney int not null
)
insert orders values
(1,'2008-01-12'),(2,'2008-02-10'),
(3,'2008-02-15'),(4,'2008-03-10')
insert orderItem values
(1,1,'文具','笔',72,2),
(2,1,'文具','尺',10,1),
(3,1,'体育用品','篮球',1,56),
(4,2,'文具','笔',36,2),
(5,2,'文具','固体胶',20,3),
(6,2,'日常用品','透明胶',2,1),
(7,2,'体育用品','羽毛球',20,3),
(8,3,'文具','订书机',20,3),
(9,3,'文具','订书针',10,3),
(10,3,'文具','裁纸刀',5,5),
(11,4,'文具','笔',20,2),
(12,4,'文具','信纸',50,1),
(13,4,'日常用品','毛巾',4,5),
(14,4,'日常用品','透明胶',30,1),
(15,4,'体育用品','羽毛球',20,3)
go

select SUM(theNumber) from dbo.orderItem
go
select AVG(theMoney) as 平均单价 from orderItem where orderId<3 HAVING AVG(theMoney)<10
go 
select SUM(theNumber) as 数量 from orderItem where orderId<3 HAVING AVG(theMoney)<10
go
select SUM(theNumber) as 数量 from orderItem where theNumber>50 HAVING AVG(theMoney)<10
go
select AVG(theMoney) as 平均单价 from orderItem where theNumber>50 HAVING AVG(theMoney)<10
go
select '文具' ,COUNT(itemType) from orderItem where itemType = '文具'union
select '体育用品',COUNT(itemType) from orderItem where itemType = '体育用品'union
select '日常用品',COUNT(itemType) from orderItem where itemType = '日常用品'

select '文具' , SUM(theNumber) as 数量,AVG(theMoney) as 平均单价 from orderItem where theNumber>100 and itemType = '文具'

select itemName as '产品名称',COUNT(itemName) as '订购次数',SUM(theNumber) as 总数量, AVG(theMoney) as '平均单价' from orderItem group by itemName
