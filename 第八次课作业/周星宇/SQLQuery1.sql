use master
go

create database student
on
(
     name='student',
	 filename='D:\bbs.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='student_log',
	 filename='D:\bbs_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go



use student 
go

create table studentinfo
(
    stuNO varchar(50) primary key not null,
    stuName varchar(10) unique not null,
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int not null,
	stuSex varchar(2) check(stuSex='0' or stuSex='1') not null
)

go

create table achievement
(
     examNO int primary key identity(1,1) not null,
	 stuNO varchar(50) references studentinfo(stuNO),
	 writtenExam int not null,
	 labExam int not null
)



insert into studentinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex)
select 's2501','张秋利','20','美国硅谷','1','1' union
select 's2502','李斯文','18','湖北武汉','2','0' union
select 's2503','马文才','22','湖南长沙','3','1' union
select 's2504','欧阳俊雄','21','湖北武汉','4','0' union
select 's2505','梅超风','20','湖北武汉','5','1' union
select 's2506','陈旋风','19','美国硅谷','6','1' union
select 's2507','梅超风2','20','美国硅谷','7','0'
go



select * from studentinfo

insert into achievement
select 's2501', 50, 70 union
select 's2502', 60, 65 union
select 's2503', 86, 85 union
select 's2504', 40, 80 union
select 's2505', 70, 90 union
select 's2506', 85, 90

go


--1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select stuNO '学号', stuName '姓名', stuAge '年龄', stuAddress '地址', stuSeat '座号', stuSex '性别' from studentinfo

--2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select stuName, stuAge, stuAddress from studentinfo

--3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字.注意：要用三种方法
select stuNO '学号', writtenExam '笔试', labExam '机试' from achievement

--4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select stuNO, stuName, stuAddress, stuName+'@'+stuAddress '邮箱' from studentinfo

--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
select stuNO '学号', writtenExam '笔试', labExam '机试', labExam+writtenExam '总分'  from achievement

--6.查询学生信息表（stuInfo）中学生来自哪几个地方
select distinct stuAddress from studentinfo

--7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名
select distinct stuAge '所有年龄' from studentinfo

--8.查询学生信息表（stuInfo）中前3行记录 
select top 3 * from studentinfo

--9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select top 4 stuName, stuSeat from studentinfo

--10.查询学生信息表（stuInfo）中一半学生的信息
select top 50 percent * from studentinfo

--11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from studentinfo where stuAddress = '湖北武汉' and stuAge = 20

--12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列（用两种方法实现）
select * from achievement where labExam >=60 and labExam <=80  order by labExam desc
select * from achievement where labExam BETWEEN 60 AND 80 order by labExam desc

--13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现）
select * from studentinfo where stuAddress = '湖北武汉' or stuAddress = '湖南长沙'
select * from studentinfo where stuAddress in ('湖北武汉','湖南长沙')

--14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列（用两种方法实现）
select * from achievement where writtenExam >90 or writtenExam <70 order by writtenExam asc
select * from achievement where not (writtenExam >=70 AND writtenExam <=90) order by writtenExam asc
select * from achievement where writtenExam not between 70 AND 90


--15.查询年龄没有写的学生所有信息
select * from studentinfo where stuAge is null or stuAge = ''

--16.查询年龄写了的学生所有信息
select * from studentinfo where StuAge is not null

--17.查询姓张的学生信息
select * from studentinfo where stuName like '张%'

--18.查询学生地址中有‘湖’字的信息
select * from studentinfo where stuName like '%湖%'

--19.查询姓张但名为一个字的学生信息
select * from studentinfo where stuName like '张_'

--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from studentinfo where stuName like '__俊%'

--21.按学生的年龄降序显示所有学生信息
select * from studentinfo stuAge order by stuAge desc

--22.按学生的年龄降序和座位号升序来显示所有学生的信息
select * from studentinfo stuAge order by stuAge desc ,stuSeat

--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from achievement writtenExam order by writtenExam desc

--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from achievement writtenExam order by writtenExam asc

                                                 
--25.查询每个地方的学生的平均年龄
select AVG(stuAge)平均年龄 from studentinfo

--26.查询男女生的分别的年龄总和
select SUM(stuAge)男年龄总和 from studentinfo where stuSex=1
select SUM(stuAge)女年龄总和 from studentinfo where stuSex=0

--27.查询每个地方的男女生的平均年龄和年龄的总和
select SUM(stuAge)美国硅谷年龄总和 from studentinfo where stuAddress='美国硅谷'
select SUM(stuAge)湖北武汉年龄总和 from studentinfo where stuAddress='湖北武汉'
select SUM(stuAge)湖南长沙年龄总和 from studentinfo where stuAddress='湖南长沙'
