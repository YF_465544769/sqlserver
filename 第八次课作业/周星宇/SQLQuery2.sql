use master
go
create database webshop
on
(
     name='webshop',
	 filename='D:\webshop.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='webshop_log',
	 filename='D:\webshop_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go


use webshop 
go

create table orders
(
     orderID int primary key identity(1,1) ,
	 orderDate datetime
)


go

create table orderItem
(
      ItemiD int primary key identity(1,1) ,
	  orderId int,
	  itemType varchar(20),
	  itemName nvarchar(10),
	  theNumber int,
	  theMoney money
)

set identity_insert orderItem ON--打开
insert into orderItem(ItemiD,orderId,itemType,itemName,theNumber,theMoney)
select '1','1','文具','笔','72','2' union
select '2','1','文具','尺','11','1' union
select '3','1','体育用品','篮球','1','56' union
select '4','2','文具','笔','36','2' union
select '5','2','文具','固体胶','20','3' union
select '6','2','日常用品','透明胶','20','1' union
select '7','2','体育用品','羽毛球','20','3' union
select '8','3','文具','订书机','20','3' union
select '9','3','文具','订书钉','10','3' union
select '10','3','文具','裁纸刀','5','5' union
select '11','4','文具','笔','20','2' union
select '12','4','文具','信纸','50','1' union
select '13','4','日常用品','毛巾','4','5' union
select '14','4','日常用品','透明胶','30','1' union
select '15','4','体育用品','羽毛球','20','3'
select * from orderItem

set identity_insert orders ON--打开
insert into orders(orderId,orderDate) values (1,'2008-1-12'),(2,'2008-2-10'),(3,'2008-2-15'),(4,'2008-3-10')
select * from orders


--1.查询所有订单订购的所有物品数量总和
select SUM(theNumber) 数量总和 from orderItem

--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select orderId,SUM(theNumber), AVG(theMoney) from orderItem group by orderId having  AVG(theMoney)<10 and orderId<3

--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId,SUM(theNumber),AVG(theMoney) from orderItem where theMoney>50 group by orderId having AVG(theNumber)<10

--4.查询每种类别的产品分别订购了几次
select itemType 类型,COUNT(itemType) 数量 from orderItem group by itemType

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType 类型,sum(theNumber)总数,avg(theMoney)平均单价 from orderItem group by itemType having sum(theNumber)>100

--6.查询每种产品的订购次数，订购总数量和订购的平均单价
select itemName 产品名称 ,COUNT(itemName)订购次数,sum(theNumber)总数量,avg(theMoney)平均单价 from orderItem group by itemName