create database dingdan
on
(
	name='dingdan',
	filename='D:\dingdan.mdf',
	size=20,
	maxsize=300,
	filegrowth=50
)
log on
(
	name='dingdan_log',
	filename='D:\dingdan_log.ldf',
	size=20,
	maxsize=300,
	filegrowth=50
)
go
use dingdan 
go
create table orderItem
(
ItemiD int ,
orderId int,
itemType varchar(10),
itemName nvarchar(10),
theNumber int,
theMoney money
)
create table orders
(
orderId int not null primary key ,
orderDate datetime 
)
insert into orders(orderId,orderDate) values (1,'2008-1-12'),(2,'2008-2-10'),(3,'2008-2-15'),(4,'2008-3-10')
select * from orders
insert into orderItem(ItemiD,orderId,itemType,itemName,theNumber,theMoney) values 
(1,1,'文具','笔',72,2),
(2,1,'文具','尺',10,1),
(3,1,'体育用品','篮球',1,56),
(4,2,'文具','笔',36,2),
(5,2,'文具','固体胶',20,3),
(6,2,'日常用品','透明胶',2,1),
(7,2,'体育用品','羽毛球',20,3),
(8,3,'文具','订书机',20,3),
(9,3,'文具','订书钉',10,3),
(10,3,'文具','裁纸刀',5,5),
(11,4,'文具','笔',20,2),
(12,4,'文具','信纸',50,1),
(13,4,'日常用品','毛巾',4,5),
(14,4,'日常用品','透明胶',30,1),
(15,4,'体育用品','羽毛球',20,3)

select * from orderItem
--1.查询所有订单订购的所有物品数量总和
select SUM(theNumber) 数量总和 from orderItem
--2.查询 每个订单订购的所有物品的 数量和 以及  平均单价
select orderId,SUM(theNumber), AVG(theMoney) from orderItem group by orderId having  AVG(theMoney)<10 and orderId<3

--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId ,sum(theNumber),AVG(theMoney) from orderItem where theNumber>50 group by orderId having AVG(theMoney)<10
--4.查询每种类别的产品分别订购了几次，例如：
--					文具      9
--                    体育用品  3
--                    日常用品  3
select itemType 类型,COUNT(itemType) 数量 from orderItem group by itemType

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价

select itemType 类型,sum(theNumber)总数,avg(theMoney) 平均单价 from orderItem group by itemType having sum(theNumber)>100

--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2
select itemName 产品名称 ,COUNT(itemName) 订购次数,sum(theNumber) 总数量 ,avg(theMoney)平均单价  from orderItem group by itemName
