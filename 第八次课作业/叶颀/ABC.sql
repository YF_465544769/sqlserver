use master
go

create database ABC
on
(
	name='ABC',
	filename='D:\test\ABC.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='ABC_log',
	filename='D:\test\ABC_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go
use ABC
go

create table orders
(
	orderId int primary key identity ,
	orderDate datetime
)
go

create table orderItem
(
	ItemiD int primary key identity ,
	orderId int references orders(orderId),
	itemType varchar(10),
	itemName varchar(10),
	theNumber int ,
	theMoney int
)
go

insert orders values 
('2008-01-12'),('2008-02-10'),
('2008-02-15'),('2008-03-10')
go

insert orderItem values
('1','文具','笔',72,2),
('1','文具','尺',10,1),
('1','体育用品','篮球',1,56),
('2','文具','笔',36,2),
('2','文具','固体胶',20,3),
('2','日常用品','透明胶',2,1),
('2','体育用品','羽毛球',20,3),
('3','文具','订书机',20,3),
('3','文具','订书针',10,3),
('3','文具','裁纸刀',5,5),
('4','文具','笔',20,2),
('4','文具','信纸',50,1),
('4','日常用品','毛巾',4,5),
('4','日常用品','透明胶',30,1),
('4','体育用品','羽毛球',20,3)
go

select SUM(theNumber)as 总数 from dbo.orderItem
go

select SUM(theNumber) as 数量 ,AVG(theMoney) as 平均单价 from dbo.orderItem where orderId<3 group by orderId having AVG(theMoney)<10 
go

select SUM(theNumber) as 数量,AVG(theMoney) as 平均单价 from dbo.orderItem where theNumber<50 group by orderId having AVG(theMoney)<10
go

select '文具：' as 类型,COUNT(itemType) 数量 from orderItem where itemType='文具' union
select '体育用品：' as 类型,COUNT(itemType) 数量 from orderItem where itemType='体育用品' union
select '日常用品：' as 类型, COUNT(itemType) 数量 from orderItem where itemType='日常用品' 
go

select itemType,SUM(theNumber) as 数量,AVG(theMoney) as 平均单价 from dbo.orderItem where theNumber>100 group by itemType 
go

select itemName as 产品名称,COUNT(itemName) as 订购次数,SUM(theNumber) as 总数量,AVG(theMoney) as 平均单价 from dbo.orderItem group by itemName



select StuProvince,AVG(StuAge)平均年龄 from dbo.StuInfo group by StuProvince
select StuSex,sum(StuAge)年龄总和 from dbo.StuInfo group by StuSex
select StuProvince,AVG(StuAge)平均年龄,sum(StuAge)年龄总和  from dbo.StuInfo group by StuProvince