create database Ordermenber
on
(
	name=Ordermenber,
	filename='D:\Ordermenber.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=Ordermenber_log,
	filename='D:\Ordermenber_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
go
--订单表（orders）列为：订单编号（orderId 主键）  订购日期（orderDate）
--订购项目表（orderItem），列为：
--项目编号（ItemiD）订单编号（orderId）产品类别（itemType）
--产品名称（itemName） 订购数量（theNumber）  订购单价（theMoney）

create table orders
(
orderID int primary key identity(1,1),
orderData  date  default(getdate()) not null,
)
insert into orders(orderData)values('2008-01-12 00:00:00:000')
insert into orders(orderData)values('2008-02-10 00:00:00:000')
insert into orders(orderData)values('2008-02-15 00:00:00:000')
insert into orders(orderData)values('2008-03-10 00:00:00:000')

create table orderItem
(
itemID  int primary key identity(1,1),
orderid  int references orders(orderID) not null,
itemType varchar(20) not null,
itemName varchar(20) not null,
theNumber int not null,
theMoney int not null
)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(1,'文具','笔',72,2)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(1,'文具','尺',10,1)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(2,'体育用品','篮球',1,56)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(2,'文具','笔',36,2)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(2,'文具','固体胶',20,3)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(2,'日常用品','透明胶',2,1)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(3,'体育用品','羽毛球',20,3)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(3,'文具','订书机',20,3)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(3,'文具','订书机',10,3)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(4,'文具','裁纸刀',5,5)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(4,'文具','笔',20,2)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(4,'文具','信纸',50,1)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(4,'日常用品','毛巾',4,5)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(4,'日常用品','透明胶',30,1)
insert into orderItem(orderid,itemType,itemName,theNumber,theMoney)values(4,'体育用品','羽毛球',20,3)
select orderID, SUM(theNumber)物品数量总和 from dbo.orderItem group by orderID
select orderID, SUM(theNumber)物品数量总和,AVG(theMoney)平均单价 from dbo.orderItem where orderID<3  group by orderID HAVING AVG(theMoney)<10
select orderID, SUM(theNumber)物品数量总和,AVG(theMoney)平均单价 from dbo.orderItem where theNumber>50  group by orderID HAVING AVG(theMoney)<10
select '文具',COUNT(itemType)订购次数 from orderItem where itemType='文具' 
UNION
select '体育用品',COUNT(itemType)订购次数 from orderItem where itemType='体育用品'
UNION
select'日常用品',COUNT(itemType)订购次数 from orderItem where itemType='日常用品'
select '文具', SUM(theNumber)订购总数量,AVG(theMoney)平均单价 from dbo.orderItem where theNumber>100  group by itemType
UNION
select '体育用品',SUM(theNumber)订购总数量,AVG(theMoney)平均单价 from dbo.orderItem where theNumber>100  group by itemType
UNION
select'日常用品',SUM(theNumber)订购总数量,AVG(theMoney)平均单价 from dbo.orderItem where theNumber>100  group by itemType
select '文具',COUNT(itemType)订购次数,SUM(theNumber)订购总数量,AVG(theMoney)订购的平均单价 from orderItem where itemType='文具' 
UNION
select '体育用品',COUNT(itemType)订购次数 ,SUM(theNumber)订购总数量,AVG(theMoney)订购的平均单价 from orderItem where itemType='体育用品'
UNION
select'日常用品',COUNT(itemType)订购次数 ,SUM(theNumber)订购总数量,AVG(theMoney)订购的平均单价 from orderItem where itemType='日常用品'