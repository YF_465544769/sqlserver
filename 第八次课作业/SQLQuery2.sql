create database Students03
	on
	(
	name = 'Students03',
	filename = 'D:\test.mdf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 10%
	)
	log on
	(
	name = 'Students03_log',
	filename = 'D:\test_log.ldf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 10%
	)
	go 
	use  Students03
	go

	if exists(select * from sys.objects where name = 'StudentInfo')
	drop table StudentInfo
	create table StudentInfo
	(
	StuNo varchar(10) primary key ,
	StuName varchar(10) check(len(StuName)>=2),
	StuAge int not null,
	StuAddress nvarchar(20),
	StuSeat int ,
	StuSex nvarchar(1) default ('1') check(StuSex='0' or StuSex='1')
	)
	select * from StudentInfo
	insert into StudentInfo values ('s2501','张秋利',20,'美国硅谷',1,1),('s2502','李斯文',18,'湖北武汉',2,0),
	('s2503','马文才',22,'湖南长沙',3,1),('s2504','欧阳俊雄',21,'湖北武汉',4,0),
	('s2505','梅超风',20,'湖北武汉',5,1),('s2506','陈旋风',19,'美国硅谷',6,1),('s2507','陈风',20,'美国硅谷',7,0)

	select StuNo 学号 ,StuName 姓名, StuAge 年龄, StuAddress 地址, StuSeat 座位号, StuSex 性别 from StudentInfo
	select StuName 姓名, StuAge 年龄, StuAddress 地址 from StudentInfo
	select StuNo 学号 ,StuName 姓名, StuAddress 地址, StuName + '@'+ StuAddress 邮箱  from StudentInfo
	select StuAddress 地址  from StudentInfo
	select StuAge 所有年龄 from StudentInfo
	select top 3 * from StudentInfo
	select top 4 StuName 姓名, StuSeat 座位号 from StudentInfo
	select top 50 percent  * from StudentInfo
	select *  from StudentInfo where StuAddress = '湖北武汉' and StuAge = 20
	select *  from StudentInfo where  StuAddress in ('湖北武汉' ,'湖南长沙')
	select *  from StudentInfo where  StuAddress = '湖南长沙' or  StuAddress = '湖北武汉'
	select *  from StudentInfo where StuAge is null 
	select *  from StudentInfo where StuAge is not null 
	select *  from StudentInfo where  StuName like '张%'
	select *  from StudentInfo where StuAddress like '湖%'
	select *  from StudentInfo where  StuName like '张_'
	select *  from StudentInfo where  StuName like '__俊%'
	select *  from StudentInfo order by StuAge DESC
	select *  from StudentInfo order by StuAge DESC,StuSeat ASC

	
	create table StuExamInfo
	(
	ExamNo int primary key identity(1,1),
	StuNo varchar(10) references StudentInfo(StuNo),
	WrittenExam int,
	LadExam int
	)

	select * from StuExamInfo
	insert into StuExamInfo (WrittenExam,LadExam) values (50,70),(60,65),(86,85),
	(40,80),(70,90),(85,90)
	
	select StuNo as 学号,WrittenExam as 笔试 , LadExam as 机试 from StuExamInfo
	select StuNo 学号,WrittenExam 笔试 , LadExam 机试 from StuExamInfo
	select  学号=StuNo , 笔试 =WrittenExam ,机试 = LadExam from StuExamInfo

	select StuNo 学号,WrittenExam 笔试 , LadExam 机试 , WrittenExam +LadExam 总分 from StuExamInfo
	--排序，order by 字段名 ASC/DESC，默认是升序的
	select * from StuExamInfo order by LadExam DESC
	select top 3 * from StuExamInfo order by WrittenExam  
	select top 1 * from StuExamInfo order by WrittenExam DESC
	select top 1 * from StuExamInfo order by LadExam 

	--25.查询每个地方的学生的平均年龄
	select StuAddress ,	avg(StuAge) 平均年龄 from StudentInfo group by StuAddress

	--26.查询男女生的分别的年龄总和
	select COUNT(*)男生人数 ,sum(StuAge)年龄总和 from StudentInfo where StuSex='1'
	select COUNT(*)女生人数 ,sum(StuAge)年龄总和 from StudentInfo where StuSex='0'

	--27.查询每个地方的男女生的平均年龄和年龄的总和
	select StuAddress 地址 ,StuSex 性别, avg(StuAge)平均年龄 ,sum(StuAge)年龄总和 from  StudentInfo group by StuAddress ,StuSex