use master 
go 

create database orders

use orders

--订单表（orders）列为：订单编号（orderId 主键）  订购日期（orderDate）

--订购项目表（orderItem），列为：
--项目编号（ItemiD）订单编号（orderId）产品类别（itemType）
--产品名称（itemName） 订购数量（theNumber）  订购单价（theMoney）


create table orders 
(
	orderId int primary key identity(1,1),
	orderDate datetime not null
)

create table orderItem
(
	Itemid int primary key identity(1,1),
	orderId int foreign key references orders(orderId),
	itemType nvarchar(5) not null,
	itemName nvarchar(5) not null,
	theNumber int not null,
	theMoney money not null
)

insert into orders(orderDate)
select '2008-01-12 00:00:00' union
select '2008-02-10 00:00:00' union
select '2008-02-15 00:00:00' union
select '2008-03-10 00:00:00'

insert into orderItem(orderId,itemType,itemName,theNumber,theMoney)
select '1','文具','笔','72','2' union
select '1','文具','尺','10','1' union
select '1','体育用品','篮球','1','56' union
select '2','文具','笔','36','2' union
select '2','文具','固体胶','20','3' union
select '2','日常用品','透明胶','2','1' union
select '2','体育用品','羽毛球','20','3' union
select '3','文具','订书机','20','3' union
select '3','文具','订书钉','10','3' union
select '3','文具','裁纸刀','5','5' union
select '4','文具','笔','20','2' union
select '4','文具','信纸','50','1' union
select '4','日常用品','毛巾','4','5' union
select '4','日常用品','透明胶','30','1' union
select '4','体育用品','羽毛球','20','3' 


--1.查询所有订单订购的所有物品数量总和

select SUM(theNumber) from orderItem

--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价

select * from orderItem

select orderId,AVG(theMoney) 平均单价,SUM(theNumber) 数量 from orderItem where orderId<3 group by orderId HAVING AVG(theMoney)<10

--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价

select SUM(theNumber) 数量,AVG(theMoney) 平均单价 from orderItem where theNumber<50 group by theNumber HAVING AVG(theMoney)<10

--4.查询每种类别的产品分别订购了几次，例如：
--					文具      9
--                    体育用品  3
--                    日常用品  3

select orderId ,sum(theNumber),AVG(theMoney) from orderItem where theNumber>50 group by orderId having AVG(theMoney)<10

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价

select itemType 类型,sum(theNumber)总数,avg(theMoney) 平均单价 from orderItem group by itemType having sum(theNumber)>100

--6.c，例如：

--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2

select itemName 产品名称, COUNT(itemName) 订购次数,SUM(theNumber) 总数量,AVG(theMoney) 平均单价 from orderItem  group by itemName


