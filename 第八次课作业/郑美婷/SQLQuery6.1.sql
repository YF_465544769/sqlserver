use master
go
if exists(select*from sys.databases where name='TestDB' )
drop database TestDB
create database TestDB
on
(
	name='TestDB',
	filename='D:\SQLcunchu\TestDB.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(   name='TestDB_log',
	filename='D:\SQLcunchu\TestDB_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
go
use TestDB
go
create table orders
(
orderId int primary key identity(1,1),
orderDate datetime 
)
create table orderItem
(
ItemiD int identity(1,1),
orderId int ,
itemType nvarchar(10),
itemName nvarchar(20),
theNumber int,
theMoney int
)
insert into orderItem values
(1,'文具','笔',72,2),(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),(2,'日常用品','笔透明胶',2,1),
(2,'体育用品','羽毛球',20,3),(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)
select sum(theNumber)所有物品数量总和 from orderItem--1.查询 所有订单 订购的 所有物品数量总和
select orderId 订单编号,sum(theNumber)所有物品的数量和,AVg(theMoney)平均单价 from orderItem where orderId<3 group by orderId having AVg(theMoney)<10--2.查询 订单编号小于3的，平均单价小于10 每个订单订购的 所有物品的数量和 以及 平均单价
select orderId 订单编号,sum(theNumber)所有物品数量和,Avg(theMoney)平均单价 from orderItem group by orderId having avg(theMoney)<10 and sum(theNumber)>50--3.查询 平均单价小于10 并且 总数量大于50 每个订单订购的 所有物品数量和 以及 平均单价
select itemType 产品种类,count(itemType)订购次数 from orderItem group by itemType--4.查询 每种类别的产品 分别 订购了几次 ，例如：
select count(*)订购次数 from orderItem where itemType='文具'--文具      9
select count(*)订购次数 from orderItem where itemType='体育用品'--体育用品  3
select count(*)订购次数 from orderItem where itemType='日常用品'--日常用品  3
select itemType 产品类别,sum(theNumber)订购总数量,Avg(theMoney)平均单价 from orderItem group by orderId having sum(theNumber)>100--5.查询 每种类别的产品 的 订购总数量在100以上的订购总数量 和 平均单价
select itemName 订购产品, count(itemName)订购次数,sum(theNumber)订购总数量,sum(theMoney)平均单价 from orderItem group by itemName--6.查询 每种产品的订购次数，订购总数量 和 订购的平均单价，例如：
--产品名称   订购次数  总数量   平均单价 
    --笔           3       120       2


