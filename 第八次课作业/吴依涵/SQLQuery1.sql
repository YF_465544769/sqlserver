use master
go
create database biao
on
(
name='biao',
filename='D:\新建文件夹.mdf',
size=6mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='biao_log',
filename='D:\新建文件夹_log.ldf',
size=6mb,
maxsize=100mb,
filegrowth=10mb
)
go
use biao
go
create table orders
(
orderId int primary key identity,
orderDate datetime default(getDate())
)


use biao
go
create table orderItem
(
ItemiD int identity,
orderId int references orders(orderId),
itemType nvarchar(6), 
itemName nvarchar(5),
theNumber int,
theMoney money
)

insert into orders values(GETDATE()),(GETDATE()),(GETDATE()),(GETDATE())
insert into orderItem values(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)

--1.查询所有订单订购的所有物品数量总和
select sum(theNumber) from orderItem  

--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select orderId,sum(theNumber),avg(theMoney) from orderItem where orderId<3 group by orderId having avg(theMoney)<10

--3.查询平均单价小于10且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId,sum(theNumber),avg(theMoney) from orderItem group by orderId having avg(theMoney)<10 and sum(theNumber)>50

--4.查询每种类别的产品  分别订购了几次  ，例如：文具 9 ， 体育用品  3 ， 日常用品  3
select itemType 类型, count(orderId)次数  from orderItem group by itemType

--5.查询每种类别产品的  订购总数量  在  100以上  的  订购总数量  和  平均单价
select itemType 类型,sum(theNumber) 总数量,avg(theMoney) from orderItem group by itemType

--6.查询每种产品的  订购  次数  ，订购  总数量  和订购的  平均单价  ，例如：

--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2
select itemName 产品名称,count(orderId) 订购次数,sum(theNumber) 总数量,avg(theMoney) 平均单价 from orderItem group by itemName

select * from orderItem