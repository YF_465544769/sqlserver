use master
go
create database Stundent
on
(
 name='Stundent',
 filename='C:\新建文件夹\sqlserver\第五次课作业\吴依涵.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Stundent_log',
 filename='C:\新建文件夹\sqlserver\第五次课作业\吴依涵_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Stundent
go
create table stuinfo--学生信息表
(
 stuNO varchar(10), 
 stuName nvarchar(20), 
 stuAge int, 
 stuAddress nvarchar(100),
 stuSeat char(10),
 StuSex nvarchar(1) check(StuSex in(1,0)),
 )
use Stundent
go
create table stuexam--学生成绩表
(
 examNO char(10),
 stuNO varchar(10),
 writtenExam int,
 labExam int,
)
--插入表数据
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2501','张秋丽',20,'美国硅谷',1,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2502','李斯文',18,'湖北武汉',2,0)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2503','马文才',22,'湖南长沙',3,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2504','欧阳俊雄',21,'湖北武汉',4,0)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2505','梅超风',20,'湖北武汉',5,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2506','陈旋风',19,'美国硅谷',6,1)
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,StuSex)
values('s2507','陈风',20,'美国硅谷',7,0)


insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(1,'s2501',50,70)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(2,'s2502',60,65)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(3,'s2503',86,85)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(4,'s2504',40,80)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(5,'s2505',70,90)
insert into stuexam(examNO,stuNO,writtenExam,labExam)
values(6,'s2506',85,90)

--查询
select stuNO 学号,StuName 姓名,StuAge 年龄,StuAddress 家庭住址,StuSeat 座号,StuSex 性别 from stuinfo--1

select stuName 姓名,stuAge 年龄,stuAddress 家庭住址 from stuinfo--2

select stuNO as 学号,writtenExam as 笔试,labExam as 机试 from stuexam--3
select 学号=stuNO,笔试=writtenExam,机试=labExam from stuexam--3
select stuNO 学号,writtenExam 笔试,labExam 机试 from stuexam--3

select 学号=StuNo,姓名=StuName,家庭住址=StuAddress,StuName+'@'+StuAddress as 邮箱 from stuinfo--4

select stuNO 学号,writtenExam 笔试,labExam 机试,writtenExam+labExam as 总分 from stuexam--5

select distinct stuAddress from stuinfo--6

select distinct 所有年龄=StuAge from stuinfo--7

select top 3 * from stuinfo--8

select top 4 StuName 姓名, StuSeat 座号 from stuinfo--9 
 
select top 50 percent * from stuinfo--10

select * from stuinfo where StuAddress like '湖北武汉' and StuAge=20--11

select * from stuexam where labExam>=60 and labExam<=80 order by labExam DESC--12

select * from stuinfo where StuAddress like '湖北武汉' or StuAddress like '湖南长沙'--13
select * from stuinfo where StuAddress in ('湖北武汉','湖南长沙')--13

select * from stuexam where not (writtenExam>=70 and writtenExam<=90) order by writtenExam ASC--14

select * from stuinfo where StuAge is null --15

select * from stuinfo where StuAge is not null--16

select * from stuinfo where StuName like '张%'--17

select * from stuinfo where StuAddress like '%湖%'--18

select * from stuinfo where StuName like '张_'--19

select * from stuinfo where StuName like '__俊%'--20

select * from stuinfo order by StuAge DESC--21

select * from stuinfo order by StuSeat ASC,StuAge DESC--22

select top 1 * from stuexam order by writtenExam DESC --23

select top 1 * from stuexam order by labExam ASC--24


--25.查询每个地方的学生的平均年龄
select stuAddress 地区, avg(stuAge)平均年龄 from stuinfo group by stuAddress

--26.查询男女生的分别的年龄总和
select StuSex 性别, sum(stuAge)年龄总和  from  stuinfo group by StuSex

--27.查询每个地方的男女生的平均年龄和年龄的总和
select stuAddress 地区, avg(stuAge)平均年龄, sum(stuAge)年龄总和 from stuinfo group by stuAddress