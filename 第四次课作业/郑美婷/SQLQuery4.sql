use master
go
if exists(select * from sys.databases where name='Student')
drop database Student
create database Student
on
(
name='Student _mdf',
filename='D:\SQLcunchu\Student _mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='Student _ldf',
filename='D:\SQLcunchu\Student _ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use Student
go
--��
create table Class
(
ClassId int primary key identity(1,1),
ClassName nvarchar(20) unique not null,
)
--����
insert into Class(ClassName) values ('����һ��'),('���ж���'),('��������'),('�����İ�'),('԰�����'),('Ӱ������'),('�����߰�'),('δ���˰�'),('����Ű�'),('����ʮ��')
update Class set ClassName='����һ��' where ClassId=1
delete from Class where ClassId=10
--��
create table Student
(
StuId int primary key identity(1,1),
ClassId int references Class(ClassId),
StuName nvarchar(20) not null,
StuSex nvarchar(1) check(StuSex='��' or StuSex='Ů') default('��') ,
StuBirthday date ,
StuPhone nvarchar(11) unique,
StuAddress nvarchar(200) ,
)
--����
insert into Student values
(1,'��','��',2001/2/5,12345678901,'������԰ĳĳ¥'),
(2,'wuwu','��',2002/5/8,12345678902,'������԰ĳĳ¥'),
(9,'����','Ů',2004/2/3,12345678903,'������԰ĳĳ¥'),
(5,'�ù�','Ů',2002/5/7,12345678904,'������԰ĳĳ¥'),
(3,'����','Ů',2005/5/8,12345678905,'������԰ĳĳ¥'),
(6,'����','��',2001/8/9,12345678906,'������԰ĳĳ¥'),
(4,'ѧϰ','Ů',2006/1/5,12345678907,'������԰ĳĳ¥'),
(5,'����','��',2001/12/4,12345678908,'������԰ĳĳ¥'),
(6,'����','��',2001/12/5,12345678909,'������԰ĳĳ¥'),
(2,'ͷͺ','��',2001/11/4,12345678920,'������԰ĳĳ¥'),
(8,'����','��',2009/11/11,12345678900,'������԰ĳĳ¥'),
(1,'����','��',2005/12/12,12345678912,'������԰ĳĳ¥'),
(7,'����','��',2011/7/8,12345678911,'������԰ĳĳ¥'),
(4,'�;�','Ů',2001/9/9,12345678966,'������԰ĳĳ¥'),
(6,'����','��',2003/11/10,12345678988,'������԰ĳĳ¥'),
(5,'����','��',2002/8/10,12345678999,'������԰ĳĳ¥'),
(6,'����','��',2001/6/7,12345678923,'������԰ĳĳ¥'),
(7,'�Ǵ�','Ů',2001/6/7,12345678918,'������԰ĳĳ¥'),
(2,'����','��',2001/4/5,12345678927,'������԰ĳĳ¥'),
(10,'�Ϳ�','��',2005/3/4,12345678979,'������԰ĳĳ¥')

alter table Student add CreateDate datetime default(getdate())
update Student set CreateTime=(getdate()) where StuId=*
delete from Student where ClassId=1


--��
create table Course
(CourseId  int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int check(CourseCredit<=5 and CourseCredit>=1) not null,
CourseCredits nvarchar(10) check(CourseCredit='רҵ��'or CourseCredit='������')
)
--����
insert into Course(CourseName) values ('Ӣ��'),('����'),('˼��'),('����'),('��������'),('����')
select CourseName from Course
update Course set CourseCredit=100 where(CourseName='����')
--��
create table Score
(
ScoreId int primary key identity(1,1),
StuId int references Student(StuId),
CourseId int references Course(CourseId),
Score decimal(5,2) unique not null
)
--����
insert into Score values
(00,1,100),(02,3,93),(03,4,90),(04,1,100),
(05,2,88),(06,1,100),(07,1,100),(08,5,94),
(09,4,87),(10,2,100),(11,6,92),(12,1,97),
(13,1,96),(14,1,100),(15,3,100),(16,4,79),
(17,6,100),(18,5,79),(19,2,100),(20,1,89)
update Score set Score=99 where(CourseId=2)
delete from Score where StuId=1
delete from Score where CourseID=1
alter table Score add constraint CK_Score_Score check(Score<=100 and Score>=0) 
alter table Score add constraint DF_Score_Score default(0)
