create database class001
go 

use class001
go

create table Class001
(
	Classid001 int primary key identity(1,1),
	Classnuma001 nvarchar(100) unique not null
)
truncate table Class001
insert into Class001(Classnuma001) values('�ù�1��'),('�ù�2��'),('�ù�3��'),('�ù�4��'),('�ù�5��'),('�ƹ�1��'),('�ƹ�2��'),('�ƹ�3��'),('�ƹ�4��'),('�ƹ�5��')
update Class001 set Classnuma001='����1��' where Classid001=1 
delete from Class001 where Classid001=10

go
use class001
go

create table StuInfo001
(
	stunun001 int primary key identity,
	Classid001 int foreign key references Class001(Classid001),
	stumane001 nvarchar(20) not null,
	stuSex001 nvarchar(1) check(stuSex001 in('Ů','��')) default'��',
	stubir001 datetime,
	stuphone001 decimal(11,0) unique,
	StuAddress001 text 
)

select  * from StuInfo001
insert into StuInfo001(Classid001,stumane001,stuphone001) values(1,'��',1),(2,'��',2),(3,'����',3),(4,'����',4),(5,'��',5),(6,'��',6),(7,'��',7),(8,'��',8),(9,'��',9),(10,'��',10)
insert into StuInfo001(Classid001,stumane001,stuphone001) values(1,'��',20),(2,'��',19),(3,'��',18),(4,'��',17),(5,'��',16),(6,'��',15),(7,'��',14),(8,'��',13),(9,'��',12),(10,'��',11)
alter table Stuinfo001 add CreateDate001 datetime default(getdate())
update StuInfo001 set CreateDate001=2021-03-09
delete from StuInfo001 where Classid001=10

go
use class001
go
create table Course001
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(100) unique not null,
	CourseCredit int default(1) check(CourseCredit>0 and CourseCredit<6) not null,
	CourseCredit001 nvarchar(10) check(CourseCredit001='������' or CourseCredit001='רҵ��')
)

insert into Course001(CourseName) values('����'),('ë��'),('��ѧӢ��'),('ʵ��'),('����'),('����')
select * from  Course001
update Course001 set CourseCredit=2 where CourseName='ʵ��'

go
	use class001
	go 

	create table Score001
	(
		ScoreId int primary key identity(1,1),
		StuId001 int foreign key references StuInfo001(stunun001),
		CourseId int foreign key references Course001(CourseId),
		Score001 decimal(5,2) unique not null
	)

	insert into Score001(Score001) values(123.02),(124.04),(425.00),(125.33),(127.11),(128.22),(129.15),(130.55),(131.45),(777.12),(888.89),(999.99),(111.23),(123.09),(99.23),(95.22),(64.55),(23.11),(20.78),(10.10),(78.00)
	select * from Score001
	delete from Score001 where Score001=1
	alter table Score001 add constraint CK_Score001_Score001 check(Score001>0 and Score001<1000)
	alter table Score001 add constraint DK_Score001_Score001 default(0)