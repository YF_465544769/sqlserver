use master
go
create database Student
on
(
	name='Student',
	filename='D:\Demo\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\Demo\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student
go

create table Classid
(
	Classid int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

insert into Classid (ClassName) values ('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班')
,('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')

update Classid set ClassName='软件一班' where Classid=1

delete from Classid where Classid=10

select *from Classid 



create table Student
(
	StuId int primary key identity(1,1),
	Classid int references Classid(Classid),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200),
)
insert into Student(Classid,StuName,StuSex,StuPhone) values ('1','张一','男',1),
('2','张二','男',2),
('3','张三','男',3),
('4','张四','男',4),
('5','张五','男',5),
('6','张六','男',6),
('7','张七','男',7),
('8','张八','男',8),
('9','张九','男',9),
('1','张十','男',10),
('1','张1','女',11),
('2','张2','女',12),
('3','张3','女',13),
('4','张4','女',14),
('5','张5','女',15),
('6','张6','女',16),
('7','张7','女',17),
('8','张8','女',18),
('9','张9','女',19),
('1','张10','女',20)

alter table Student add CreateDate datetime default(getdate())

update Student set CreateDate=getdate()

delete from Student where Classid='2'

select *from Student


create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default('1') check(CourseCredit>1 and CourseCredit<5) not null,
	CourseType nvarchar(10)
)	

insert into Course(CourseName,CourseType) values ('SQL数据库','专业课'),
('职业生涯','公共课'),
('C#','专业课'),
('C++','专业课'),
('VB','专业课'),
('高数','公共课')

update Course set CourseCredit=2 where CourseName='SQL数据库'


select *from Course



create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int references Student(StuId),
	CourseId int references Course(CourseId),
	Score decimal(5,2) unique not null

)

insert into Score(Score,CourseId,StuId) values (90,6,13),
(89,8,14),(87,7,3),(86,6,4),(85,11,5),(84,4,6),(83,7,7),
(82,9,8),(81,8,9),(80,7,10),(79,10,11),(78,8,12)

delete from Score where StuId='1'
delete from Score where CourseId='1'

alter table Score add constraint CK_Score_Score check(Score>0 and Score<100)
alter table Score add consrraint DK_Score_Score default('0') for Score

select *from Score





















