use master 
go 

create database Student

use Student 

create table Class
(
	ClassId	int constraint KP_Class_ClassId primary key identity(1,1),
	ClassName nvarchar(20) unique not null, 
)

insert into Class (ClassName)
select'软件一班' union
select'软件二班' union
select'软件三班' union
select'软件四班' union
select'软件五班' union
select'软件六班' union
select'软件七班' union
select'软件八班' union
select'软件九班' union
select'软件十班' 

select * from Class

update Class set ClassName='软件十一班' where ClassId=1

delete from Class where ClassId=10

use Student

create table Student
(
	StuId int primary key identity(1,1),
	ClassId int constraint FK_Student_ClassId foreign key references Class(ClassId),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex in ('男','女')),
	StuBirthday date,
	StuPhone nvarchar(11) unique not null,
	StuAddress nvarchar(200),
)

insert into Student(StuName,StuSex,StuPhone,StuAddress)
select'张三','男','135441','asdf' union
select'张四','男','133351','assdf' union
select'张五','女','13651','asddf' union
select'张六','女','15351','asdsdf' union
select'张七','男','13951','asdsdf' union
select'张八','女','1361','as5f' union
select'张九','男','165351','asadsdf' union
select'张十','男','13351','asdsdf' union
select'张十一','男','145351','a54sdf' union
select'张十二','女','135541','asd54f' union
select'张十三','男','1354651','a785sdf' union
select'张十四','女','1378551','as587df' union
select'张十五','男','1351','asdf' union
select'张十六','男','138751','a78sdf' union
select'张十七','女','135871','as78df' union
select'张十八','男','131351','a31sdf' union
select'张十九','女','135481','as64df' union
select'张三','男','134651','asdfaf' union
select'张十九','男','351','aasdfa' union
select'张二十','女','135','dka' 

select * from Student

alter table Student add ClassId int references Class(ClassId)

alter table Student add CreateDate datetime

delete from Student where ClassId=1


use Student

create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default('1') check(CourseCredit in ('1','2','3','4','5')),
	CourseCategory nvarchar(10) check(CourseCategory in ('专业课','公共课')),
)

insert into Course(CourseName,CourseCategory)
select'语文','公共课' union
select'数学','公共课' union
select'Java','专业课' union
select '毛概','公共课' union
select'英语','公共课' union
select'SQL server','专业课'

select * from Course

update  Course set CourseCredit='5' where CourseCategory='专业课'

create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int foreign key references Student(StuId),
	Courseld int foreign key references Course(CourseId),
	Score decimal(5,2) unique not null
)

insert into Score(Score)
select '1.0' union
select '1.1' union
select '1.2' union
select '1.3' union
select '1.4' union
select '1.5' union
select '1.6' union
select '1.7' union
select '1.8' union
select '1.9' union
select '2.0' union
select '2.1' union
select '2.2' union
select '2.3' union
select '2.4' union
select '2.5' union
select '2.6' union
select '2.7' union
select '2.8' union
select '2.9' 

delete from Score where StuId='1'

delete from Score where Courseld='1'

alter table Score add constraint UK_Score_Score default('0') check(Score>=0 and Score<=100)