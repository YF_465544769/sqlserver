create  database Student
on
(
	name='Student',
	filename='D:\Students.mdf',
	size=10,
	maxsize=200,
	filegrowth=10
)
log on
(
    name='Student_log',
	filename='D:\Students_log.ldf',
	size=10,
	maxsize=200,
	filegrowth=10
)
go
use Student
go 
create table Class
(
	ClassId int  primary key  identity(1,1),
	ClassName nvarchar(20) unique not null 
)
select * from Class
insert into Class(ClassName) values ('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班'),('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')
update Class set ClassName='软件一班' where ClassId=1
delete from Class where ClassId=10
go
go
create table StudentIfo
(
	StuId int primary key identity(1,1) ,
	ClassId int foreign key references Class(ClassId),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) check(StuSex='男'or StuSex='女') default('男'),
	StuBirthday  date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200) ,
	CreateDate datetime  default(getdate())

)
select * from StudentIfo
insert into StudentIfo(ClassId,StuName,StuSex) values ('3','张三','男'), ('1','张一','男'), ('2','张市','男'), ('6','张六','男'), ('7','张三1','女'), ('4','张六1','男'), ('5','张三1','女') ,('8','张六2','男'), ('9','张三5','女'), ('9','张六0','男'), '('10','张三10','女'),
create table Course
(
	CourseId int  primary key  identity(1,1),
	CourseName nvarchar(50) unique not null ,
	CourseCredit int  not null default(1) check(CourseCredit in(1,2,3,4,5)),
	CourseCredit1  nvarchar(10) check(CourseCredit1 in('专业课','公共课')) 
)
select * from Course
insert into Course(CourseName) values('数学'),('英语'),('专业'),('体育'),('思修'),('物理')
update Course set CourseCredit=3 where CourseName='英语'
create table Score
(
	ScoreId int primary key identity(1,1) ,
	StuId int foreign key references StudentIfo(StuId),
	CourseId int foreign key references Course(CourseId),
	Score decimal(5,2) not null unique,
)
insert into Score  values(2044010101,1,53.25),(2044010102,2,43.25),(2044010103,3,58.25),(2044010104,4,88.25)
delete from Score where StuId=1