use master
go 
create database Students
on
(
name='Students',
filename='D:\test\Students.mdf',
size=5mb,
maxsize=5mb,
filegrowth=10%
)
log on
(
name='Students_log',
filename='D:\test\Students_log.mdf',
size=5mb,
maxsize=5mb,
filegrowth=10%
)
go
use Students
go
create table ClassInfo
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique not null
)
insert into ClassInfo(ClassName) values('软1'),('软2'),('软3'),('软4'),('软5'),('软6'),('软7'),('软8'),('软9'),('软10')
update ClassInfo set ClassName='土木1班' where ClassID=1
delete from ClassInfo where ClassID=10
create table StuInfo
(
StuID int primary key identity(1,1),
ClassID int ,
StuName nvarchar(20) not null,
StuSex  nvarchar(1) default('男') check(StuSex='男'or StuSex='女'),
StuBrithday date ,
StuPhone nvarchar(11) unique not null,
StuAddress nvarchar(200) ,
 )
 insert into StuInfo(ClassID,StuName,StuSex,StuBrithday,StuPhone,StuAddress) values
(1,'来来1','女','2001-3-02','12345682902','福建'),
(2,'来来2','女','2001-1-02','12345618902','福建'),
(3,'来来3','女','2001-4-02','12345674902','福建'),
(4,'来来4','女','2001-5-02','12345676902','福建'),
(6,'来来5','女','2001-5-12','12344478902','福建'),
(5,'来来6','女','2001-11-22','12345633302','福建'),
(7,'来来7','女','2001-11-4','12345633902','福建'),
(8,'来来8','女','2001-11-12','14345678902','福建'),
(9,'来来9','女','2001-11-2','12545678902','福建'),
(10,'来来10','女','2001-12-02','12845678902','福建'),
(11,'来来11','女','2001-11-07','12375678902','福建'),
(12,'来来22','女','2001-11-09','12345678902','福建'),
(13,'来来12','女','2001-11-06','92345678902','福建'),
(14,'来来13','女','2001-10-12','123456709902','福建'),
(15,'来来23','女','2001-3-22','12345678882','福建'),
(16,'来来24','女','2001-7-02','12345679902','福建'),
(17,'来来34','女','2003-5-02','12345699902','福建'),
(18,'来来35','女','2002-6-02','12345677772','福建'),
(19,'来来36','女','2003-5-02','12388878902','福建'),
(20,'来来37','女','2002-4-02','19945678902','福建')
alter table  StuInfo add CreateDate  datetime default(getdate())
update StuInfo set CreateDate = getdate()
delete from StuInfo where ClassID= 1


select * from StuInfo



create table CourseInfo
(
CourseID int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int not null default(1) check(CourseCredit>=1 and CourseCredit<=5 ),
CourseType nvarchar(10)check(CourseType='专业课' or CourseType='公共课' )
)
insert into CourseInfo(CourseName) values('英语'),('思修'),('高数'),('职素'),('体育'),('专业')
update CourseInfo set CourseCredit=5 where CourseName='英语'

select * from CourseInfo



create table ScoreInfo
(
ScoreId int primary key identity(1,1),
StuId int ,
CourseId int ,
Score decimal(5,2) unique not null
)
insert into  ScoreInfo(StuId,Score) values(1,33),(2,33),(3,44),(4,66),(5,77),(8,88),(6,88),
(9,88),(12,22),(13,33),(14,77),(15,88),(16,66),(17,67),(18,65),(19,55),(20,),(7,90),(10,100),
(11,90)
insert into ScoreInfo(CourseId) values(1),(2),(5),(8)
delete from	ScoreInfo where StuId= 1
delete from	ScoreInfo where CourseId= 1
alter table ScoreInfo add constraint  int check (Score>=0 or Score<=100 ) 