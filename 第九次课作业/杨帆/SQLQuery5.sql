use master
go

create database  bbs
on
(
	name='bbs',
	filename='D:\test\bbs.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\test\bbs_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=10%
)

go
use bbs
go

create table bbsUsers
(
	UuId int identity(1,1),
	UName varchar(10) not null ,
	USex varchar(2) ,
	UAge int ,
	UPoint int not null
)
go

alter table bbsUsers add constraint PK_UuId primary key (UuId)
go
alter table bbsUsers add constraint UQ_UName unique (UName)
go
alter table bbsUsers add constraint CK_USex check(USex='男'or USex='女')
go
alter table bbsUsers add constraint CK_UAge check(UAge>=15 and UAge<=60)
go
alter table bbsUsers add constraint CK_UPoint check(Upoint>=0)
go

create table bbsSection
(
	Ssid int identity(1,1) not null,
	SName varchar(10) not null,
	SuId int,
)
go

alter table bbsSection add constraint PK_Ssid primary key (Ssid)
go
alter table bbsSection add constraint FK_SuId foreign key(SuId) references bbsUsers(UuId)
go

create table bbsTopic
(
	TtId int primary key identity(1,1),
	TuId int references bbsUsers(Uuid),
	TsId int references bbsSection(SsId),
	TTitlr varchar(100) not null,
	TMsg text,
	TTime datetime,
	TCount int 
)
go

create table bbsReply
(
	RrId int primary key identity(1,1),
	RuId int references bbsUsers(UuId),
	RtId int references bbsTopic(TtId),
	RMsg text not null,
	RTime datetime,
)
go

insert bbsUsers (UName,USex,UAge,UPoint)
select '小雨点','女',20,0 union
select '逍遥','男',18,4 union
select '七年级生','男',19,2 
go

select UName ,UPoint into bbsPoint from bbsUsers
go

insert into bbsSection(SName,SuId)values
('技术交流',2),
('读书世界',3),
('生活百科',2),
('八卦区',3)
go

alter table bbsTopic drop constraint FK__bbsTopic__TuId__300424B4
go

insert into bbsTopic(TuId,TsId,TTitlr,TMsg,TTime,TCount)values 
(3,4,'范跑跑','谁是范跑跑','2008-7-8','1'),
(2,1,'.NET ','与JAVA的区别是什么呀？','2008-9-1','2'),
(4,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10','0'),
(3,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10','0'),
(4,1,'.NET ','与JAVA的区别是什么呀？','2008-9-1','0'),
(2,4,'范跑跑','范跑跑谁是???','2008-7-8','0')
go

alter table bbsReply drop constraint FK__bbsReply__RtId__34C8D9D1
go

alter table bbsReply drop constraint FK__bbsReply__RuId__33D4B598
go

insert into bbsReply(RuId,RMsg,RTime,RtId)values 
('2','范跑跑 ','2008-7-8','1'),
('3','NET ','2008-9-1','2'),
('4','.NET ','2008-9-10','2')
go

update bbsUsers set UPoint=UPoint+10 where UuId=3

truncate table bbsReply
go

select TsId,COUNT(TtId) as 发帖总数 from bbsTopic group by TsId
go

select RtId, COUNT(RrId) as 回帖总数量 from bbsReply group by RtId
go

select TuId,COUNT(TtId) as 发帖总数 from bbsTopic group by TuId
go

select TuId,SUM(TCount) as 回复数量总和 from bbsTopic group by TuId
go

select TsId,AVG(TCount) as 平均回复数量 from bbsTopic group by TsId having AVG(TCount)>3
go

select * from bbsUsers where UPoint=(select MAX(UPoint) from bbsUsers)
go

select * from bbsTopic where TTitlr like('%快乐%') or  TMsg like('%快乐%')
go

select * from bbsUsers where UAge>=15 and UAge<=20 and UPoint>=10
go

select * from bbsUsers where UName like('小%') and UName like('__大%')
go

select TTitlr as 标题, TMsg as 内容 from bbsTopic where TTime>='2008-9-10 12:00:00' and TCount>=10
 go

 select TuId ,TCount from bbsTopic where TTitlr like ('%!')
 go
