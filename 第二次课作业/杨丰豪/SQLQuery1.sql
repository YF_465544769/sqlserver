if exists(select * from sys.databases where name='Homework2')
drop database Homework2
create database Homework2
on
(
name='Homework2',
filename='D:\test\Homework2.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10MB
)
log on
(
name='Homework_log.ldf',
filename='D:\test\Homework2_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10MB
)
go
use Homework2
go
create table teachers
( 
StuID int primary key identity(1,1) not null,
StuNum char(10) not null,
StuName nvarchar(20) not null,
StuSex char(2) not null,
StuPhone varchar(20)

)