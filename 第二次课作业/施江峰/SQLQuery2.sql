use master
go
create database students
use students
go
create table stuinfo--学生信息表
(stuID int primary key identity(1,1),
stunum char(10) not null,
stuname nchar(20) not null,
stusex char(2) not null,
stuphone char(20) not null,
)
create table classinfo--班级表
(classID int primary key identity(1,1),
classnum char(10) not null,
classname nchar(20) not null,
classRemark  text not null,
stuID INT NOT NULL,
)