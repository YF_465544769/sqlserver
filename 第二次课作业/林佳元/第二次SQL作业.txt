﻿create database Students

on
(
  name='Students',
  filename='D:\Students.mdf',
  size=5mb
)
log on
(

  name='Students_log',
  filename='D:\Students_log.ldf',
  size=5mb
)
go 

use Students
go

create table StuInfo
(
  StuID int primary key identity(1,1),
  StuNum char(10) not null,
  StuName nvarchar(20) not null,
  StuSex nchar(2) default('男') check(StuSex='男' or StuSex='女') not null,
  StuPhone char(11),
)

create table ClassInfo
(
  ClassID int primary key identity(1,1),
  ClassNum char(15) not null,
  ClassName nvarchar(30) not null,
  ClassRemark char check(ClassRemark>=0 and ClassRemark<=1000000) ,
  StuID int
)