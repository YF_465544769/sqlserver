create  database  Students
on(
	name='Students',
	filename='D:\Students.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)

log on
(
   name='Students_log',
	filename='D:\Students_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
go 
use Students
go
create table StuInfo
(
	StuID  int primary key identity(1,1) not null,
	StuNum   varchar(10)   not null,
	StuName   nvarchar(20)  not null,
	StuSex    nvarchar(1) check(StuSex='��' or StuSex='Ů' ) default('��')  not null,
	StuPhone   varchar(20) unique    
)
go
use  Students
go  
create table ClassInfo
(
	ClassID int primary key identity(1,1) not null,
	ClassNum  varchar(15) not null,
	ClassName  nvarchar(30) not null,
	ClassRemark  ntext ,
	StuID int references Stulnfo(StuID) not null
)
