create database Student
on(
name= 'Student',
filename='C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\\Student.mdf',
size=5,
maxsize=50,
filegrowth=2
)
log on
(
name='Student-log',
filename='C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\\Student-log,ldf',
size=5,
maxsize=50,
filegrowth=2
)
go

use Student
go

create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum char(10) not null,
	StuName nvarchar(20) not null,
	StuGender char(2) check(StuGender='��' or StuGender='Ů') default('��') not null,
	StuPhone decimal(11,0)
)
create table ClassInfo
(
	ClassID int primary key identity(1,1) not null,
	ClassNum char(15) not null,
	ClassName nvarchar(30) not null,
	ClassRemark text,
	StuID int foreign key references StuInfo(StuID) 
)