use tempdb
go

if exists(select * from sys.databases where name='tempdb')
	drop database tempdb

create database tempdb
on
(
	name='tempdb',
	filename='C:\Users\Allmy\Documents\SQL Server Management Studio',
	size=6MB,
	maxsize=100MB,
	filegrowth=10Mb
)
log on
(
	name='tempdb_log',
	filename='C:\Users\Allmy\Documents\SQL Server Management Studio',
	size=6MB,
	maxsize=100MB,
	filegrowth=10Mb
)
go

use tempdb
go

create table Teachers
(
	TeacherID int primary key identity(1,1) not null,
	TeacherName nvarchar(20) not null,
	TeacherSalary money check(TeacherSalary>=0 and TeacherSalary<=1000000) not null ,
	TeacherGender char(2) default('��') check(TeacherGender='��' or TeacherGender='Ů') not null,
	TeacherBirth datetime 
)