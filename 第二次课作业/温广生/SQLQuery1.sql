create database Chapter02
on
(
	name='Chapter02',
	filename='D:\project\Chapter02.mdf',
	size=6mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='Chapter02_log',
	filename='D:\project\Chapter02.ldf',
	size=6mb,
	maxsize=100mb,
	filegrowth=10mb
)
go


create table Teachers
(
	TeacherID int primary key identity(1,1) not null,
	TeacherName nvarchar(20) not null,
	TeacherSalary money check(TeacherSalary>=0 and TeacherSalary<=1000000) not null,
	TeacherGender char(2) default('��') check(TeacherGender='��' or TeacherGender='Ů') not null,
	TeacherBirth datetime

)