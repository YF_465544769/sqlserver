create database Chapter02
on
(
	name='Chapter02',
	filename='D:\Chapter02.mdf',
	size=6MB,
	maxsize=100MB,
	filegrowth=10Mb
)
log on
(
	name='Chapter02_log',
	filename='D:\Chapter02_log.ldf',
	size=6MB,
	maxsize=100MB,
	filegrowth=10Mb
)
use Chapter02
go
create table students

(
	studentID int primary key identity(1,1)not null,
	studentNum nvarchar(10) not null,
	studentName nvarchar(20) not null,
	studentSex char(1) default('��') check(studentSex='��' or studentSex='Ů') not null,
	studentPhone int
)
use Chapter02
go
create table class 
(
	ClassID int primary key identity(1,1)not null,
	ClassNum nvarchar(10) not null,
	ClassName nvarchar(20) not null,
	ClassRemark ntext ,
	foreign key (ClassID) references students (StudentID )
)