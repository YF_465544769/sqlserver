create database Student
on
(
	name='Stuinfo',
	filename='D:\Demo\Stuinfo.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='Stuinfo_log',
	filename='D:\Demo\Stuinfo_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
go
use Student

create table Stuinfo
(	
	stuNO varchar(5) unique not null,
	stuName nvarchar(20),
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='男' or stuSex='女')
)
go

create table Scoreinfo
(
	examNO int primary key identity(1,1),
	stuNO varchar(5) references Stuinfo(stuNO),
	writtenExan int,
	labExam int,
)
insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)

insert into Stuinfo values ('s2501','张秋利','20','美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')

select stuNO as 学号,stuName as 姓名,stuAge as 年龄,stuAddress as 地址,stuSex as 性别 from Stuinfo --  给列添加名称注释
select stuName,stuAge,stuAddress from Stuinfo					--查询信息
select 学号=stuNO, 笔试=writtenExan,机试=labExam from Scoreinfo				--题三
select stuNO as 学号,writtenExan as 笔试,labExam as 机试 from Scoreinfo		--题三
select stuNO 学号 ,writtenExan  笔试 ,labExam 机试 from Scoreinfo			--题三
select stuNO as 学号,writtenExan as 笔试,labExam as 机试,writtenExan+labExam as 总分 from Scoreinfo --查询 加总分
select stuNO as 学号 ,stuName as 姓名 ,stuAddress as 地址,stuName+'@'+stuAddress as 邮箱 from Stuinfo		--改名
select distinct stuAddress as 地址 from Stuinfo -- distinct 地址去重
select distinct stuAge as 所有年龄 from Stuinfo 
select top 3 * from  Stuinfo				  --前三行排序
select top 3 * from  Stuinfo  order by 1 desc --倒数三行 1代表列名

select top 4 stuName,stuSeat from Stuinfo     
select top 50 percent *from Stuinfo			 
select * from Stuinfo where stuAddress='湖北武汉' and stuAge='20' 
select labExam from Scoreinfo where labExam>=60 and labExam <=90 order by labExam DESC
select * from Stuinfo where stuAddress='湖北武汉' or stuAddress='湖南长沙'
select * from Stuinfo where stuAddress in('湖北武汉','湖南长沙')
select writtenExan from Scoreinfo where not writtenExan>=70 and writtenExan<=90 order by writtenExan
select *from Stuinfo where stuAge is null
select *from Stuinfo where stuAge is not null
select * from Stuinfo where stuName like '张%'
select * from Stuinfo where stuAddress like '%湖%'
select *from Stuinfo where stuName like '陈_'
select *from Stuinfo where stuName like '__俊%'
select *from Stuinfo  order by stuAge DESC
select *from Stuinfo  order by stuAge DESC,stuSeat ASC
select top 1 * from Scoreinfo order  by writtenExan DESC
select top 1 * from Scoreinfo order  by labExam ASC



