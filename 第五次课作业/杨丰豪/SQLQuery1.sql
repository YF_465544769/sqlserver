create database Student
on
(
name='Student',
filename='D:\test.mdf',
size=5mb,
maxsize=50mb,
filegrowth=50mb
)

log on
(
name='Student_log',
filename='D:\test_log.ldf',
size=5mb,
maxsize=50mb,
filegrowth=50mb
)
use master
go
create table Stuinfo
(
    StuSeae int primary key identity(1,1),
    StuNo char(100) not null,
	StuName nvarchar(20) not null,
	StuAge char(3) not null,
	StuAddress nvarchar(20) not null,
    StuSex char(1)check(StuSex in('女','男'))
)

go
create table fraction
(
	ExanNo int primary key identity(1,1),
	StuNo char(10) not null,
	Wittenexam int ,
	Labexam int
)

go
insert into Stuinfo(StuNo,StuName,StuAge,StuAddress,StuSex) values ('1','东东',19,'广西',1),('2','张飞',18,'湖北武汉',2),
('3','小爱',18,'北京',0),('4','小的',21,'湖北长沙',0),('5','谢三俊',20,'广西',1),('6','小与',19,'福建',1),
('7','陈',20,'河北',0)
insert into fraction(StuNo,Wittenexam,Labexam) values('1',54,78),('2',60,86),('3',78,84),('4',47,84),('5',77,80),('6',67,74)

select StuNo as 学号 from Stuinfo
select StuName as 姓名 from Stuinfo
select StuAge as 年龄 from Stuinfo
select StuAddress as 地址 from Stuinfo
select StuSeae as 座位号 from Stuinfo
select StuSex as 性别 from Stuinfo

select StuNo 学号,Wittenexam 笔试,Labexam 机试 from fraction
select StuNo as 学号,Wittenexam as 笔试,Labexam as 机试 from fraction
select 学号=StuNo, 笔试=Wittenexam, 机试=Labexam from fraction

select StuNo 学号,StuName 姓名, StuAddress 地址 ,StuName+'@'+StuAddress 邮箱 from Stuinfo

select StuNo 学号,Wittenexam 笔试,Labexam 机试,Labexam+Wittenexam 总分  from fraction

select distinct StuAddress from Stuinfo

select distinct StuAge as 所有年龄 from Stuinfo

select top 3 * from Stuinfo

select top 4 StuName,StuSeae from Stuinfo

select top 50 percent * from Stuinfo

select * from Stuinfo where StuAddress='湖北武汉' and StuAge=20

select * from Fraction where Labexam>60 and Labexam<80 order by Labexam desc

select * from Stuinfo where stuAddress='湖北武汉' or stuAddress='湖南长沙'

select * from Stuinfo where (stuAddress in('湖北武汉','湖南长沙'))

select * from fraction where Wittenexam like '[^7,8,9]%' order by Wittenexam asc

select * from Stuinfo where StuAge is null or StuAge=''

select * from Stuinfo where StuAge like '%'

select * from Stuinfo where StuName like '张%'

select * from Stuinfo where StuAddress like '湖%'

select * from Stuinfo where StuName like '张_'

select * from Stuinfo where StuName like '__俊%'

select * from Stuinfo  order by StuAge desc

select * from Stuinfo  order by StuAge desc,StuSeae asc

select top 1 * from fraction order by Wittenexam desc

select top 1 * from fraction order by Wittenexam 