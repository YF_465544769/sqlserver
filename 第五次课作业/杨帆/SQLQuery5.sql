use Student
go

create table StuInfo
(
	stuNO varchar(10) primary key,
	stuName nvarchar(20) not null,
	stuAge int check(stuAge >= 0 or stuAge <= 150),
	stuAddress nvarchar(100),
	stuSeat int identity,
	stuSex int check(stuSex = 0 or stuSex = 1) default(0)

)

create table Exam
(
	examNO int primary key identity,
	stuNO varchar(10) foreign key references StuInfo(stuNO),
	writtenExam int,
	labExam int
)

insert into StuInfo values('s2501', '������', 20, '�������', 1 )

insert into StuInfo
select 's2502', '��˹��', 18, '�����人', 0

insert into StuInfo
select 's2503', '���Ĳ�', 22, '���ϳ�ɳ', 1 union
select 's2504', 'ŷ������', 21, '�����人', 0 union
select 's2505', '÷����', 20, '�����人', 1 union
select 's2506', '������', 19, '�������', 1 union
select 's2507', '�·�', 20, '�������', 0
go

select * from StuInfo

insert into Exam
select 's2501', 50, 70 union
select 's2502', 60, 65 union
select 's2503', 86, 85 union
select 's2504', 40, 80 union
select 's2505', 70, 90 union
select 's2506', 85, 90
go

select stuNO "ѧ��", stuName "����", stuAge "����", stuAddress "��ַ", stuSeat "����", stuSex "�Ա�" from StuInfo
select stuName, stuAge, stuAddress from StuInfo
select stuNO "ѧ��", writtenExam "����", labExam "����" from Exam
select stuNO, stuName, stuAddress, stuName+'@'+stuAddress "����" from StuInfo
select stuNO "ѧ��", writtenExam "����", labExam "����", labExam+writtenExam "�ܷ�"  from Exam
select distinct stuAddress from StuInfo
select distinct stuAge "��������" from StuInfo
select top 3 * from StuInfo
select top 4 stuName, stuSeat from StuInfo
select top 50 percent * from StuInfo
select * from StuInfo where stuAddress = '�����人' and stuAge = 20
select * from StuInfo "S"
	join Exam "E"
	on "S".stuNO = "E".stuNO
	where labExam between 60 and 80
	order by labExam desc
 
select * from StuInfo
	where stuAddress = '�����人' or stuAddress = '���ϳ�ɳ'
select * from StuInfo where stuAddress in ('�����人','���ϳ�ɳ')
select * from Exam
	join StuInfo
	on Exam.stuNO = StuInfo.stuNO
	where labExam not between 70 and 90

select * from StuInfo where stuName is null or stuName = ''
select * from StuInfo where stuName is not null or stuName <> ''
select * from StuInfo where stuName like '��%'
select * from StuInfo where stuName like '__��%'
select * from StuInfo
	order by stuAge desc

select * from Exam
	where writtenExam not in 
	(
		select distinct "E1".writtenExam from Exam "E1"
			join Exam "E2"
			on "E1".writtenExam < "E2".writtenExam
	)

select top 1 * from Exam
	order by labExam asc