create database Students1
on
(
name='Students1',
filename='D:\test\Students1.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10%
) 
log on
(
name='Students1_log',
filename='D:\test\Students1_log.ldf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)
go
use Students1
go
create table StudentInfo
(
StuNO varchar(10) primary key,
StuName nvarchar(10) ,
StuAge int ,
StuAddress nvarchar(20),
StuSeat int,
StuSex nchar(2) default(1) check(stuSex in(0,1)
)
select * from StudentInfo
	insert into StudentInfo values ('s2501','张秋利',20,'美国硅谷',1,1),('s2502','李斯文',18,'湖北武汉',2,0),
	('s2503','马文才',22,'湖南长沙',3,1),('s2504','欧阳俊雄',21,'湖北武汉',4,0),
	('s2505','梅超风',20,'湖北武汉',5,1),('s2506','陈旋风',19,'美国硅谷',6,1),('s2507','陈风',20,'美国硅谷',7,0)

	select StuNo 学号 ,StuName 姓名, StuAge 年龄, StuAddress 地址, StuSeat 座位号, StuSex 性别 from StudentInfo
	select StuName 姓名, StuAge 年龄, StuAddress 地址 from StudentInfo
	select StuNo 学号 ,StuName 姓名, StuAddress 地址, StuName + '@'+ StuAddress 邮箱  from StudentInfo
	select StuAddress 地址  from StudentInfo
	select StuAge 所有年龄 from StudentInfo
	select top 3 * from StudentInfo
	select top 4 StuName 姓名, StuSeat 座位号 from StudentInfo
	select top 50 percent  * from StudentInfo
	select *  from StudentInfo where StuAddress = '湖北武汉' and StuAge = 20
	select *  from StudentInfo where  StuAddress in ('湖北武汉' ,'湖南长沙')
	select *  from StudentInfo where  StuAddress = '湖南长沙' or  StuAddress = '湖北武汉'
	select *  from StudentInfo where StuAge is null 
	select *  from StudentInfo where StuAge is not null 
	select *  from StudentInfo where  StuName like '张%'
	select *  from StudentInfo where StuAddress like '湖%'
	select *  from StudentInfo where  StuName like '张_'
	select *  from StudentInfo where  StuName like '__俊%'
	select *  from StudentInfo order by StuAge DESC
	select *  from StudentInfo order by StuAge DESC,StuSeat ASC




	if exists(select * from sys.objects where name = 'StuExamInfo')
	drop table StuExamInfo
	create table StuExamInfo
	(
	ExamNo int primary key identity(1,1),
	StuNo varchar(10) references StudentInfo(StuNo),
	WrittenExam int,
	LadExam int
	)

	select * from StuExamInfo
	insert into StuExamInfo (WrittenExam,LadExam) values (50,70),(60,65),(86,85),
	(40,80),(70,90),(85,90)
	
	select StuNo as 学号,WrittenExam as 笔试 , LadExam as 机试 from StuExamInfo
	select StuNo 学号,WrittenExam 笔试 , LadExam 机试 from StuExamInfo
	select  学号=StuNo , 笔试 =WrittenExam ,机试 = LadExam from StuExamInfo

	select StuNo 学号,WrittenExam 笔试 , LadExam 机试 , WrittenExam +LadExam 总分 from StuExamInfo
	--排序，order by 字段名 ASC/DESC，默认是升序的，从小到大 ASC，可以省略
	select * from StuExamInfo order by LadExam DESC
	select top 3 * from StuExamInfo order by WrittenExam  
	select top 1 * from StuExamInfo order by WrittenExam DESC
	select top 1 * from StuExamInfo order by LadExam 
