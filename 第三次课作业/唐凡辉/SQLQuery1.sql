create table	class
(classID int primary key identity(1,1),
ClassName nvarchar(20) unique  not null,
)
create table Student(
StuID int primary key identity(1,1),
ClassID int not null,
StuNam nvarchar(20) not null,
StuSex nvarchar(1) check(StuSex='��'or StuSex='Ů'),
StuBirthday date null,
StuPhone nvarchar(11) null unique,
StuAddress nvarchar(200) null,
)
create table Course(
CourseID int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int default'1' not null check(CourseCredit=1 or CourseCredit=2 or CourseCredit=3 or CourseCredit=4 or CourseCredit=5)
)
create table Score(
ScoreID int  identity,
StuID int ,
CourseID int ,
Score decimal(5,2) unique not null,
)
