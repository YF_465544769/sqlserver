
	create database Student
	
	--班级信息表
	create table Class
	(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
	)

	--学生信息表
	create table Student
	(
	StuID int primary key identity(1,1),
	ClassID int ,
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check (StuSex = '男' or StuSex = '女'),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	)

	--课程信息表
	create table Course
	(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCridit int default(1-5) not null
	)

	--成绩信息表
	create table Score
	(
	ScoreID int ,
	StuID int ,
	CourseID int ,
	Score decimal(5,2) unique not null
	)

	alter table score add ScoreID int primary key identity(1,1)
	alter table Student add constraint FK_Student_ClassID foreign key(ClassID ) references Class(ClassID )
	alter table Student add StuAddress nvarchar(200)
	alter table Score add constraint FK_Score_StuID  foreign key(StuID ) references Student(StuID ) 
	alter table Score add constraint FK_Score_CourseID  foreign key(CourseID) references Course(CourseID)