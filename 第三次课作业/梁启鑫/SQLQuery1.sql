create database Student

go
use Student
go
create table ClassInfo
(
	ClassID int primary key identity (1,1),
	ClassName  nvarchar(20) unique not null
)
create table StudentInfo
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references ClassInfo(ClassID), 
	StuName nvarchar(20) not null,
	StuGender nvarchar(1) check(StuGender in('��','Ů')),
	StuBirthday date,
	StuPhone bigint unique,
)
create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(CourseCredit>0 and CourseCredit<6)
)
create table Score
(
	ScoreID int identity(1,1),
	StuID int not null,
	CourseID int not null,
	Score decimal(5,2) unique
)


alter table StudentInfo add stu32Address nvarchar(200)

alter table Score add constraint PK_Score_ScoreID primary key(ScoreID) 

alter table Score add constraint FK_Score_StuID foreign key(StuID) references StuInfo(StuID)