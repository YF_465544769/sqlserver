create database Student
use Student
go

create table Class
(
	Classld int primary key identity(1,1),
	ClassName nvarchar(20) unique not null,
)
drop table Class

insert into Class values ('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班'),('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')

delete from Class where Classld='10'
update Class set ClassName='软件15班' where Classld=1
select * from Class

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int,
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex in('男','女')),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200),
	CreateDate DateTime Default(getdate()),
)
select *from Student
alter table Student add constraint Student_ClassID_Classld foreign key(ClassID) references Class(Classld)
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '1','小a','男','13205095181','阿三地方了阿空间'
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '2','小b','女','13205095182','阿三地方了啊啊空间'
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '3','小c','女','13205095183','阿三地了空间'
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '4','小d','男','13205095184','阿三地方阿了空间' 
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '5','小e','女','13205095185','阿三地了空间' union
select '6','小f','男','13205095186','阿三地方了空间'
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '7','小g','男','13205095187','阿三地方了空间' union
select '8','小h','女','13205095188','阿三地方了空间' union
select '9','小o','女','13205095189','阿三地方了空间' union
select '9','小p','女','12205095182','阿三地方了空间'
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '8','小m','男','11205095182','阿三地方了空间' union
select '7','小z','男','14205095182','阿三地方了空间' union
select '6','小x','女','15205095182','阿三地方了空间' union
select '5','小c','男','16205095182','阿三地方了空间' union
select '4','小v','女','17205095182','阿三地方了空间' union
select '3','小q','男','18205095182','阿三地方了空间' union
select '2','小w','女','19205095182','阿三地方了空间' union
insert into Student (ClassID,StuName,StuSex,StuPhone,StuAddress)
select '1','小狗','男','13205095151','阿三地方了阿空间'
select *from Student
update Student set CreateDate=getdate()
delete from Student where ClassID=2

create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default(1-5),
	CourseCreditt nvarchar(10) check(CourseCreditt in('专业课','公共课'))
)
select * from Course

insert into Course(CourseName)
select '语文' union
select '数学' union
select '英语' union
select 'java' union
select 'c++' union
select 'python'

update Course set CourseCredit=3 where CourseName=('数学')

create table Score
(
	Scoreld int primary key identity(1,1),
	Stuld int,
	Courseld int,
	Score decimal(5,2) unique not null,
)

alter table Score add constraint Fk_Score_Stuld foreign key(Stuld) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(Courseld) references Course(CourseID)

insert into Score(Score)
select '90' union
select '80' union
select '50' union
select '30' union
select '40' union
select '20' union
select '10' union
select '70' union
select '60' union
select '95' union
select '15' union
select '25' union
select '35' union
select '45' union
select '55' union
select '65' union
select '75' union
select '85' union
select '190' union
select '290'

select * from Score

--update Score set 

delete from Score where Stuld=1
delete from Score where Scoreld=1
--alter table Score add constraint yueshu_Score Score default(0)