
create database Student 
on
(
	name='Student',		
	filename='D:\Student.mdf',
	size=5mb,			
	maxsize=50mb,		
	filegrowth=10%		
)
log on
(
	name='Student_log',		
	filename='D:\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student	
go

create table Class		
(  		
	ClassID int primary key identity(1,1),		
	ClassName nvarchar(20) unique not null		
)
create table Student
(
	StuID int primary key identity(1,1),		
	ClassID int references Class(ClassID),		
	StuName nvarchar(20) not null,				
	StuSex nvarchar(1) check(Stusex='��' or Stusex='Ů'), 
	StuBirthday date,							
	StuPhone nvarchar(11) unique				
)
create table Course
(
	CourseID int primary key identity(1,1),		
	CourseName  nvarchar(50) unique not null,
	CourseCredit int default(1) check(CourseCredit>'1' or CourseCredit<'5') not null, 
)
create table Score
(
	ScoreID int not null,
	StuID int,
	CourseID int,
	Score decimal(5,2) unique not null
)

