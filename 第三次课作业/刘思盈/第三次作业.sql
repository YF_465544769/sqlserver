if exists(select*from sys.databases where name = 'Student')
drop database Student
create database Student

on
(
name='Student',
filename='D:\test\Student.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
log on
(
name='Student_log',
filename='D:\test\Student_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
go
use Student
go
create table ClassInfo
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique not null

)
create table StuInfo
(
StuID int primary key identity(1,1),
ClassID int foreign key references ClassInfo(ClassID),
StuName nvarchar(20) not null,
StuSex nvarchar(1) check(StuSex='��'or StuSex='Ů'),
StuBirthday date,
StuPhone nvarchar(11) unique
)
create table CourseInfo
(
CourseID int primary key identity(1,1),
 CourseName nvarchar(50) unique not null,
 CourseCredit int default(1) not null,

)
Create table ScoreInfo
(
ScoreID int  identity(1,1),
StuID int ,
CourseID int,
Score decimal(5,2) unique not null


)
use Student
go
alter table StuInfo add StuAddress  nvarchar(20)


alter table ScoreInfo add constraint IX_ScoreInfo primary key(ScoreID)
alter table ScoreInfo add constraint IX_ScoreInfo foreign key(StuID)references StuInfo(StuID)
alter table ScoreInfo add constraint IX_ScoreInfo foreign key(CouseID)references CourseInfo(CourseID)
