use master --使用系统数据库
go

create database Student --创建Student库
on
(
	name='Student',		--逻辑名
	filename='D:\Students.mdf', --物理名
	size=5mb,			--初始大小
	maxsize=50mb,		--最大值
	filegrowth=10%		--每次增长增量
)
log on
(
	name='Student_log',		
	filename='D:\Students_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student	--切换到Student库
go

create table Class		--创建表
(   --列名  类型  约束			
	ClassID int primary key identity(1,1),		--int型，primary 主键增量1
	ClassName nvarchar(20) unique not null		--nvarchar型 唯一约束unique 非空
)
create table Student
(
	StuID int primary key identity(1,1),		--int型，primary 主键增量1 
	ClassID int references Class(ClassID),		--int型, references 外键关联 Class表
	StuName nvarchar(20) not null,				--nvarchar型 非空
	StuSex nvarchar(1) check(Stusex='男' or Stusex='女'), --nvarchar型 check条件约束
	StuBirthday date,							--date型 日期
	StuPhone nvarchar(11) unique				--nvarchar型 unique 唯一约束 
)
create table Course
(
	CourseID int primary key identity(1,1),		--int型 primary 主键 
	CourseName  nvarchar(50) unique not null,	--nvarchar型 唯一约束 非空
	CourseCredit int default(1) check(CourseCredit>'1' or CourseCredit<'5') not null, --int型 default 默认约束 check 条件约束
)
create table Score
(
	ScoreID int not null,
	StuID int,
	CourseID int,
	Score decimal(5,2) unique not null
)

--加字段
--alter table 表名 add 字段名 数据类型 约束
alter table Student add StuAddress nvarchar(200)


--添加约束 
--alter table 表名 add constraint 约束名 约束的类型
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)
alter table Score add consrraint UK_Score_Score unique(Score)























