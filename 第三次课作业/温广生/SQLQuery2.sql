create database Student

use Student
go
create table classInfo
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null,
)
create table Student
(
	StuID int primary key identity(1,1),
	ClassID int,
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('��') check(StuSex in('��','Ů')),
	StuBirthday date,
	StuPhone nvarchar(11) unique not null,
)
create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default(1-5),

)create table Score
(
	ScoreID int,
	StuID int,
	CourseID int,
	Score decimal(5,2) unique not null,

)
alter table Score add ScoreID int primary key identity(1,1)

alter table Student add StuAddress nvarchar(200)

alter table Student add constraint FK_Student_ClassID foreign key(ClassID) references Classinfo(ClassID)

alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)

alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)