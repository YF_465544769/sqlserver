create database ATMinfo
on
(
name='ATMinfo',
filename='D:/bank/ATMinfo.mdf',
size=5MB,
maxsize=100MB,
filegrowth=15%
)
log on
(
name='ATMinfo_log',
filename='D:/bank/ATMinfo_log.ldf',
size=5MB,
maxsize=100MB,
filegrowth=15%
)
go
use ATMinfo
go

create table userInfo 
(
customerID int primary key identity ,
customerName nvarchar(5)  not null,
PID varchar(18) not null check(PID='15' or PID='18') unique ,
telephone  nvarchar(18) not null check(len(telephone)=15 or len(telephone)=18),
address nvarchar(50) ,
)
go

create table cardInfo
(
cardID varchar(20) not null primary  key check(cardID like '1010 3576 ____ ___'), 
curType money  default('RMB') not null ,
savingType nvarchar(5) check(savingType='定期' or savingType='活期' or savingType='定活两便'),
openDate datetime default(getdate()),
balance int not null check(balance>1),
pass int not null check(len(pass)=6) default(888888),
IsReportLoss  nvarchar(5) check(IsReportLoss='是' or IsReportLoss='否')  default('否'),
customerID int foreign key  references userInfo (customerID)  not null
)
go

create table transInfo
(
transId int primary key identity ,
transDate datetime not null default(getdate()),
cardID varchar(20) not null foreign key references cardInfo(cardID),
transType nvarchar(5) check(transType ='支取' or transType ='存入'),
transMoney money not null check(transMoney>0),
remark text 
)
go

insert into userInfo values(1,'孙悟空','123456789012345','0716-78989783','北京海淀')
insert into cardInfo(balance,savingType,cardID) values(1000,'活期','1010 3576 1234 567')
go


insert into userInfo values(2,'沙和尚','421345678912345678','0478-44223333','海淀')
insert into cardInfo(balance,savingType,cardID) values(1,'定期','1010 3576 1212 117')
go


insert into userInfo values(3,'唐僧','321245678912345678','0478-44443333','河南')
insert into cardInfo(balance,savingType,cardID) values(1,'定期','1010 3576 1212 113')
go


update cardInfo set balance='611234'


select *from userInfo
insert into transInfo  values('111','2020_10_11','1010 3576 1234 567','支取','200')
update cardInfo set balance=balance-200 where customerID=1