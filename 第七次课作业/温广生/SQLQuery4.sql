create database ATM
on
(
	name='ATM',
	filename='D:\bank\ATM.mdf',
	size=5,
	maxsize=10,
	filegrowth=15%
)
log on
(
	name='ATM_log',
	filename='D:\bank\ATM.ldf',
	size=5,
	maxsize=10,
	filegrowth=15%
)
go
use ATM
go

create table userInfo
(
	customerID int primary key identity,
	customerName varchar(20) not null,
	PID varchar(20) not null check(len(PID)=15 or len(PID)=18),
	telephone varchar(20) not null check(len(telephone)=13 and telephone like '____-________'),
	address text
)

create table cardInfo
(
	cardID varchar(20)  check(cardID like '1010 3576 ____ ___') primary key not null,
	curType varchar(20) default('RMB') not null,
	savingType varchar(10) check(savingType in ('活期','定活两便','定期')),
	openDate datetime not null default getdate(),
	balance int not null  check(balance>=1),
	pass int not null check(len(pass)=6) default('888888'),
	IsReportLoss char(2) not null default('否') check(IsReportLoss in('是','否')),
	customerID int foreign key references userInfo(customerID)
)
 ---check(len (cardID)=15 and cardID like '____ ____ ____ ___'))

create table transInfo
(
	transInfo int primary key identity ,
	transDate datetime not null default getdate(),
	cardID varchar(20) not null,
	transType varchar(20) not null check(transType in('存入','支取')),
	transMoney varchar(20) not null check(transMoney>0),
	remark text
)
--alter table cardInfo alter column check(cardID like '____ ____ ____ ___'))
--customerID	顾客编号	自动编号（标识列），从1开始，主键
--customerName	开户名	必填
--PID	身份证号	必填，只能是18位或15位，身份证号唯一约束
--telephone	联系电话	必填，格式为xxxx-xxxxxxxx或手机号13位
--address	居住地址	可选输入

insert into userInfo (customerName,PID,telephone,address)
select '孙悟空开户','123456789012345','0716-78989783','北京海錠' union
select '沙和尚开户','421345678912345678','0478-44223333','北京海錠' union
select '唐僧开户','321245678912345678','0478-44443333','北京海錠'
select *from userInfo
--cardID	卡号	必填，主健，银行的卡号规则和电话号码一样，一般前8位代表特殊含义，如某总行某支行等。假定该行要求其营业厅的卡号格式为：1010 3576 xxxx xxx开始,每4位号码后有空格，卡号一般是随机产生。
--curType	货币种类	必填，默认为RMB
--savingType	存款类型	活期/定活两便/定期
--openDate	开户日期	必填，默认为系统当前日期
--balance	余额	必填，不低于1元,否则将销户
--pass	密码	必填，6位数字，开户时默认为6个“8”
---IsReportLoss	是否挂失	必填，是/否值，默认为”否”
--customerID	顾客编号	外键，必填，表示该卡对应的顾客编号，一位顾客允许办理多张卡号
 
alter table cardInfo add constraint FK_cardInfo_customerID foreign key(customerID) references userInfo(customerID)
insert into cardInfo (balance,savingType,cardID) 
select 1000,'活期','1010 3576 1234 567' union
select 1,'定期','1010 3576 1212 117' union
select 1,'定期','1010 3576 1212 113'
select *from userInfo
select *from cardInfo
alter table cardInfo drop constraint  FK__cardInfo__custom__1CF15040
update cardInfo set pass=611234 where cardID='1010 3576 1234 567'

insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支出',200)
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'

insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'
--select * from 表 where 时间>=前十天 and 时间<=今天
--select * from 表名 where between 前第十天 and 今天
select * from cardInfo where opendate >='2021-03-12' and opendate<= '2021-03-22'

update cardInfo set IsReportLoss='是' where customerID=3