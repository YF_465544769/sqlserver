use master
go
create database bbs
on
(
 name='bbs',
 filename='D:\新建文件夹.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='bbs_log',
 filename='D:\新建文件夹_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use bbs
go
create table bbsUsers
(
 bbUID int identity,
 uName varchar(10),
 uSex varchar(2),
 uAge int,
 uPoint int
 )
go
use bbs
go
create table bbsSection
(
 bbsID int identity,
 sName varchar(10) not null,
 sUid int references bbsUsers(bbUID)
 )
go
use bbs
go
create table bbsTopic
(
 tID int primary key identity,
 tUID int references bbsUsers(bbUID),
 tSID int references bbsSection(bbsID),
 tTitle varchar(100) not null,
 tMsg text not null,
 tTime datetime,
 tCount int  
 )
go
use bbs
go
create table bbsReply
(
 rID int primary key identity,
 rUID int references bbsUsers(bbUID),
 rTID int references bbsTopic(tID),
 rMsg text not null,
 rTime datetime 
 )
 --添加约束
alter table bbsUsers add constraint PK_bbsUsers_bbUID primary key(bbUID)

alter table bbsUsers add constraint UK_bbsUsers_uName unique (uName) 

alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男'or uSex='女')

alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60) 

alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_bbsID primary key(bbsID)

alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(bbUID)

insert into bbsUsers(uName,uSex,uAge,uPoint) 
values('小雨点','女',20,0),
      ('逍遥','男',18,4),
      ('七年级生','男',19,2)
--二2.
select uName,uPoint into bbsPoint from bbsUsers

--二3.
insert into bbsSection(sName,sUid) 
values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)


insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) 
values(2,4,'范跑跑','谁是范跑跑',2008-7-8,1),
      (3,1,'.NET','与JAVA的区别是什么呀？',2008-9-1,2),
      (1,3,'今年夏天最流行什么',' 有谁知道今年夏天最流行什么呀？',2008-9-10,0)


insert into bbsReply(rUID,rTID,rMsg,rTime) 
values(1,3,'地震跑路人',2020-1-1),
(1,2,'区别很大',2020-2-1),
(2,2,'流行敲代码',2020-3-1)


--以下题目基于之前的bbs数据库
--1、删除字段：删除主贴表的“回帖数量”字段
--2、新增字段：用户表新增“手机号码”字段，并为该字段添加唯一约束，且长度为11，必填
--3、修改字段：修改回帖表的“回帖消息”字段，数据类型改成varchar(200)
--4、删除约束：删除用户表的“积分”字段的检查约束
--5、修改数据：修改用户“小雨点”的名字改成“小雪”，将所有用户的积分都增加100
--6、复制用户表的数据到新表bbsUser2
--7、删除bbsUser2的所有数据，写出两种方式的语句

--1.
alter table bbsTopic drop column tCount
--2.
alter table bbsUsers add bbsNum varchar(11) 
--3.
alter table bbsReply alter column rMsg varchar(200)
--4.
alter table bbsUsers drop constraint CK_bbsUsers_uPoint
--5.
update bbsUsers set uName='小雪' where bbUID=1  
update bbsUsers set uPoint=uPoint+100 
--6.
select * into bbsUsers2 from bbsUsers
--7.
drop table bbsUsers2
truncate table bbsUsers2