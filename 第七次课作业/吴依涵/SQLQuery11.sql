use master
 go
 create database ATM
 on
 (
 name='ATM',
 filename='D:\吴.mdf',
 size=6mb,
 maxsize=100mb,
 filegrowth=15%
 )
 log on
 (
 name='ATM_log',
 filename='D:\吴_log.ldf',
 size=6mb,
 maxsize=100mb,
 filegrowth=15%
 )
 go
 use ATM
 create table userInfo 
 (
 customerID bigint primary key identity(1,1), 
 customerName nvarchar(10) not null,
 PID bigint check(len(PID)=18 or len(PID)=15) unique not null,
 telephone varchar(20) check(len(telephone)=13 or telephone='____-________' ) not null,
 address text
 )
 use ATM
 create table cardInfo
 (
 cardID varchar(20) primary key check(cardID like'1010 3576 ____ ___') not null,
 curType nvarchar(5) default('RMB') not null,
 savingType nvarchar(7) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
 openDate datetime default(getdate()) not null,
 balance int check(balance>=1  ) not null,
 pass int default(888888) check(len(pass)=6) not null,
 IsReportLoss nvarchar(1) default('否') check(IsReportLoss='是' or IsReportLoss='否') not null,
 customerID bigint references userInfo(customerID)  not null
 )
 use ATM
 create table transInfo 
 (
 transId bigint identity primary key,
 transDate datetime default(getdate()) not null,
 cardID varchar(20) references cardInfo(cardID),
 transType nvarchar(2) check(transType='存入' or transType='支取'),
 transMoney money not null,
 remark text
 )

insert into userInfo(customerName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783','北京海淀')
insert into cardInfo values ('1010 3576 1234 567', 'RMB','活期',getdate(),1000,888888,'否',1)

insert into userInfo(customerName,PID,telephone) values ('沙和尚',421345678912345678,'0478-44223333')
insert into cardInfo values ('1010 3576 1212 117', 'RMB','定期',getdate(),1,999999,'否',2)

insert into userInfo(customerName,PID,telephone) values ('唐僧',321245678912345678,'0478-44443333')
insert into cardInfo values ('1010 3576 1212 113', 'RMB','定期',getdate(),1,222222,'否',3)


--1.将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where customerID=1

--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(transDate,cardID,transType,transMoney) values(getdate(),'1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'

--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(transDate,cardID,transType,transMoney) values(getdate(),'1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3

--5.查询出最近10天开户的银行卡的信息
select * from cardInfo where openDate>='2021-03-09' and  openDate<='2021-03-25'

select * from cardInfo