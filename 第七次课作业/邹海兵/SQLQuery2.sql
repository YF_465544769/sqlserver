use master
go

create database bank
on
(
	name='bank',
	filename='D:\bank.mdf',
	size=5,
	filegrowth=15%
)
log on
(
	name='bank_log',
	filename='D:\bank_log.ldf',
	size=5,
	filegrowth=15%
)
go

use bank
go

create table userInfo--用户信息表
(
	customerID int primary key identity(1,1),
	customerName nvarchar(10) not null,
	PID varchar(20) check(len(PID)=18 or len(PID)=15),
	telephone varchar(15) check(len(telephone)=13 and telephone like '____-________') not null,
	address text
)
insert into userInfo values('孙悟空',123456789012345,'0716-78989783','北京海淀')
insert into userInfo values('沙和尚',421345678912345678,'0478-44223333',null)
insert into userInfo values('唐僧',321245678912345678,'0478-44443333',null)
select * from userInfo


create table cardInfo--用户信息表
(
	cardID varchar(20) primary key check(cardID like '1010 3576 ____ ___') not null,
	curType nvarchar(10) default('RMB') not null,
	savingType nvarchar(10) check(savingType in('活期','定活','定期')),
	openDate date default(getdate()),
	balance decimal(7,2) check(balance>=1) not null,
	pass varchar(10) default(888888) check(len(pass)=6),
	IsReportLoss nchar(1) default('否') check(IsReportLoss in ('是','否')),
	customerID int references userInfo(customerID)
)
insert into cardInfo values('1010 3576 1234 567','RBM','活期',getdate(),1000,'666666','否',1)
insert into cardInfo values('1010 3576 1212 117','RBM','活期',getdate(),1,'666666','否',2)
insert into cardInfo values('1010 3576 1212 113','RBM','活期',getdate(),1,'666666','否',3)

update cardInfo set pass='611234' where cardID='1010 3576 1234 567'
update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 117'
select * from cardInfo



create table transInfo--交易信息表
(
	teansID int primary key identity(1,1),
	transDate datetime default(getdate()),
	cardID varchar(20) references cardInfo(cardID),
	transType nvarchar(2) check(transType in('存入','取出')),
	transMoney decimal check(transMoney>0) not null,
	remark nvarchar(20)
)
insert into transInfo values(getdate(),'1010 3576 1234 567','取出',200,null)
insert into transInfo values(getdate(),'1010 3576 1212 117','存入',300,null)
select * from  transInfo
select * from cardInfo where openDate like'[(2021-03-09)-(2021-03-19)]'