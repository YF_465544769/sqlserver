create database ATM
on
(
	name='ATM',
	filename='D:\test\ATM.mdf',
	size=5,
	maxsize=50,
	filegrowth=15%
)
log on 
(
	name='ATM_log',
	filename='D:\test\ATM_log.ldf',
	size=5,
	maxsize=50,
	filegrowth=15%
)
go
use ATM
go

create table userInfo
(
	customerID int primary key identity,
	customerName varchar(20) not null,
	PID bigint check(len(PID)=15 or len(PID)=18) unique not null,
	telephone varchar(40) check(len(telephone)=13 or telephone like '____-________') not null,
	address varchar(100)
)
go

create table cardInfo
(
	cardID varchar(40) check(cardID like '1010 3576 ____ ___') primary key not null,
	curType varchar(8) default('RMB') not null,
	savingType varchar(8) check(savingType in('活期','定活两便','定期')),
	openDate datetime default(getdate()) not null,
	balance int check(balance>=1) not null,
	pass varchar(6) default('888888') not null,
	IsReportLoss varchar(2) check(IsReportLoss in('是','否')) default('否') not null,
	customerID int references userInfo(customerID) not null
)
go

create table transInfo 
(
	transId int primary key identity,
	transDate datetime default(getdate()) not null,
	cardID varchar(40) references cardInfo(cardID) not null,
	transType  varchar(4) check(transType in('存入','支取')) not null,
	transMoney int check(transMoney>0) not null,
	remark text
)
go

insert into userInfo values 
('孙悟空',123456789012345,'0716-78989783','北京海淀'),
('沙和尚',421345678912345678,'0478-44223333','null'),
('唐僧',321245678912345678,'0478-44443333','null')
go

insert into cardInfo(cardID,savingType,balance,customerID) values
('1010 3576 1234 567','活期',1000,1),
('1010 3576 1212 117','定期',1,2),
('1010 3576 1212 113','定期',1,3)
go

update cardInfo set pass='611234' where customerID=1
go

insert transInfo(cardID,transType,transMoney,remark) values ('1010 3576 1234 567','支取',200,'孙悟空取出200元') 
go

update cardInfo set balance = balance -200 where customerID = 1

insert transInfo(cardID,transType,transMoney,remark) values ('1010 3576 1212 117','存入',300,'沙和尚存入300元')
go

update cardInfo set balance = balance +300 where customerID = 2
go

update cardInfo set IsReportLoss = '是' where customerID = 3
go

select * from cardInfo where openDate between '2021-03-09' and '2021-03-24'