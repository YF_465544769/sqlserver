create database ATM柜员机系统
on
(
	name = 'ATM柜员机系统',
	filename = 'D:\bank.mdf'
	size=10,
	maxsize=100,
	filegrowth=15%
)
log on
(
	name = 'ATM柜员机系统_log',
	filename = 'D:\bank.ldf'
	size=10,
	maxsize=100,
	filegrowth=15%
)
use ATM柜员机系统
go 
create table userInfo 
(
	customerID int identity(1,1) primary key,
	customerName varchar not null,
	PID int check(PID = 18 or PID = 15) unique not null,
	telephone int check(telephone like '____-________' or telephone=13) not null,
	address text
)
create table cardInfo
(
	cardID int primary key check(cardID like '1010 3576 ____ ____') not null,
	curType varchar default('RMB') not null,
	savingType varchar check(savingType in ('活期,定活两便,定期')),
	openDate varchar default(getdate()) not null,
	balance money check(balance >= 1) not null,
	pass int default(888888) not null,
	IsReportLoss varchar check(IsReportLoss in ('是,否')) default('否'),
	customerID int references userInfo(customerID) not null
)
create table transInfo 
(
	transId int identity(1,1) primary key,
	transDate varchar default(getdate()) not null,
	cardID int references not null,
	transType varchar check(transType in ('存入,支取')),
	transMoney int check(transMoney>0) not null,
	remark text
)
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117
--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113
insert into userInfo(customerName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783','北京海淀 '),('唐僧',321245678912345678,'0478-44443333','null'),
('沙和尚',421345678912345678,'0478-44223333','null')
insert into cardInfo(balance,savingType,cardID,customerID,IsReportLoss)values ('1000','活期','101035761234567','1','否'), ('1','定期','101035761212117','2','否'), ('1','定期','101035761212113','3','否')
update cardInfo set pass = 611234 where customerID = 1
select * from cardInfo
select * from transInfo
select * from userInfo
insert into transInfo(cardID,transType,transMoney,remark) values(101035761234567,'支取',200,'孙悟空要取200')
update cardInfo set balance=(balance -200) where customerID = 1
select cardID from cardInfo where customerID = 3
insert into transInfo(cardID,transType,transMoney,remark) values(101035761212117,'存入',200,'沙和尚要存300')
update cardInfo set balance=(balance +300) where customerID = 3
update cardInfo set IsReportLoss = '是' where customerID = 2

select * from cardInfo where openDate between 2021-3-10 and 2021-3-22

select *from userInfo
select *from cardInfo order by customerID
select *from  transInfo 
