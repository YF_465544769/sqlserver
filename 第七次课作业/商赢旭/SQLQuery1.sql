use master 
go 
create database bank1
on
( name=' bank1',
  filename='D:\bank\ bank1.mdf',
  size=5mb,
  maxsize=5000mb,
  filegrowth=15%

)
  log on
( name=' bank1_log',
  filename='D:\bank\bank1_log.ldf',
  size=5mb,
  maxsize=5000mb,
  filegrowth=15%

)
go
use bank1
go 
create table userInfo 
(	customerID int primary key identity(1,1),
	customerName nvarchar(5)  not null,
    PID varchar(18) unique check(len(PID)=18 or len(PID)=15),
	telephone char(13) check(telephone like '____-________' or len(telephone)=13),   
	address1     nvarchar(200) 
)

create table cardInfo
(	cardID      char(20)     not null primary key  check(substring(cardID,1,9)='1010 3576' and len(cardID)=18),
	curType     nvarchar(10) not null default('RMB'),
	savingType  nvarchar(20)  check(savingType='定期' or savingType='定活两便' or  savingType='活期' ) ,
	openDate    datetime  not null  default(getdate()),
	balance     bigint    not null  check(balance>1), 
	pass        int       not null  default('888888') check(len(pass)=6),
	IsReportLoss nchar(1) not null  default('否'),
	customerID  int  references userInfo(customerID) not null

)
create table transInfo 
(	transId  int primary key identity(1,1),
	transDate datetime not null default(getdate()),
	cardID    char(20)   references cardInfo(cardID)  not null,
	transType nchar(2)  not null check(transType='存入'or transType='支取' ),
	transMoney bigint  not null check(transMoney>0),
	remark      text  
)

select*from userInfo
select*from cardInfo
select*from transInfo 

  insert into userInfo  values('孙悟空',123456789012345,'0716-78989783','北京海淀 '),
                              ('沙和尚',421345678912345678,'478-044223333','湖南'),
							  ('唐僧',321245678912345678,'0478-44443333','武汉')

  insert into cardInfo values( '1010 3576 1234 567',  default ,'活期',default,1000,default,'是',4),
                             ( '1010 3576 1212 117',  default ,'活期',default,3,default,'是',5),
							 ( '1010 3576 1212 113',  default ,'活期',default,5,default,'是',6)

--  第二阶段：增、删、改、查 
--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where customerID=4

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
   select*from userInfo where customerName='孙悟空'
   select cardID  from cardInfo where customerID in ( select customerID from userInfo where customerName='孙悟空')
   
--插入交易金额和修改账上余额：
   insert into transInfo  values(default,'1010 3576 1234 567','支取',200,'在今天花了200元')
   update  cardInfo set balance=balance-200 where customerID in(select customerID from userInfo where customerName='孙悟空')
   

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
   select*from userInfo where customerName='沙和尚'
   select cardID  from cardInfo where customerID in ( select customerID from userInfo where customerName='沙和尚')
   insert into transInfo  values(default,'1010 3576 1212 117','存入',300,'在今天存了300元')
   update  cardInfo set balance=balance+300 where customerID in(select customerID from userInfo where customerName='沙和尚')

--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
    update  cardInfo set IsReportLoss='是' where customerID=( select customerID  from userInfo where customerName='唐僧') 
--5.	查询出最近10天开户的银行卡的信息
select*from cardInfo  where  openDate   between '2021-03-18'and '2021-03-20'

--6.	查询交易金额最大的银行卡信息，子查询实现
select*from transInfo where transMoney=(select max(transMoney) from transInfo )
--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
--  显示效果：
--  总交易金额：1400.00
select sum(transMoney) as 总交易金额 from transInfo 