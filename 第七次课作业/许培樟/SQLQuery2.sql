create database bank
on
(
	name='bank',
	filename= 'D:\bank.mdf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
log on
(
	name='bank_log',
	filename= 'D:\bank_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
go
use bank
go

create table userInfo
(
 customerID int primary key identity(1,1),
 customerName varchar(10) not null,
 PID varchar(20) check(len(PID)=15 or len(PID)=18) unique not null,
 telephone varchar(20) not null check(len(telephone)=13 or telephone like '____-________'),
 address text 
)
create table cardInfo
(
 cardID varchar(30) not null primary key check(cardID like '10103576_______'),
 curType varchar(10) not null default('RMB'),
 savingType varchar(10) check(savingType='活期' or savingType='定活两期' or savingType='定期'),
 openDate datetime default(getdate()) not null,
 balance int check(balance>=1) not null,
 pass int  not null default('888888'),check(len(pass)=6) ,
 IsReportLoss varchar(2) not null default('否') check(IsReportLoss in ('否','是')),
 customerID  int references userInfo(customerID)
)
create table transInfo 
(
 transId int primary key identity(1,1),
 transDate datetime default(getdate()) not null,
 cardID varchar(30) not null references cardInfo(cardID),
 transType nvarchar(2) not null check(transType in ('存入','取出')),
 transMoney money not null check(transMoney>=0),
 remark text 
)
insert into userInfo(customerName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783','北京海淀 '),('唐僧',321245678912345678,'0478-44443333','null'),
('沙和尚',421345678912345678,'0478-44223333','null')
insert into cardInfo(balance,savingType,cardID,customerID)values ('1000','活期','101035761234567','1'), ('1','定期','101035761212117','2'), ('1','定期','101035761212113','3')
update cardInfo set pass=611234 where customerID=1
select * from cardInfo
select * from userInfo
select * from transInfo
insert into transInfo(cardID,transType,transMoney,remark) values ('101035761234567','取出','200','孙悟空要取200块钱')
update cardInfo set balance=(balance -200) where customerID=1
select cardID from cardInfo where customerID=3
insert into transInfo(cardID,transType,transMoney,remark) values ('101035761212113','存入','300','沙和尚存入300块钱')
update cardInfo set balance=(balance +300) where customerID=3
update cardInfo set IsReportLoss='是' where customerID=2 

select * from cardInfo where openDate between 2021-03-09 and 2021-03-21

