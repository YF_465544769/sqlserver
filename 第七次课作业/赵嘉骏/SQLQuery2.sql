use master
go

create database bank
on
(
	name='bank',
	filename='D:\bank\bank.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=15%
)
log on
(
	name='bank_log',
	filename='D:\bank\bank_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=15%
)
go
--用户信息表：userInfo ：
--字段名称	说  明
--customerID	顾客编号	自动编号（标识列），从1开始，主键
--customerName	开户名	必填
--PID	身份证号	必填，只能是18位或15位，身份证号唯一约束
--telephone	联系电话	必填，格式为xxxx-xxxxxxxx或手机号13位
--address	居住地址	可选输入

create table userInfo
(
customerID  int primary key identity(1,1),
customeName  nvarchar(3) not null,
PID char(18) check(len(PID)=15 or len(PID)=18)  unique not null,
telephone varchar(13)  check(len(telephone)=13 or telephone like '____-________') not null,
address nvarchar
)

--银行卡信息表：cardInfo
--字段名称	说  明
--cardID	卡号	必填，主健，银行的卡号规则和电话号码一样，一般前8位代表特殊含义，如某总行某支行等。假定该行要求其营业厅的卡号格式为：1010 3576 xxxx xxx开始,每4位号码后有空格，卡号一般是随机产生。
--curType	货币种类	必填，默认为RMB
--savingType	存款类型	活期/定活两便/定期
--openDate	开户日期	必填，默认为系统当前日期
--balance	余额	必填，不低于1元,否则将销户
--pass	密码	必填，6位数字，开户时默认为6个“8”
--IsReportLoss	是否挂失	必填，是/否值，默认为”否”
--customerID	顾客编号	外键，必填，表示该卡对应的顾客编号，一位顾客允许办理多张卡号
create table cardInfo
(
cardID char(18) primary key  check(cardID like '1010 3576 ____ ___') not null,
curType varchar default('RMB') not null,
savingType varchar check(savingType='活期' or savingType='定活两便' or savingType='定期' ),
openDate float default(getdate()) not null,
balance float check(balance>=1) not null,
pass int check(len(pass)=6)default('888888') not null,
IsReportLoss  varchar default('否')check(IsReportLoss='是'or IsReportLoss='否') not null,
customerID int references userInfo(customerID) not null

)
--交易信息表：transInfo 
--字段名称	说  明
--transId	交易编号	标识列、主键
--transDate	交易日期	必填，默认为系统当前日期
--cardID	卡号	必填，外健，可重复索引
--transType 	交易类型	必填，只能是存入/支取
--transMoney	交易金额	必填，大于0
--remark	备注	可选输入，其他说明
create table transInfo
(
transId int primary key identity(1,1),
transDate float default(getdate()) not null,
cardID char(18) references cardInfo(cardID) not null,
transType varchar check(transType='存入' or transType='支取') not null,
transMoney float check(transMoney>0) not null,
remark varchar
)


--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
  -- 开户金额：1000 活期   卡号：1010 3576 1234 567

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
  -- 开户金额： 1  定期 卡号：1010 3576 1212 117

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
  -- 开户金额： 1  定期 卡号：1010 3576 1212 113

insert into userInfo (customerID,customeName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783')
insert into userInfo (customerID,customeName,PID,telephone) values('沙和尚',421345678912345678,'0478-44223333')
insert into userInfo (customerID,customeName,PID,telephone) values('唐僧',321245678912345678,'0478-44443333')

insert into cardInfo (cardID,savingType,balance,customerID) values ('1010 3576 1234 567','活期',1000,1)
insert into cardInfo (cardID,savingType,balance,customerID) values ('1010 3576 1212 117','定期',1,2)
insert into cardInfo (cardID,savingType,balance,customerID) values ('1010 3576 1212 113','定期',1,3)

--第二阶段：增、删、改、查

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where customerID=1
--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo (cardID,transType,transMoney ) values ('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where customerID=1

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo (cardID,transType,transMoney) values('421345678912345678','存钱',300)
update cardInfo set balance=balance+300 where customerID=2
--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3
--5.	查询出最近10天开户的银行卡的信息
select *from cardInfo where openDate between '2021-03-12' and '2021-03-22'
--6.	查询交易金额最大的银行卡信息，子查询实现
select *from userInfo
select *from cardInfo order by customerID
select *from  transInfo 
--7.	再交易信息表中，将总的交易金额，支取的交易金额，存入的交易金额查询出来并输出显示(可以用变量实现)
  --显示效果：
  --总交易金额：1400.00
  --支取交易金额：200.00
  --存入交易金额：1200.00
