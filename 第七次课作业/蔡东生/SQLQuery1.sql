create database bank
on
(    
       name='bake',
	   filename='D:\bake.mdf',
	   size=10,
	   maxsize=100,
	   filegrowth=10

)
log on
(      name='bake_log',
       filename='D:\bake_log.ldf',
	   size=10,
	   maxsize=100,
	   filegrowth=10


)
go
use bank
go
create table userInfo
( customerID int primary key identity,
  customerName varchar(10 ) not null,
  PID varchar(20) not null unique check(len(PID)=15 or len(PID)=18  ),
  telephone varchar(20) not null check(len(telephone)=13 or telephone like '____-________'),
  address varchar(100) 
  
)
go
create table cardInfo
( cardID varchar(30) not null primary key   check(cardID like '1010 3576_______'),
  curType varchar(10) not null default('RMB'),
  savingType nvarchar(20) check(savingType in ('活期两便','活期','定期')),
  openDate datetime not null default(getdate()),
  balance money not null check(balance>=1),
  pass int not null check(len(pass)=6) default('888888'),
  IsReportLoss nvarchar(10 ) not null default('否') check(IsReportLoss in('否','是')),
  customerID int not null references userInfo(customerID)

)
go
create table transInfo
(    
     
	 transId int primary key identity,
	 transDate datetime not null default(getdate()),
	 cardID varchar(30) not null references cardInfo(cardID),
	 transType nvarchar(10) not null check(transType in('存入','取出')),
	 transMoney money not null check(transMoney>0),
	 remark nvarchar(100)

)
go
insert into userInfo(customerName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783','北京海淀 '),('唐僧',321245678912345678,'0478-44443333','null'),
('沙和尚',421345678912345678,'0478-44223333','null')
select * from userInfo
insert into cardInfo(balance,savingType,cardID,customerID)values ('1000','活期','1010 35761234567','1'), ('1','定期','1010 35761212117','2'), ('1','定期','1010 35761212113','3')
update cardInfo set pass=611234 where customerID=1
select * from cardInfo
insert into transInfo(cardID,transType,transMoney,remark) values ('1010 35761234567','取出','200','孙悟空要取200块钱')
update cardInfo set balance=(balance -200) where customerID=1
select cardID from cardInfo where customerID=3
insert into transInfo(cardID,transType,transMoney,remark) values ('1010 35761212113','存入','300','沙和尚存入300块钱')
update cardInfo set balance=(balance +300) where customerID=3
update cardInfo set IsReportLoss='是' where customerID=2 
select * from cardInfo where openDate>='2021-03-09' and openDate<='2021-03-25' 
select * from transInfo