
use master
go 

create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf'
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf'
)
go

use bbs
go

create table bbsUsers
(
	UID int identity(1,1),
	uName varchar(10) not null,
	uSex varchar(2) not null,
	uAge int not null,
	uPoint int not null
)


create table bbsSection
(
	sID int identity(1,1) ,
	sName varchar(10) not null ,
	sUid int 
)

create table bbsTopic
(
	tId int identity(1,1),
	tUID int,
	tSID int,
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)


create table bbsReply
(
	rId int identity(1,1),
	rUId int,
	rTID int,
	rMsg text not null,
	rTime datetime
)


alter table bbsUsers add constraint PK_bbsUsers_UID primary key (UID)
alter table bbsUsers add constraint UQ_bbsUsers_uName unique (uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男' or uSex='女')
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_sID primary key (sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUID) references bbsUsers(UID) 

alter table bbsTopic add constraint PK_bbsTopic_tID primary key (tID)
alter table bbsTopic add constraint FK_bbsTopic_tUID foreign key(tUID) references bbsUsers(UID)
alter table bbsTopic add constraint FK_bbsTopic_tSID foreign key(tSID) references bbsSection(sID)

alter table bbsReply add constraint PK_bbsReply_rID primary key (rID)
alter table bbsReply add constraint FK_bbsReply_rUID foreign key(rUID) references bbsUsers(UID)
alter table bbsReply add constraint Fk_bbsReply_rTID foreign key(rTID) references bbsTopic(tID)


insert into bbsUsers(uName,uSex,uAge,uPoint)
select'小雨点','女','20','0'union
select'逍遥','男','18','4'union
select'七年级生','男','19','2'


select uName,uPoint into bbsPoint from bbsUsers

insert into bbsSection(sName,sUid)
select'技术交流','3'union
select'读书世界','1'union
select'生活百科','3'union
select'八卦区','1'

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
select '2','1','范跑跑','谁是范跑跑','2008-7-8','1'union
select '1','3','.NET','与Java的区别是什么呀？','2008-9-1','2'union
select '3','4','今年夏天最流行什么呀？','有谁知道今年夏天最流行什么呀？','2008-9-10','0'


insert into bbsReply(rTID,rMsg,rTime,rUId)
select'1','不知道','2008-9-5','2'union
select'2','不认识','2008-9-10','1'union
select'3','不知道','2008-10-1','3'





alter table bbsReply drop constraint FK_bbsReply_rUID
alter table bbsTopic drop constraint FK_bbsTopic_tUID
alter table bbsSection drop constraint FK_bbsSection_sUid
alter table bbsUsers drop constraint PK_bbsUsers_UID

delete from bbsUsers where UID='2' 



update bbsPoint set uPoint=(uPoint + 10) where uName='小雨点'



delete from bbsReply where rTID=3
delete from bbsTopic where tSID=4
delete from bbsSection where sID=4


delete from bbsReply where rTID=1  
delete from bbsReply where rTID=2
alter table bbsUsers drop constraint UQ__bbsUsers__61AE339B08DDAE67

alter table bbsTopic drop column tCount
alter table bbsUsers add  telephone varchar(20) unique check(len(telephone)=11)  null
alter table bbsReply alter column rMsg varchar(200)
alter table bbsUsers drop constraint CK_bbsUsers_uPoint
update bbsUsers set uName='小雪' where UID=1
update bbsUsers set uPoint=100
select * into bbsUser2 from bbsUsers
select * from bbsUser2
delete from bbsUser2
truncate table bbsUser2