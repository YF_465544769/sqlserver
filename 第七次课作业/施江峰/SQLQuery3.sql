create database Student02
on
(
	name='Student02',
	filename='d:\Student02.mdf',
	size=20,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='Student02_log',
	filename='d:\Student02_log.ldf',
	size=20,
	maxsize=50,
	filegrowth=10%
)
use Student02
go 
create table Studenifo
(
	stuNo int identity(2501,1) primary key,
	stuName nvarchar(20) not null unique,
	stuAge int null,
	studdRess nvarchar(20) not null,
	stuSeat int ,
	stuSext	int check(stuSext in('1,','0'))default('0')
)
insert into Studenifo(stuName,stuAge,studdRess,stuSeat,stuSext)values('张秋利','20','美国硅谷','1','1'),('李斯文','18','湖北武汉','2','0'),('马文才','22','湖南长沙','3','1'), 
('欧阳俊雄','21','湖北武汉','4','0'),('梅超凤','20','湖北武汉','5','1'),('陈炫风','20','美国硅谷','6','1'),('陈风','20','美国硅谷','7','0')
create table Class09
(
	examNo int identity(1,1)primary key,
	stuNo char(20) null,
	writtenExam int ,
	labExam int 
)
insert into Class09(stuNo,writtenExam,labExam) values ('s2501','50','70'),('s2502','60','65'),('s2503','86','85'),('s2503','40','80'),('s2504','70','90'),('s2505','85','90')

	select stuNo 学号, stuName 姓名,stuAge 年龄, studdRess 地址,stuSeat 座位号,stuSext 性别 from StudenIfo --查询表StudenIfo的所以数据
	select stuName 姓名,stuAge 年龄, studdRess 地址 from StudenIfo --查询表StudenIfo中姓名 年龄 地址的数据
	select stuNo as 学号, writtenExam as 笔试成绩,labExam as 机试成绩 from  Class09 --查询表 Class09中学号 笔试成绩 机试成绩的数据
	select stuNo 学号, writtenExam 笔试成绩,labExam 机试成绩 from  Class09 --查询表 Class09中学号 笔试成绩 机试成绩的数据
	select 学号=stuNo,笔试成绩=writtenExam ,机试成绩=labExam  from  Class09 --查询表 Class09中学号 笔试成绩 机试成绩的数据
	select stuNo 学号, stuName 姓名, studdRess 地址,stuName+'@'+ studdRess as 邮箱 from StudenIfo--查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
	select stuNo as 学号, writtenExam as 笔试成绩,labExam as 机试成绩,labExam+ writtenExam 总分 from Class09--查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分（笔试+机试）
	select  studdRess from  StudenIfo--查询学生信息表（stuInfo）中学生来自哪几个地方 
	select distinct stuAge as 所有年龄 from StudenIfo--查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名'所有年龄'
	select * from StudenIfo where  stuSeat<=3 --查询学生信息表（stuInfo）中前3行记录 
	select top 4 stuName 姓名, stuSeat 座位号 from StudenIfo --查询学生信息表（stuInfo）中前4个学生的姓名和座位号
	select top 50 percent  * from StudenIfo  order by stuAge DESC --查询学生信息表（stuInfo）中一半学生的信息
	select * from  StudenIfo  where studdRess =  '湖北武汉' and stuAge=20 --将地址是湖北武汉，年龄是20的学生的所有信息查询出来
	select * from Class09 where labExam like '[60-80]%'--将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
	select * from  StudenIfo where studdRess in('湖北武汉','湖南长沙')--查询来自湖北武汉或者湖南长沙的学生的所有信息
	select * from  StudenIfo where studdRess ='湖北武汉' or studdRess ='湖南长沙'--查询来自湖北武汉或者湖南长沙的学生的所有信息

	select * from Class09 where writtenExam like '[70-90]%' order by writtenExam ASC--查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列

	select * from  StudenIfo where stuName like '张%'  --查询姓张的学生信息
	select * from  StudenIfo where studdRess like '湖%'  --查询学生地址中有‘湖’字的信息
	select * from  StudenIfo where  stuName like '陈_'   --查询姓陈但名为一个字的学生信息
	select * from  StudenIfo where stuName like '_俊%' --查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
	select * from  StudenIfo order by   stuAge DESC --按学生的年龄降序显示所有学生信息
	select * from  StudenIfo order by   stuAge DESC ,  stuSeat ASC --按学生的年龄降序和座位号升序来显示所有学生的信息
	 select top 1 * from Class09  order by  writtenExam DESC --显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
	 select top 1 * from Class09  order by  labExam ASC    --显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩