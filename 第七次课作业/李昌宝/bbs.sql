create database bbs
on
(
name = 'bbs',
size = 5,
maxsize = 500,
filename = 'D:\.mdf',
filegrowth = 10%
)
log on
(
name = 'bbs_log',
size = 5,
maxsize = 500,
filename = 'D:\.ldf',
filegrowth = 10%
)
use bbs
go
create table bbsUsers   --用户信息表
(
UID int identity (1,1),   
uName varchar(10) not null ,  
uSex  varchar(2) not null,  
uAge  int not null,  
uPoint  int not null,  
)
go
create table bbsSection		--板块表
(
 sID  int identity, 
 sName  varchar(10) not null,
 sUid   int,  
)
go
alter table bbsUsers add constraint PK_UID primary key(UID);
alter table bbsUsers add constraint UQ_uName unique(uName);
alter table bbsUsers add constraint CK_uSex check(uSex = '男' or uSex = '女');
alter table bbsUsers add constraint CK_uAge check(uAge>=15 and uAge<=60);
alter table bbsUsers add constraint CK_uPoint check(uPoint>=0);
alter table bbsSection add constraint CK_sID primary key (sID);
alter table bbsSection add constraint FK_sUid foreign key(sUid) references bbsUsers(UID)
go
create table bbsTopic           --主贴表
(
tID  int primary key identity,
tUID  int references bbsUsers(UID),
tSID  int references bbsSection(sID),
tTitle  varchar(100) not null,
tMsg  text not null,
tTime  datetime  ,
tCount  int
)
go
create table bbsReply		--回贴表
(
rID  int primary key identity,
 rUID  int references bbsUsers(UID),
 rTID  int references bbsTopic(tID),
 rMsg  text not null,
  rTime  datetime 
)
go
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection (sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2 ,4,'范跑跑','谁是范跑跑 ',2008-7-8,1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(3 ,1,'.NET','与JAVA的区别是什么呀？ ',2008-9-1,2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(1 ,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀 ',2008-9-10,0)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,1,'不认识',2008-7-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,2,'没有区别',2008-9-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(2 ,2,'请百度',2008-9-12)
delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsReply where rUID=2
delete from bbsUsers where uName='逍遥'
update bbsUsers set uPoint=uPoint+10 where UID=1
select * from bbsUsers
delete bbsTopic where tID=3
delete from bbsReply
alter table bbsTopic drop column tCount
alter table bbsUsers add  telephone varchar(20) unique check(len(telephone)=11) not null
alter table bbsReply alter column rMsg varchar(200)
alter table bbsUsers drop CK_uPoint
update bbsUsers set uName='小雪' where UID=1
update bbsUsers set uPoint=100
select * into bbsUser2 from bbsUsers
truncate table bbsUser2
delete from bbsUser2
