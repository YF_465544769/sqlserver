create database ATM
on primary
(
	name = 'ATM',
	size = 5,
	maxsize = 500,
	filename = 'D:\bank.mdf',
	filegrowth = 15%
)
log on 
(
	name = 'ATM_log',
	size = 5,
	maxsize = 500,
	filename = 'D:\bank.ldf',
	filegrowth = 15%
)
use ATM
GO
create table userInfo				--用户信息表
(
	customerID int identity(1,1) primary key,--顾客编号
	customerName varchar(10) not null,		 --开户名
	PID varchar(20) unique check(len(PID) = 15 or len(PID) = 18) not null, --身份证号
	telephone varchar(18) not null check(telephone like '____-________'), --联系电话
	address text,    --居住地址
)
go
create table cardInfo		--银行卡信息表
(
	cardID char(19) not null check(cardID like '1010 3567 [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9]'),
								--卡号
	curType varchar(10) not null default('RMB') ,	--货币种类
	savingType varchar(10) check(savingType = '活期' or savingType = '定期' or savingType = '定活两期'),
				--存款类型
	openDate smalldatetime not null DEFAULT GETDATE(),		--开户日期
	balance int not null check(balance>=1),			--余额
	pass varchar(6)  not null check(pass like '[0-9][0-9][0-9][0-9][0-9][0-9]') default(888888),   --密码
	IsReportLoss varchar(2) check(IsReportLoss = '是' or IsReportLoss = '否'), --是否挂失
	customerID int identity(1,1) references userInfo(customerID),		--顾客编号
)
alter table cardInfo
add constraint PK_ccardInfo_ardID primary key(cardID)
alter table cardInfo add constraint DF_cardInfo_IsReportLoss default '否' for IsReportLoss
go
create table transInfo  --交易信息表
(
	transId int identity(1,1) primary key,		--交易编号
	transDate smalldatetime not null DEFAULT GETDATE(), --交易日期
	cardID char(19) references cardInfo(cardID),
	transType  varchar(4) check(transType = '存入' or transType = '支取'),  --交易类型
	transmoney int not null check(transmoney>0),			--交易金额
	remark text,    --备注
)
insert into userInfo(customerName,PID,telephone,address)
values ('孙悟空' ,123456789012345678, '0716-78989783', '北京海淀' )
insert into userInfo(customerName,PID,telephone)
select '沙和尚',421345678912345678 ,'0478-44223333' union
select '唐僧' ,321245678912345678 , '0478-44443333 '
insert into cardInfo (cardID,balance,savingType) 
values('1010 3567 1234 5666',1000,'活期'),('1010 3567 1234 5866',1,'活期'),('1010 3567 1234 5865',1,'活期')
update cardInfo set pass = 611234 where cardID ='1010 3567 1234 5666'
insert into transInfo(transType,transmoney,cardID,remark) values ('支取',200,'1010 3567 1234 5666','孙悟空') 
update cardInfo set balance = balance - 200 where cardID ='1010 3567 1234 5666'
insert into transInfo(transType,transmoney,cardID,remark) values ('存入',300,'1010 3567 1234 5866','沙和尚') 
update cardInfo set balance = balance + 200 where cardID ='1010 3567 1234 5866'
select * from cardInfo where openDate between 2021-03-09 and 2021-03-19
select * from cardInfo
update cardInfo set IsReportLoss = '是' where cardID = '1010 3567 1234 5865'


















