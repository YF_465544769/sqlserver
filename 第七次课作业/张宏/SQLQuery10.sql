use master
go
create database bank
on
(
name='bank',
filename='D:\bank.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10%
)
log on(
name='bank_log',
filename='D:\bank_log.mdf',
size=5mb,
maxsize=50mb,
filegrowth=10%

)
go
use bank
go
create table userInfo 
(
customerID int primary key identity(1,1), 
customerName nvarchar(10) not null,
PID varchar(20) check(len (PID)=15 or len(PID)=18) unique not null,
telephone varchar(20) check(len(telephone)=13 and telephone like'____-________') not null,
address text not null
)
create table cardInfo
(
cardID varchar(20) primary key check(cardID like '1010 3576 [0-9][0-9][0-9][0-9] [0-9][0-9][0-9]'),
curType varchar(20) default('RMB'),
savingType varchar(10) check(savingType in('活期','定活两便','定期'))not null,
openDate datetime default(getdate())not null,
balance money check(balance>=1) not null,
pass int check(len(pass)=6)default(888888),
IsReportLoss nvarchar(2)  check(IsReportLoss='是' or IsReportLoss='否')default('否') not null,
customerID int references userInfo(customerID)
)
create table transInfo 
(
transId int primary key identity(1,1),
transDate datetime default(getdate())not null,
cardID varchar(20) references cardInfo(cardID),
transType  nvarchar(4) check(transType in('存入','支取')),
transMoney money check(transMoney>0)not null,
remark text 
)

insert into userInfo(customerName,PID,telephone,address)
select'孙悟空','123456789012345','0716-78989783','北京海淀'
insert into userInfo(customerName,PID,telephone,address)
select'沙和尚','421345678912345678','0478-44223333','家里蹲'
insert into userInfo(customerName,PID,telephone,address)
select'唐僧','321245678912345678','0478-44443333','五道口'
insert into cardInfo(balance,savingType,cardID)
select  1000,'活期','1010 3576 1234 567'
insert into cardInfo(balance,savingType,cardID)
select 1,'定期','1010 3576 1212 117'
insert into cardInfo(balance,savingType,cardID)
select 1, '定期','1010 3576 1212 113'  
                                                                                                                
update cardInfo set pass=611234 where cardID='1010 3576 1234 567'
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'
update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'  

select * from  userInfo
select * from  cardInfo
select * from  transInfo  
select * from  cardInfo where openDate between 2021-03-09 and 2021-03-19


