--某银行拟开发一套ATM取款机系统，实现如下功能：
--1、开户（到银行填写开户申请单，卡号自动生成）
--2、取钱
--3、存钱
--4、查询余额
--5、转账（如使用一卡通代缴手机话费、个人股票交易等）


--第一阶段：建库、建表、建约束、添加数据
--A. 现要求对“ATM柜员机系统”进行数据库的设计并实现，数据库保存在D:\bank目录下，文件增长率为15% 。
--B. 根据下图创建表，约束种类参考下列表的说明
create database ATM柜员机系统
on
(
   name='ATM柜员机系统',
   filename='D:\SQL课堂\ATM柜员机系统.mdf',
   size=5,
   maxsize=100,
   filegrowth=15%
)
log on
(
   name='ATM柜员机系统_log',
   filename='D:\SQl课堂\ATM柜员机系统_log.ldf',
   size=5,
   maxsize=100,
   filegrowth=15%
)
go
use ATM柜员机系统
go

--用户信息表：userInfo ：
--字段名称	说  明
--customerID	顾客编号	自动编号（标识列），从1开始，主键
--customerName	开户名	必填
--PID	身份证号	必填，只能是18位或15位，身份证号唯一约束
--telephone	联系电话	必填，格式为xxxx-xxxxxxxx或手机号13位
--address	居住地址	可选输入

create table userInfo 
(
   customerID int primary key identity(1,1),
   customerName nchar(10) not null,
   PID varchar(20) not null unique check(len(PID)=15 or len(PID)=18),
   telephone varchar(20) not null check(len(telephone)=13 or telephone like '____-________'),
   address text
)
go
use ATM柜员机系统
go

--银行卡信息表：cardInfo
--字段名称	说  明
--cardID	卡号	必填，主健，银行的卡号规则和电话号码一样，一般前8位代表特殊含义，如某总行某支行等。
--假定该行要求其营业厅的卡号格式为：1010 3576 xxxx xxx开始,每4位号码后有空格，卡号一般是随机产生。
--curType	货币种类	必填，默认为RMB
--savingType	存款类型	活期/定活两便/定期
--openDate	开户日期	必填，默认为系统当前日期
--balance	余额	必填，不低于1元,否则将销户
--pass	密码	必填，6位数字，开户时默认为6个“8”
--IsReportLoss	是否挂失	必填，是/否值，默认为”否”
--customerID	顾客编号	外键，必填，表示该卡对应的顾客编号，一位顾客允许办理多张卡号

create table cardInfo
(
   cardID int not null primary key check(cardID like '1010 3576 ____ ___'),
   curType varchar(20) not null default('RMB'),
   savingType nchar(10) check(savingType='活期' or savingType='定活两便' or savingType='定期') ,
   openDate date not null default(getdate()),
   balance float not null check(balance>=1),
   pass varchar(10) not null default('888888') check(len(pass)=6),
   IsReportLoss nvarchar(4) not null default('否') check(IsReportLoss='是' or IsReportLoss='否'),
   customerID int not null references userInfo(customerID)
)
go
use ATM柜员机系统
go

--交易信息表：transInfo 
--字段名称	说  明
--transId	交易编号	标识列、主键
--transDate	交易日期	必填，默认为系统当前日期
--cardID	卡号	必填，外健，可重复索引
--transType 	交易类型	必填，只能是存入/支取
--transMoney	交易金额	必填，大于0
--remark	备注	可选输入，其他说明

create table transInfo
(
   transId int not null primary key identity(1,1),
   transDate date not null default(getdate()),
   cardID int not null references cardInfo(cardID),
   transType nchar(10) not null check(transType='存入' or transType='支取'),
   transMoney money not null check(transMoney>0),
   remark text
)
go
--C. 根据下列条件插入和更新测试数据
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--  开户金额：1000 活期   卡号：1010 3576 1234 567

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113

insert into userInfo values ('孙悟空','123456789012345','0716-78989783','北京海淀'),
('沙和尚','421345678912345678','0478-44223333',null),
('唐僧','321245678912345678','0478-44443333',null)
select * from userInfo
insert into cardInfo values ('1010 3576 1234 567','RMB','活期',getdate(),1000,888888,'否',1),
('1010 3576 1212 117','RMB','定期',getdate(),1,888888,'否',2),
('1010 3576 1212 113','RMB','定期',getdate(),1,888888,'否',3)
select * from cardInfo

--第二阶段：增、删、改、查
--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
       --update cardInfo set pass=611234
--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200 
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
       --insert into transInfo values(getdate(),'1010 3576 1234 567','支取',200 ,'取钱' )
 --3.	用同上题一样的方法实现沙和尚存钱的操作(存300)
       --insert into transInfo values(getdate(),'1010 3576 1212 117','存入',300,'存')
       --insert into transInfo values(getdate(),'1010 3576 1212 117','存入',300,'存')
 --4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
       --update cardInfo set IsReportLoss='是'
 --5.	查询出2021-03-09到2021-03-19开户的银行卡的信息
--以下题目基于之前的bbs数据库
--1、删除字段：删除主贴表的“回帖数量”字段
--2、新增字段：用户表新增“手机号码”字段，并为该字段添加唯一约束，且长度为11，必填
--3、修改字段：修改回帖表的“回帖消息”字段，数据类型改成varchar(200)
--4、删除约束：删除用户表的“积分”字段的检查约束
--5、修改数据：修改用户“小雨点”的名字改成“小雪”，将所有用户的积分都增加100
--6、复制用户表的数据到新表bbsUser2
--7、删除bbsUser2的所有数据，写出两种方式的语句
 
-- update cardInfo set pass=611234 where customerID = 6
 --select * from cardInfo
 update cardInfo set IsReportLoss='是'
 select*from userInfo where customerName='孙悟空'
 select cardID  from cardInfo where customerID in ( select customerID from userInfo where customerName='孙悟空')
 insert into transInfo  values(default,'1010 3576 1234 567','支取',200,'在今天花了200元')
   update  cardInfo set balance=balance-200 where customerID in(select customerID from userInfo where customerName='孙悟空')
   select*from userInfo where customerName='沙和尚'
   select cardID  from cardInfo where customerID in ( select customerID from userInfo where customerName='沙和尚')
   insert into transInfo  values(default,'1010 3576 1212 117','存入',300,'在今天存了300元')
   update  cardInfo set balance=balance+300 where customerID in(select customerID from userInfo where customerName='沙和尚')
   update  cardInfo set IsReportLoss='是' where customerID=( select customerID  from userInfo where customerName='唐僧') 
   select*from transInfo where transMoney=(select max(transMoney) from transInfo )
   select sum(transMoney) as 总交易金额 from transInfo