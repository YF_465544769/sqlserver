create database testDB
on
(
name='wssb',
filename='D:\wssb\testDB',
size=10,
maxsize=50,
filegrowth=10%
)
log on
(
name=wssb,
filename='D:\wssb\testDB1_log.ldf',
size=5,
filegrowth=10%
)
use testDB
go
create table stuinfo
(
loginID int primary key identity(1,1),
loginNeam char(10) not null unique,
logunpwd char(20) not null default(123456),
loginsex bit not null
)
go

create database company
on
(
NAME='wssb',
filename='D:\wssb\testDB2.MDF',
SIZE=10,
MAXSIZE=50,
FILEGROWTH=10%
),
(
NAME='wssb2',
filename='D:\wssb\testDB2.NDF',
SIZE=10,
MAXSIZE=50,
FILEGROWTH=10%
)
LOG ON
(
	NAME='wssb_log',
	filename='D:\wssb\testDB2_log.ldf',
	SIZE=1,
	MAXSIZE=50,
	FILEGROWTH=10%
)
go
create table sectionlnfo
(
	 sectionID int Primary Key,
	 sectionName varchar(10) not NULL,
)
create table userinfo
(
	userNO int Primary Key not NULL,
	userName varchar(10)Unique  not NULL check(len(userName)<4),
	userSex varchar(2) not NULL  check(userSex='��' or userSex='Ů' ),
	userAge int not NULL check(1<userAge and 100>userAge),
	userAddress varchar(50) default'����',
	userSection int Foreign key references sectionlnfo(sectionID),
)
create table wprklnof
(
	workld int primary key identity(1,1) not NULL ,
	userld int Foreign key references sectionlnfo(sectionID) not NULL,
	workTime datetime not NULL,
	WorkDesription varchar(40) not NULL check(WorkDesription='�ٵ�'or WorkDesription='����'or WorkDesription='��' or WorkDesription='����' or WorkDesription='�¼�'),
)
go
create database Stuent
on
(
 nema=wssb3,
 filename='D:\wssb\testDB3.MDF',
 size=5,
 maxsize=50,
 filegrowth=10%
)
log on
(
nema=wssb4,
fileame='D:\wssb\testDB3_log.ldf',
    SIZE=1,
	MAXSIZE=50,
	FILEGROWTH=10%
)
go
create table class
(
calssid int primary key identity(1,1),
calssname varchar(20) not NULL UNIQUE,
calsstime int not NULL,
calssdescription varchar(100)
)
create table Stuebt
(
StuebtID int primary key identity(1,1),
Stuebtname int check(Stuebtname>2) unique,
Stuebtxes varchar(2)default'��' check(Stuebtxes='��'or Stuebtxes='Ů') not NULL,
Stuebtage int check(Stuebtage>15 and Stuebtage<40),
Stuebthome varchar(10) default'�����人',
StuebtcalssID int Foreign key references class(calssid),
)
create table course 
(
courseid int primary key identity(1,1),
coursename int unique not null,
coursedescription varchar(100), 
)
create table grade
(
gradeid int primary key identity (1,1),
gradeStuedt int Foreign key references Stuebt(Stuebtid) not NULL,
gradecourse int Foreign key references course(courseid),
grade int check(grade<100 and grade>0)
)
go
create database building
on
(
 nema=wssb5,
 filename='D:\wssb\testDB4.MDF',
 size=5,
 maxsize=50,
 filegrowth=10%
)
log on
(
nema=wssb6,
fileame='D:\wssb\testDB4_log.ldf',
    SIZE=1,
	MAXSIZE=50,
	FILEGROWTH=10%
)
go
create table tbiUser
(
	userid int primary key identity(1,1),
	userName varchar(20),
	userTel int default(123456),
)
create table tbiHouseType
(
	typeid int primary key identity(1,1),
	typName char(10) check(typName='����'or typName='��ͨסլ' or typName='ƽ��' or typName='������'),
)
create table tblQx
(
	qxid int primary key identity(1,1),
	qxName varchar(10)  check(qxName='���'or qxName='����' or qxName='����'),
)
create table tbiHouseinfo
(
	id int primary key identity(1,1),
)
create table descID
(
	userID int primary key identity(1,1),
	zj money,
	shi varchar(10),
	ting varchar(10),
	typeid int references tbiHouseType(typeid),
	qxid int references tblQx(qxid)
)
